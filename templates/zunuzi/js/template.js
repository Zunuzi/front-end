var storage_teaching = Joomla.getOptions('teaching');

jQuery(document).ready(function(){
    if(localStorage.getItem('paypal_success') == 'true') {
        paymentSuccess(localStorage.getItem('price'));
    } else if (localStorage.getItem('paypal_success') == 'false') {
        paymentFalse(localStorage.getItem('message'));
    }
    localStorage.removeItem('paypal_success');
    localStorage.removeItem('price');
    localStorage.removeItem('message');
});

function paymentFalse($message, $stripe = false) {
    if(!$stripe) {
        jQuery('.for-close-modal').fadeIn();
    } else {
        jQuery('#my-balance-payment-modal').fadeOut();
        jQuery('#loader').css('display', 'none');
    }
    jQuery('.my-balance-payment-modal.my-balance-payment-modal-message.error-messages').fadeIn();
    jQuery('.my-balance-payment-modal.my-balance-payment-modal-message.error-messages .server-message-text').html($message);
}

function paymentSuccess($price, $stripe = false) {
    // window.dataLayer = window.dataLayer || [];
    // window.dataLayer.push({
    //     'event': 'onPaySuccess'
    // });
    if(!$stripe) {
        jQuery('.for-close-modal').fadeIn();
    }
    jQuery('.success-payment-size').html($price);
    if ($stripe) {
        var total = parseInt(jQuery('.my-balance-total').text(), 10);
        total = total + parseInt($price);
        jQuery('.my-balance-total').text(total + '.00');
        jQuery('#loader').css('display', 'none');
        var available = parseInt(jQuery('.my-balance-available').text(), 10);
        available = available + parseInt($price);
        jQuery('.my-balance-available').text(available + '.00');
    }

    jQuery('#my-balance-payment-modal').fadeOut();
    jQuery('.my-balance-payment-modal.my-balance-payment-modal-message.success-messages').fadeIn();
    jQuery('.my-balance-payment-modal.my-balance-payment-modal-message.success-messages .server-message-text')
        .html(Joomla.JText._('COM_TEACHING_THANKS_FOR_PAYMENT'));
}
// toggle menu
function openMenu() {
    let toggle_menu = document.getElementById("toggleMenu");
    let toggle_btn = document.getElementById("toggleBtn");
    let header = document.getElementById("header");
    if (toggle_menu.style.display === "block") {
        toggle_menu.style.display = "none";
        toggle_btn.style.backgroundImage = 'url("/images/menu-24px 1.svg")';
        header.classList.remove('full-height');
    } else {
        toggle_menu.style.display = "block";
        toggle_btn.style.backgroundImage = 'url("/images/close-icon.svg")';
        header.classList.add('full-height');
    }
}

// languages
function openDropdown() {
    document.getElementById("my-Dropdown").classList.toggle("show");
}
function openDropdownHeader() {
    document.getElementById("myDropdownHeader").classList.toggle("show");
}


function initZunuzi()
{
//balance-page

//payment-button
//     jQuery(".withdrawBtn").click(function() {
//         jQuery(this).toggleClass('my-balance__btn-open');
//         jQuery('#withdraw-txt').slideToggle('hide');
//         jQuery('.my-balance__buying-credit').slideToggle('slow');
//     });
//     if(window.location.href.match('withdraw=true')) {
//         jQuery('.withdrawBtn').click();
//     }
    jQuery('input#paymentRequestAmount').keyup(function (){
        console.log(jQuery(this).val());
        if (jQuery(this).val() != '') {
            jQuery('.strong-withdraw-amount').removeClass('hide');
            jQuery('.withdraw-amount').html(jQuery(this).val());
        } else {
            jQuery('.strong-withdraw-amount').addClass('hide');
            jQuery('.withdraw-amount').html('');
        }
    });

    jQuery('.commission').mouseover(function() {
        jQuery('.commission__text').addClass('show');
        jQuery('.commission__text').removeClass('hide');
    });

    jQuery('.commission').mouseleave(function() {
        jQuery('.commission__text').removeClass('show');
        jQuery('.commission__text').addClass('hide');
    });

    jQuery(document).on('click', ".showViewDetails", function () {
        jQuery(this).next(".hideViewDetails").slideToggle();
        jQuery(this).closest("tr").siblings().find('.hideViewDetails').slideUp();
        jQuery( this ).toggleClass('orange-style');
    });

    /*select:*/
    jQuery.teaching.initCustomSelect(document);

    //video

    jQuery(document).on('click', '.videoBtn', function (e) {
        var myVideo = jQuery(e.currentTarget).siblings('video')[0];

        if (myVideo.paused)
            myVideo.play();
        else
            myVideo.pause();
    });


    jQuery('.review-section_video-content').on('tab.close', function (e) {

        var myVideo = jQuery(e.currentTarget).find('video')[0];
        if (!myVideo.paused)
            jQuery(e.currentTarget).find('button.videoBtn').click();
    });
    jQuery(document).on('click', '.videoBtn', function() {
        jQuery( this ).toggleClass('activeVideoBtn');
    });

    // tabs
    if (jQuery('button.tabItem').length > 0)
    {
        jQuery('button.tabItem').on('click', function (e) {
            let button = e.currentTarget;
            let tabId = jQuery(button).data('tab');
            let target_id = jQuery(jQuery(button).parent()).data('target');
            if (jQuery(button).hasClass('active')) {
                jQuery(button).removeClass('active');
                jQuery(target_id + ' #' + tabId).removeClass('active').trigger('tab.open');
            } else {
                jQuery(button).addClass('active').siblings('[data-tab]').removeClass('active');
                jQuery(target_id + ' #' + tabId).addClass('active').siblings('[data-content].active').removeClass('active').trigger('tab.close');
            }
        });
    }

    //wishlist

    jQuery('.wishlist-icon:not(.button-login)').on('click', function(e){
        var el = jQuery(this), em = jQuery('span', el);
        let is_fav = em.hasClass('fa-heart-o') ? 1 : 0;
        jQuery.post('index.php?option=com_teaching&task=favourite.action', {
            uid: el.closest('.search-tutor-item').data('id'),
            act: is_fav,
            [storage_teaching.token]: "1"
        }, function (r) {
            if (!r.ok) alert(r.msg ? r.msg : 'Error! Please try again later.');
            else {
                var ele = jQuery(e.currentTarget).find('.chang-icon');
                if(ele.hasClass('fa-heart-o')){
                    ele.removeClass('fa-heart-o')
                        .addClass('fa-heart')
                }
                else{
                    ele.addClass('fa-heart-o')
                        .removeClass('fa-heart')
                }
                if (is_fav)
                {
                    jQuery(e.currentTarget).attr('title', Joomla.JText._('COM_TEACHING_REMOVE_FROM') + ' ' + Joomla.JText._('COM_TEACHING_WISH_LIST'));
                }
            }
        }, 'json');
    });
}


function makeBig() {
    myVideo.width = 560;
}

function makeSmall() {
    myVideo.width = 320;
}

function makeNormal() {
    myVideo.width = 420;
}

function autoScrollTo(el) {
    var top = jQuery("#" + el).offset().top;
    jQuery("html, body").animate({ scrollTop: top - 75 }, 1000);
}


function closeAllSelect(elmnt) {
    var x, y, i, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    for (i = 0; i < y.length; i++) {
        if (elmnt == y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < x.length; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}

document.addEventListener("click", closeAllSelect);

function openContent(evt, name) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName('tabcontent');
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = 'none';
        // tabcontent[i].classList.toggle('hide');
    }
    tablinks = document.getElementsByClassName('tablinks');
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(' active', "");

    }

    document.getElementById(name).style.display = 'block';

    evt.currentTarget.className += ' active';

}

(function(jQuery)
{


    if (!jQuery.hasOwnProperty('teaching'))
    {
        jQuery.teaching = {};
    }
    jQuery.teaching.review_list = function () {
        let review_list = document.getElementsByClassName('reviews-list')[0];
        let cols = 4;
        if (window.innerWidth < 700) {
            cols = 1;
        } else if (window.innerWidth < 1200) {
            cols = 2;
        } else if (window.innerWidth < 1600) {
            cols = 2;
        } else {
            cols = 3;
        }

        let item_list_html = document.createElement('div');
        item_list_html.innerHTML = jQuery.teaching.review_items;
        let review_items = jQuery(item_list_html).find('.review-item');

        let columns = [];
        for (let j = 1; j <= cols; j++) {
            columns[j] = document.createElement('div');
            columns[j].classList = 'col-sm-' + (12 / cols);
            for (let i = j-1; i < review_items.length; i+=cols) {
                columns[j].innerHTML += review_items[i].outerHTML;
            }
        }
        review_list.innerHTML = '';
        for (let j = 1; j <= cols; j++){
            review_list.innerHTML += columns[j].outerHTML;
        }
    };


    jQuery.teaching.buyCredits = function(e) {
        e.preventDefault();
        let radioChecked = jQuery(e.target).find('input:checked').val();
        if(radioChecked === 'stripe') {
            const paymentCont = jQuery('#jform_sum').val();
            jQuery('.payment-count').html('$' + paymentCont);
            setTimeout(function (){
                jQuery.teaching.eventWorker.stopWork();
            }, 200);
        } else {
            jQuery('.my-balance-bottom-input-group').slideUp();
            jQuery.teaching.eventWorker.startWork();
        }
        jQuery.teaching.eventWorker.startWork();
        jQuery.ajax({
            url: e.currentTarget.getAttribute('action') + '&returnType=raw',
            data: jQuery(e.currentTarget).serializeArray(),
            method: 'post',
            success: function (data) {
                //var data = JSON.parse(data)
                if (typeof data == 'object') {
                    if (data.hasOwnProperty('success') && !data.success) {
                        if (data.data.hasOwnProperty('error_field')) {
                            $el = jQuery('#jform_' + data.data.error_field);
                            $el.addClass('error invalid');
                            if(data.data.message) {
                                $el.closest('.my-balance__buying-credit_input').find('.my-balance__block_description')
                                    .html('<span class="validation-error-message error-field-email-text" style="color: red;font-size: 14px;line-height:1.1;display:block;">' + data.data.message + '</span>');
                            }
                        }
                        jQuery('#loader').hide();
                        return false;
                    }
                    if (data.data.hasOwnProperty('redirect') && data.data.redirect) {
                            window.location.href = data.data.redirect;
                        return true;
                    }
                } else {
                    if(radioChecked === 'stripe') {
                        jQuery('#my-balance-payment-modal, .for-close-modal').fadeIn();
                    }
                    let s_img = jQuery('<div style="display: none;"></div>').append(data).appendTo(document.body).find('img');
                    jQuery(s_img).click();
                }
            }
        })
    };

    jQuery(document).ready(function()
    {
        var getUrlParameter = function getUrlParameter(sParam) {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === sParam) {
                    return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                }
            }
            return false;
        };
        var displaySignInModal = getUrlParameter('sign-in-modal');
        if(displaySignInModal == 1) {
            jQuery('.header__link.sign-in-button').click();
        }

        initZunuzi();
        jQuery('#buyCreditsForm').on('submit', jQuery.teaching.buyCredits);

        // Correcting view of reviews list in tutors profile page
        let review_list = document.getElementsByClassName('reviews-list');
        if (review_list.length > 0) {
            jQuery.teaching.review_items = review_list[0].innerHTML;
            jQuery.teaching.review_list();
            window.onresize = jQuery.teaching.review_list;
        }
    });
    jQuery('#member-registration.registration').on('submit', function(e) {
        let form = this;
        let submit = true;
        let username_exp = /[0-9"\[\]\^£$%&*()}{@;:#`!.~?><>,|=_+¬]/;
        let email_exp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        function error(el) {
            jQuery(el).addClass('error');
        }
        function success(el) {
            jQuery(el).removeClass('error');
        }
        if (username_exp.test(jQuery(form).find('#jform_name').val())) {
            submit = false;
            error('#jform_name');
        } else {
            success('#jform_name');
        }
        if (username_exp.test(jQuery(form).find('#jform_profile_default_lastname').val())) {
            submit = false;
            error('#jform_profile_default_lastname');
        } else {
            success('#jform_profile_default_lastname');
        }
        if (!email_exp.test(jQuery(form).find('#jform_email1').val())) {
            submit = false;
            error('#jform_email1');
        } else {
            success('#jform_email1');
        }
        // if (!email_exp.test(jQuery(form).find('#jform_email2').val())) {
        //     submit = false;
        //     error('#jform_email2');
        // } else {
        //     success('#jform_email2');
        // }

        return submit;
    });

    // Cookie Agreement
    function deleteAllCookies() {
        var cookies = document.cookie.split(";");

        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
    }
    jQuery('#cookie-accept a.close').on('click',  function (e) {
        jQuery('#cookie-accept > div').hide("slide", {'direction': 'down'}, 500);
        jQuery.cookie('agreed', true);
        let freshdeskButton = jQuery(document).find('#launcher-frame');
        if (freshdeskButton) {
            freshdeskButton.css('bottom', '22px', 500);
        }
    });

    jQuery(window).on('beforeunload unload', function (e) {
        if (!jQuery.cookie('agreed')) {
            deleteAllCookies();
        }
    });

    window.addEventListener('load', function () {
        jQuery('#cookie-accept').show('slide', {'direction':'down', 'delay':500}, 500);
    });

})(jQuery);


function modal_settings(modal_details){
    var id = jQuery(modal_details).attr('data-id');
    var title = jQuery(modal_details).attr('data-text');
    jQuery('.adult_learner_modal_title').html(title);
    jQuery('.adult_learner_role_id').val(id);
}

// Handle key pressing
document.onkeydown = (e) => {
    if (e.code) {
        switch (e.code.toLowerCase()) {
            case 'escape':
                // Close modals and popovers
                let modals = document.getElementsByClassName('modal');
                let popovers = document.getElementsByClassName('popover')

                if (popovers.length > 0) {
                    // Close all popovers
                    jQuery('.popover').popover('destroy');
                }
                if (modals.length > 0) {
                    // Close last (length - 1) opened modal
                    jQuery(modals[modals.length -1 ]).modal('hide');
                }
        }
    }
};

jQuery(document).ready(function(){
    jQuery('#gift_card_detail .custom-down').click(function () {
        jQuery(this).css('display', 'none');
        jQuery('#gift_card_detail .custom-up').css('display', 'inline-block');
        jQuery(this).parent().toggleClass('expand');
        jQuery(this).parent().next().toggle();
    });
    jQuery('#gift_card_detail .custom-up').click(function () {
        jQuery(this).css('display', 'none');
        jQuery('#gift_card_detail .custom-down').css('display', 'inline-block');
        jQuery(this).parent().toggleClass('expand');
        jQuery(this).parent().next().toggle();
    });
    jQuery('.gift-exp').on('click', function (e) {
        jQuery('#giftExp').modal();
    });


    jQuery('#giftReceive').on('change', function () {
        let date = jQuery(this).val();
        const url = `${Joomla.getOptions('teaching').base_url}index.php?option=com_teaching&task=referfriend.getReferRequest`;

        var data = {
            date: date,
            [storage_teaching.token]: 1
        }
        jQuery.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            error: function (e) {
                console.log(e);
            },
            success: function (res) {
                jQuery('#giftExp .pane-vScroll tbody tr').remove();
                jQuery.each(res.data.items, function (index, value) {
                    jQuery('#giftExp .pane-vScroll tbody').append('<tr class="gift-tr"><td>'+ value["receiveDate"] +'</td>' +
                        '<td>'+ value["name"] +'</td>' +
                        '<td>'+ value["gift"] +'</td>' +
                        // '<td>'+ value["availableGift"] +'</td>' +
                        '<td>'+ value["expiredDate"] +'</td>' +
                        '<td class="status '+ value["status"] +'">'+ value["status"] +'</td></tr>');
                });
                jQuery('.total-available').html(res.data.totalAvailable);
                jQuery('.grand-total').html(res.data.grandTotal);
            }
        });
    });

    jQuery('.pane-hScroll').scroll(function() {
        jQuery('.pane-vScroll').width(jQuery('.pane-hScroll').width() + jQuery('.pane-hScroll').scrollLeft());
    });
    jQuery(".op-user-date input.form-control").tooltip('disable');

    // Change freshdesk button place
    window.addEventListener('load', function () {
        setTimeout(function() {
            let freshdeskButton = jQuery(document).find('#launcher-frame');
            let cookieAcceptHeight = jQuery(document).find('#cookie-accept').height();
            if (cookieAcceptHeight) {
                freshdeskButton.css('bottom', cookieAcceptHeight + 'px');
            } else {
                freshdeskButton.css('bottom', '22px');
            }
            freshdeskButton.css('display', 'inline-block');
        },100);
    });

    jQuery('#resend-reset-request').on('click', function(e) {
        e.preventDefault();
        jQuery(this).closest('form').submit();
    });

});
