jQuery(function ($) {

    $('.header__link').on('click', function (e) {
        const id = $(this).attr('href');
        if (id[0] === '#') {
            if ($('#toggleBtn').is(":visible")) {
                $('#toggleBtn').click();
            }
            e.preventDefault();
            $('html, body').animate({
                scrollTop: $(id).offset().top
            }, 1000);
        }
    });

    $('.selectpicker').selectpicker();
    $('input[type=radio]').on('change', function () {
        $(".radio").removeClass("test_prep_radio_checked");
        $(this).closest(".radio").addClass("test_prep_radio_checked");
    });
    showPassword = function (e) {
        var $this = $(e);
        $this.toggleClass("fa-eye fa-eye-slash");
        var input = $this.parent().find('input');
        if (input.attr('type') == 'password') {
            input.attr('type', 'text');
        } else {
            input.attr('type', 'password');
        }
    };
    $('.moreFilters').on('click', function () {
        var block = $(this),
            span = block.find('span');
        block.parent().find('.closeFilter').slideToggle('showBlock');

        if ($(this).hasClass('active')) {
            span.text(Joomla.JText._('COM_TEACHING_SEARCH_TUTOR_FILTERS'));
            $(this).removeClass('active');
        } else {
            span.text(Joomla.JText._('COM_TEACHING_SEARCH_TUTOR_L_FILTERS'));
            $(this).addClass('active');
        }
    });
    $('html,body').on('keyup', '#jform_name ,  #jform_profile_default_lastname , #jform_contact_name, #jform_contact_emailmsg , #jform_contact_message', function () {
        var val = $(this).val();
        val = val.replace(/\s{2,}/g, ' ');
        if (
            [ 'jform_name', 'jform_profile_default_lastname' ]
                .indexOf( $(this).attr( 'id' ) ) > -1
        ) {
            val = val.replace(/\s+/g, '');
        }
        val = val.replace(/<[^>]*>/g, '');
        $(this).val(val);
    });

    $('li.my-profile').click(function (e) {
        if (e.target.id === 'small-cart-info' || e.target.parentElement.id === 'small-cart-info') {
            return true;
        }
        e.stopPropagation();
        $('#top-menu-special').slideToggle();
        $('#top-menu-edit').slideUp();
    });

    if($('#instanttutoring.tab-pane .table').length > 0) {
        setTimeout(function (){
            $('.requests-tab .tab-pane, .requests-tab .nav-tabs li').removeClass('active');
            $('#instanttutoring').addClass('active');
            $('.requests-tab .nav-tabs li')[1].classList.add('active');
        }, 100)
    }

    $('li.profile-pictures').click(function (e) {
        e.stopPropagation();
        $('#top-menu-edit').slideToggle();
        $('#top-menu-special').slideUp();
    });
    $(document.body).click(function () {
        $('#top-menu-special').slideUp();
        $('#top-menu-edit').slideUp();
    });

    $('.custom-select-field .selectize-input').on('click', function () {
        const options = $('.custom-select-field .option');
        const data = [];

        for (let i = 0; i < options.length; i++) {
            options.each(function (elem, v) {
                if ($(v).data('value') === i + 1) {
                    data.push(v)
                }
            })
        }
        $('.custom-select-field .selectize-dropdown-content').html(data);
    });

    // function titleFixWidthFunc() {
    //     let titleFixWidth = 0;
    //     const parent = $('.withdrawContent-buttons');
    //     if ($(window).width() > 960) {
    //         $('.withdrawContent-buttons > *').each(function (i, v) {
    //             if (!$(v).hasClass('balance-title')) {
    //                 titleFixWidth += $(v).width();
    //             }
    //         });
    //         $(parent).find('.balance-title').width(($(parent).width() - (titleFixWidth * 2 + 20)) + 'px');
    //     } else {
    //         $(parent).find('.balance-title').width('100%');
    //     }
    // }
    //
    // titleFixWidthFunc();

    // $(window).on('resize', titleFixWidthFunc);
    if ($(window).width() > 960) {
        const fixHeightLabel = $('#member-profile .form-group.col-md-4.tutor-disabilities');
        fixHeightLabel.find('label').height(fixHeightLabel.prev().find('label').height() + 'px');
    }

/*    $('#member-profile').on('submit', function (){
        if (!$.teaching.eventWorker.checkWork()) {
            $.teaching.eventWorker.startWork();
        }
    });*/

    // $('#buyCreditsForm .payments-methods-group .flex-style [type=radio]').on('change', function () {
    //     if ($('#jform_payment_method0').attr('checked')) {
    //         if ($('.my-balance-bottom-input-group [type=radio]').length > 0) {
    //             $('.my-balance-bottom-input-group').slideDown();
    //         } else {
    //             $('#my-balance-payment-modal, .for-close-modal').fadeIn();
    //         }
    //     } else {
    //         $('.my-balance-bottom-input-group').slideUp();
    //     }
    // });
    $(document).on('click', '.use-another-card-btn', function () {

        var e = $("#buyCreditsForm")
        $.ajax({
            url: $(e).attr('action') + '&returnType=raw',
            data: $(e).serializeArray(),
            method: 'post',
            success: function (data) {
                let s_img = jQuery('<div style="display: none;"></div>').append(data).appendTo(document.body).find('img');
                jQuery(s_img).click();
                $('#my-balance-payment-modal, .for-close-modal').fadeIn();

            }
        })
    });

    let captionSmallTagCount = 0;
    $('.thumbnail.children-profile-item .caption').each(function (){
        if($(this).find('small').length > 0) {
            captionSmallTagCount++;
        }
    });
    if(captionSmallTagCount > 0) {
        $('.thumbnail.children-profile-item .caption').css('min-height', '95px');
    }

    $('#jform_profile_tutor_subject_levels input[type=checkbox]').on('change', updateAreasExpertise);

    function updateAreasExpertise() {
        let helperVariable = false;
        $('#jform_profile_tutor_subject_levels input[type=checkbox]').each(function (){
            if (this.checked) {
                helperVariable = true;
            } else {
                $('.subform-repeatable-group[data-base-name="'+ this.value +'"').remove();
            }
        });
        if(!helperVariable) {
            $('#set-rl_tabs-2').fadeOut();
            $('#jform_profile_tutor_subject_levels').parents('.container-fluid.row').find('.tab-content').fadeOut();
        } else {
            $('#set-rl_tabs-2').css('display', 'flex');
            $('#jform_profile_tutor_subject_levels').parents('.container-fluid.row').find('.tab-content').fadeIn();
        }
    }
    updateAreasExpertise();

    $(window).load(function() {
        if ( $('.needed-price').length ){
            let neededPrice = $('.needed-price').val();
            if (neededPrice !== ''){
                $('html, body').animate({
                    scrollTop: $('.buyCredits-container').offset().top - 120
                }, 1000);
                let selectedItems = JSON.parse($('.selectedItems').val());
                if (!selectedItems.includes(+neededPrice)){
                    $('.other-amount input').addClass('active').focus();
                }
            }
        }
    });
});

jQuery(document).ready(function ($) {
    const fee = +$('.fee-value').text();
    $('.selectPurchase-input-box').not('.other-amount').on('click', function (){
        $('.purchase_value, .other-amount input').removeClass('active');
        let credit = $(this).find('.purchase_value').addClass('active').val();
        countSummary(credit);
        $('#purchase_value_hidden').val(credit);
        $('.other-amount').find('span').show();
    });

    $('.other-amount').on('click', function (){
        $('.purchase_value').removeClass('active');
        $(this).find('span').hide();
        $(this).find('input').focus();
    });

    $('.other-amount input').on('focusout', function (){
        let val = $(this).val();
        if (val){
            $(this).addClass('active');
            $('#purchase_value_hidden').val(val);
            countSummary(val);
        } else {
            $('.selectPurchase-values .selectPurchase-input:nth-child(2) .selectPurchase-input-box').click();
        }

    });

    function countSummary(credit){
        $('.credits-value, .sub-total-value').text(credit);
        let total = +credit + fee;
        $('.total-value, .orderSummary-btn > span').text(total);
        $('input[name="jform[sum]"]').val(total);
    }

    $('#parent_password').click(function () {
        var form = $('#parentLoggedIn');
        var _comment = $('#parent_password_form');
        var _footer = $('#password_button');
        var header = $('#parent_heading').val();
        var modal = $.teaching.runModal({
            header: header,
            body: _comment.html(),
            footer: _footer.html()
        });

        modal.on('shown.bs.modal', function () {
            modal.find('input[name="password"]').focus();
        });

        var submitFunc = function () {
            var temp = modal.find('input[name="password"]'),
                password = temp.val();
            if (password.length === 0) {
                temp.parent('.form-group').addClass('has-error').focus();
                return false;
            } else {
                $('#passwd_val').val(password);
                form.submit();
            }
        };

        modal.on('click', '#parent_submid_password', function () {
            submitFunc();
        });

        $(document).on('keypress', function (e) {
            e.keyCode === 13 ? submitFunc() : null;
        })
        modal.on('hide.bs.modal', function () {
            return true;
        });
        return false;
    });

    initBrowserBackButtonBlocker();

    addSpaces();


    let changeTimeout = null;

    $(document).on('change keyup', '#completion-steps .price-box td:nth-child(2), #profileBlock .price-box td:nth-child(2)', function () {

        if(changeTimeout !== null){
            clearTimeout(changeTimeout);
        }

        changeTimeout = setTimeout(function(){
            if (typeof $.cookie('convenience') === 'undefined'){
                const options =
                    {
                        modalSelector: 'convenience_popup',
                        body: `<div>
                            <span>${Joomla.JText._('COM_TEACHING_CONVENIENCE')}</span>
                        </div>`,
                        header: 'Convenience',
                        footer: '<button type="button" class="btn btn-success" data-dismiss="modal">' + Joomla.JText._('COM_TEACHING_OK') + '</button>'
                    };

                $.teaching.runModal(options);
                $.cookie("convenience", 1, {expires : 1});
            }
            }, 3000);
    });

    if (jQuery(window).width() < 1024) {
        jQuery('.subjects').insertAfter('.imageWithTxt');
    }

    // move to next section
    $('.down-arrow').on('click', function (){
        $('html, body').animate({
            scrollTop: $('.explore-tutoring').offset().top
        }, 1000);
    });

    // Home Slide
    if ($('.home-slider').length){
        $('.home-slider').slick({
            speed: 1000,
            dots: true,
            prevArrow: `<button class="slick-prev slick-arrow" type="button">
                            <svg width="86" height="86" viewBox="0 0 86 86" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M86 43C86 66.748 66.748 86 43 86C19.252 86 0 66.748 0 43C0 19.2517 19.252 0 43 0C66.748 0 86 19.2517 86 43ZM56.4395 25.4395L40.9395 40.9395L39.8955 41.9834L40.9229 43.0437L56.4229 59.0435L58.5771 56.9563L44.1045 42.0166L58.5605 27.5605L56.4395 25.4395Z" fill="white" fill-opacity="0.8"/>
                            </svg>
                        </button>`,
            nextArrow: `<button class="slick-next slick-arrow" type="button">
                            <svg width="86" height="86" viewBox="0 0 86 86" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M0 43C0 19.252 19.2518 0 43 0C66.7482 0 86 19.252 86 43C86 66.748 66.7482 86 43 86C19.2518 86 0 66.748 0 43ZM45.0774 42.9561L29.5774 26.9561L27.4226 29.0439L41.8954 43.9834L27.4393 58.4395L29.5607 60.5605L45.0607 45.0605L46.1046 44.0166L45.0774 42.9561Z" fill="white" fill-opacity="0.8"/>
                            </svg>
                        </button>`,
            customPaging: function (slider, i) {
                let title = $(slider.$slides[i]).data('title');
                let slide = $(slider.$slides[i]).data('slider');
                return `
                    <button class="custom-click" data-class="slick-${slide}"><span>${title}</span></buttonclass>
                `;
            }
        });
    }

    if ($('.view-calendar').length && localStorage.getItem('calendarView') !== null ){
        $('.'+localStorage.getItem('calendarView')).click();
    }

    $(document).on('submit', '#eventNoteForm', function (){
        let activeView = $('.view-calendar .fc-right .fc-button.fc-state-active');
        if (activeView){
            let activeViewClass = activeView.attr('class').split(' ')[0];
            localStorage.setItem('calendarView', activeViewClass);
        }
    });

    $('a.remove-role').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        const href = $(this).attr('href');
        const currentRole = $(this).data('current');
        const url = location.protocol + "//" + location.host + href;
        let message = Joomla.JText._('MOD_TEACHING_USERGROUPS_REMOVE_CURRENT_CONFIRM_MODAL');
        message = message.replace("%s", currentRole);

        $.teaching.confirmationModal(message, url);
    });

    $('a.remove-child').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();
        const href = $(this).attr('href');
        const url = location.protocol + "//" + location.host + href;
        const childName = $(this).data('child-name');
        let message = Joomla.JText._('COM_TEACHING_DELETE_CHILD_PROFILE_CONFIRM_MODAL');
        message = message.replace("%s", childName);

        $.teaching.confirmationModal(message, url);
    });

});

function initBrowserBackButtonBlocker() {
    const url = window.location.href.split('//')[1];

    if (url === location.host || url === location.host + '/calendar') {
        window.history.pushState({prevUrl: window.location.href}, null, window.location.href)
        window.onbeforeunload = function () {
            history.pushState(null, document.title, location.href);
        };

        history.pushState(null, document.title, location.href);
        window.addEventListener('popstate', function (event) {
            history.pushState(null, document.title, location.href);
        });
    }
}

function addSpaces() {
    const el = jQuery('.teaching-file #preview').next('.note.text-primary');
    el.text(el.text().replace(/,/g, ', '))
}
