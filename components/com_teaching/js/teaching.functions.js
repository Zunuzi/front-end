Array.prototype.diff = function (a) {
    return this.filter(function (i) {
        return a.indexOf(i) < 0;
    });
};

;(function ($) {
    "use strict";
    var storage_teaching = Joomla.getOptions('teaching');
    $(document).ajaxComplete(function (e, xhr, settings) {
        e.preventDefault();
        try {
            if (xhr && xhr.hasOwnProperty("responseJSON")) {
                if (xhr.responseJSON.hasOwnProperty('messages') && xhr.responseJSON.messages) {
                    Joomla.renderMessages(xhr.responseJSON.messages);
                    document.getElementById('main-section').scrollIntoView();
                }
            }

            if (xhr.getResponseHeader('UserGuest') === '1' && (storage_teaching && !storage_teaching.guest)) {
                if (xhr.hasOwnProperty("responseJSON") && xhr.responseJSON.hasOwnProperty('messages') && xhr.responseJSON.messages) {
                    setTimeout(function () {
                        location = '/?option=com_users&view=login';
                    }, 3000);
                } else {
                    location = '/?option=com_users&view=login';
                }
                return false;
            } else if (xhr.getResponseHeader('UserGuest') !== '1'
                && storage_teaching.guest
                && (settings.url.indexOf('login') || settings.url.indexOf('register'))) {
                location.reload();
            }
        } catch (e) {
            if (e) {
                console.log('ajax Complete catched error', e);
            }
        }

    });
    // Object to share our data between JQuery blocks
    $.teaching = $.teaching || {};

    /**
     * Makes Bootstrap checkboxes available from keyboard
     *
     * Full description (multiline)
     *
     * @param   type  $name  Description
     *
     * @return   type  Description
     */
    $.teaching.highlightActiveRadios = function ($parent, selector) {
        var $radios = $parent.find(selector);

        // $parent.find(selector + ":checked").closest('label').css('text-shadow', '1px 1px 5px');

        $radios.focus(function () {

            var $this = $(this);

            $this.closest('fieldset').find('label').css('text-shadow', '').removeClass('active');


            var label = $this.closest('label');
            // label.css('text-shadow', '1px 1px 5px').addClass('active');
            label.addClass('active');
        });
    };

    /**
     * Changes password field from/to password/text
     *
     * @param   JQuery Object  $parent  Context to look
     *
     * @return   bool  Always false
     */
    $.teaching.togglePassword = function ($parent) {
        $parent.find('.passwd_toggle').on('click', function (e) {
            e.preventDefault();
            var $this = $(this);
            var $input = $(this).parent().find('input');

            if ($input.attr('type') == 'password') {
                $this.find('i').prop('class', 'glyphicon icon-eye-close glyphicon-eye-close');
                $input.attr('type', 'text');
            } else {
                $this.find('i').prop('class', 'glyphicon icon-eye-close glyphicon-eye-open');
                $input.attr('type', 'password');
            }
            return false;
        });
    };


    /**
     * Autopoulates hidden fields on submit
     *
     * @param   JQuery Object  $parent  Context to look
     *
     * @return   bool  Always false
     */
    $.teaching.autoPopulateHiddenFieldsonSubmit = function ($parent) {
        var $form = $parent.find('form#member-registration');

        if (!$form.length) {
            return;
        }

        $form.submit(function (e) {
            var email1 = $form.find('input[name="jform[email1]"]').val();

            var $username = $($form.find('input[name="jform[username]"]'));
            $username.val(email1);

            var $name = $($form.find('input[name="jform[name]"]'));

            if ($name.val().length < 1) {
                $name.val('name');
            }
            // e.preventDefault();
        });
        $(this).trigger('submit');
    };

    /**
     * Attaches popovers to fields.
     *
     * Since labels are hidden, but contain needed hints
     * we get the popover text from labels and use for fields popovers
     *
     * @param   JQuery Object  $parent  Context to look
     *
     * @return   bool  Always false
     */
    $.teaching.attachPopovers = function ($parent) {
        $parent.find('.hasPopover').each(function () {
            var $this = $(this);
            var $label = $this.parent().find('label');
            if ($label.length < 0) {
                return;
            }

            var title = $label.attr('title') || $label.data('original-title') || $label.text();

            if (!title || title.length < 0) {
                return;
            }

            var tmp = title.split('</strong><br />');

            var content = $label.data('content');

            if ((!content || !content.length) && tmp.length > 1) {
                title = tmp[0] + '</strong>';
                content = content + tmp[1];
            }

            // content = content + $label.data('content');
            // $this.data('title', title);
            $this.data('content', content);
            $this.data('toggle', 'popover');
            $this.data('placement', 'auto');
            $this.data('trigger', 'hover');
            $this.data('html', 'true');

            $this.popover();
        });
    };

    /**
     * Creates fields placeholders based on labels
     *
     * By default Joomla fields have labels shown and no input placeholders.
     * We hide labels using layout overrides in our template, but we need placholders.
     * Rather then we change/override a number of XML to add hint="true" parameter,
     * we search for all hidden labels (according to BS3 markup .sr-only) and
     * make the placeholders to based on the labels text
     *
     * @param   JQuery Object  $parent  Context to look
     *
     * @return   void
     */
    $.teaching.hintOn = function ($parent) {
        $parent.find('label.sr-only').each(function () {
            var $label = false || $(this);

            var placeHolder = $label.text().trim();
            var $input = $label.parent().find('input,textarea');

            var lastchar = placeHolder.substr(placeHolder.length - 1, 1);

            if (lastchar == ":" || lastchar == "*") {
                placeHolder = placeHolder.substr(0, placeHolder.length - 1).trim();
            }

            $input.attr('placeholder', placeHolder);
        });
    };

    /**
     * This is a very tricky and not clear to Gruz how and why it works function
     *
     * Chosen plugin renders chosen selects on a hidden tab with zero width
     * Check here http://stackoverflow.com/questions/25983906/width-of-chosen-dropdowns-near-zero-when-starting-in-collapsed-bootstrap-accordi
     * When we visit a page with tabs, then tabs are hidden and visible several times during DOM generation
     * So for the problematic chosen selects with width zero we have to recalculate and reset it.
     * Gruz doesn't know why, but that setTimeout is a must (at least in FF)
     * This was very tricky to build this function.
     *
     * @param   JQuery Object  $parent  Context to look
     *
     * @return   void
     */
    $.teaching.tabsChosenFix = function ($parent) {

        // Stupid, but would not work without wrapping into timeout, even if it's a zero one
        setTimeout(function () {

            $parent.find('select').each(function () {
                // Re-calculate width of every select wrapper. Hidden selects width is
                // calculated as 0 by 'chosen' plugin.
                var $select = $(this);

                $select.siblings('.chzn-container').each(function () {

                    var $chzn = $(this);
                    if ($chzn.css('width') == '0px') {
                        var width = realWidth($select);
                        var width2 = $select.eq(0).width();

                        if (width2 > 0) {
                            width = width2;
                        }

                        $chzn.css('width', width);
                    }
                });
            }, 0); // How long do you want the delay to be (in milliseconds)?
        });

        /**
         * Calculates real width of an element.
         *
         * @param {object} $element
         *   jQuery object.
         *
         * @returns {string}
         *   Element width string.
         */
        function realWidth($element) {
            var $clone = $element.clone();
            $clone.css("visibility", "hidden");
            $element.closest('form').append($clone);
            var width = $clone.outerWidth();
            $clone.remove();
            return width;
        }
    };

    /**
     * This is a very tricky and not clear to Gruz how and why it works function
     *
     * @param   JQuery Object  $parent  Context to look
     *
     * @return   void
     */
    $.teaching.tabsMakeDependant = function ($parent) {
        // Add listeners to core jQuery methods used by Joomla showon
        // Due to this approach we are able to catch the moment when Joomla showon
        // hides a tab heading, and we hide-show the appropriate tab
        // ;(function($){
        // 		var orig_slideUp = $.fn.slideUp;

        // 		$.fn.slideUp = function(e){
        // 				$(this).trigger('slideUp');
        // 				orig_slideUp.apply(this, Array.from(arguments));
        // 		};
        // })(jQuery);

        // ;(function($){
        // 		var orig_slideDown = $.fn.slideDown;

        // 		$.fn.slideDown = function(e){
        // 				$(this).trigger('slideDown');
        // 				orig_slideDown.apply(this, Array.from(arguments));
        // 		};
        // })(jQuery);

        var selector = 'li.nn_tabs-tab[data-showon]';

        function changeTabsState(elem) {
            var showon = elem.data('showon'),
                elements = $('[name*="' + showon[0].field + '"]');

            //debugger;

            $.each(elements, function () {
                var id = $(this).val().replace(/_/g, '-');
                var link = $('#tab-' + id); //$this.find('a.rl_tabs-toggle').data('id');
                var tab = $('div#' + id);

                if ($(this).is(':checked')) {
                    //debugger;
                    // tab.show().find('div').show();  //<div>
                    // link.show() // <a>
                    //     .parent().show(); //.addClass('active'); // <li>
                    link.tab('show');
                } else {
                    //debugger;
                    //link.attr('data-toggle', 'none');
                    // tab.find('div').hide();
                    // link.parent().hide();
                }
            });
        }

        /*activate tab */
        $parent.find(selector).each(function (i) {
            var _this = $(this),
                showon = _this.data('showon'),
                depend_elements = $('[name*="' + showon[0].field + '"]');

            if ($(depend_elements[i]).is(':checked')) {
                var link = $('a', _this),
                    tabId = link.data('id'), /* $this.find('a.rl_tabs-toggle').data('id');*/
                    tab = $('#' + tabId);

                tab.find('div').show();  //<div>
                link.show();  // <a>
                _this.show();  // <li>
                link.tab('show');
            }

            // $('a', _this).on('shown.bs.tab hidden.bs.tab', function (e) {
            //     console.log('catch ' + e.target + ' ' + e.type );
            // });

            $(depend_elements[i]).on('change', function () {
                var check = $.inArray($(this).val(), showon[0].values);
                if (check != -1) {
                    if ($(this).is(":checked")) {
                        $('a', _this).tab('show');
                    } else {
                        var active_tab = $('a', _this.siblings()).filter(':visible');
                        active_tab.tab('show');
                    }
                }
            });
        });

        $(selector).on('slideDown slideUp', function (e) {
            var $this = $(this);


            var tabid = $this.find('a.rl_tabs-toggle').data('id');
            var $tab = $('#' + tabid);

            switch (e.type) {
                // Close
                case 'slideUp':
                    $tab.css('display', 'none');

                    var siblings = $this.siblings(':visible');

                    if (siblings.length) {
                        var $another_element = $(siblings[0]);
                        var another_element_id = $another_element.find('a.rl_tabs-toggle').data('id');

                        RegularLabsTabs.show(another_element_id);

                        // ~ var $another_tab = $('#' + another_element_id);

                        // ~ $another_element.addClass('active');
                        // ~ $another_tab.addClass('active');
                    }

                    $this.removeClass('active');
                    $tab.removeClass('active');

                    break;

                // Open
                case 'slideDown':
                    $tab.css('display', '');

                    break;
                default:

            }
        });
    };

    /**
     * Chains rates
     *
     * @param   JQuery Object  $parent  Context to look
     *
     * @return   void
     */
    $.teaching.chainRates = function ($parent) {
        $parent.find('.price-matrix').each(function (index, row) {
            var inputs = $(row).find('input.form-control'),
                $base = $(inputs[0]);

            for (var i = 1; i < inputs.length; i++) {
                var $input = $(inputs[i]);
                if ($base.val() != $input.val()) {
                    $base.removeClass('price-chain-editing');
                    $base.closest('td').find('a.chain-status').addClass('inactive');
                }
                $base = $input;
            }
        });

        $parent.find('a.chain-status').on('click', function (e) {
            e.preventDefault();
            var $current = $(e.target).closest('a.chain-status');
            var $input = $current.closest('td').find('input.form-control');

            $current.toggleClass('inactive')
            $input.toggleClass('price-chain-editing');
        });

        $parent.find('input.price-chain').on('keyup keypress blur change', function (e) {
            var $this = $(e.target);
            var val = $this.val();
            var $inputs = $this.closest('td').nextAll().find('input.form-control');
            for (var i = 0; i < $inputs.length; i++) {
                var $i = $($inputs.get(i));
                $i.val(val);
                if (!$i.hasClass('price-chain-editing')) break;
            }
        });
    };

    /*$.teaching.attachPopoverToSubmitButton = function ($parent)
    {
        if (null === $parent){
            return;
        }

        $.each(Joomla.getOptions('validationary').forms, function(formSelector, formOptions)
        {
            $parent.find(formSelector).each(function(){
                var $form = $(this);

                $form.find('label').each(function(index, element){
                    var id = $(element).attr('id') || '';
                    var link_id = id + '_' + index;
                    if ('' !== id)
                    {
                        $(element).wrap('<a id="' + link_id +'"class="link_label" href="javascript:void(0)"></a>');
                    }
                });

                var submit_button = $form.find('button[type="submit"]');

                if (submit_button.length > 0)
                {
                    submit_button.wrap( '<div id="button-container"></div>' );
                    submit_button.after('<div id="popover-area"></div>');
                    var button_container = $('#button-container');
                    var popover_area = $('#popover-area');
                    var string_message = '';

                    button_container.on('mouseover', function ()
                    {
                        if (submit_button.is(':disabled'))
                        {
                            popover_area.removeClass('hide');
                            var arr = [];
                            $form.find('input:invalid, .error[name]').each(function(){
                                var $this = $(this);
                                if ($this.hasClass('rEXAMPLEequired'))
                                {
                                    return;
                                }
                                var anchor_id =$this.parents('.form-group:first').find('a').attr('id');
                                var label_content = $this.parents('.form-group:first').find('label').attr('data-content');
                                var popover_title = $this.parents('.form-group:first').find('.popover-title').html() || '';

                                if (label_content && label_content.length > 100)
                                {
                                    label_content = $this.parents('.form-group').find('label').attr('data-original-title') || 'Error';

                                    if ('' !== popover_title)
                                    {
                                        label_content = label_content + ': ' + popover_title;
                                    }
                                }
                                if (!!label_content && label_content.length) {
                                    if (anchor_id !== undefined) {
                                        arr.push('<a class="link_to_label" href="#' + anchor_id + '">' + label_content + '</a>');
                                    }
                                    else {
                                        anchor_id = $(this).attr('id');
                                        arr.push('<a class="link_to_label" href="#' + anchor_id + '">' + label_content + '</a>');
                                    }
                                }
                            });

                            string_message = arr.toString();
                            string_message = string_message.replace(/,/g,"<br>");

                            popover_area.popover({
                                trigger: 'manual',
                                placement: 'top',
                                container: 'body',
                                toggle: 'popover',
                                html:'true',
                                content: function() {
                                    return string_message;
                                }
                            });
                            popover_area.popover('show');
                        }
                        else
                        {
                            popover_area.addClass('hide');
                        }

                        $('.link_to_label').click(function() {
                            var $this = $(this);
                            var id = '' + $this.attr('href');
                            var label = $(id) || null;
                            if (null !== label){
                                if (!label.parents('div.tab-pane').hasClass('active')){
                                    $('div.tab-pane').removeClass('active');
                                    $('.nn_tabs-tab').removeClass('active');
                                    label.parents('div.tab-pane').addClass('active');
                                    var tab = label.parents('div.tab-pane').attr('id');
                                    tab = $('#tab-' + tab);
                                    tab.parent().addClass('active');
                                }
                            }
                            setTimeout(function () {
                                                        popover_area.popover('hide');
                            }, 100);
                        });

                    });

                    button_container.on('mouseleave', function () {
                        setTimeout(function () {
                            if($( ".popover:not(:hover)" ).length)
                            {
                                popover_area.popover('hide');
                            }
                            $( ".popover" ).mouseleave(function () {
                                popover_area.popover('hide');
                            });
                        }, 500);
                    });
                }
            });
        });
    };
*/
    /*$.teaching.highlighTabWithWrongFields = function ($parent)
    {
        if (null === $parent){
            return;
        }

        $.each(Joomla.getOptions('validationary').forms, function(formSelector)
        {
            $parent.find(formSelector).each(function()
            {
                var $form = $(this);

                $form.find('input').each(function(index, element)
                {
                    findErrors();
                    $(element).on('ClassChanged', function ()
                    {
                        findErrors();
                        $form.find('.nn_tabs-pane').each(function(index, element)
                        {
                            var errors = $(element).find('input:invalid, .error').length;
                            if (errors === 0)
                            {
                                var tab = $('#tab-' + $(element).attr('id'));
                                tab.parent().removeClass('red');
                            }
                        });
                    });
                    $(element).on('remove', function ()
                    {
                        findErrors();
                        $form.find('.nn_tabs-pane').each(function(index, element)
                        {
                            var errors = $(element).find('input:invalid, .error').length;
                            if (errors === 0)
                            {
                                var tab = $('#tab-' + $(element).attr('id'));
                                tab.parent().removeClass('red');
                            }
                        });
                    });
                });

                function findErrors()
                {
                    $form.find('input:invalid, .error').each(function(index, element)
                    {
                        var pane_id = $(element).parents('.nn_tabs-pane').attr('id');
                        var tab = $('#tab-' + pane_id);
                        tab.parent().addClass('red');
                    });
                }

                ;(function($){
                        var orig_addClass = $.fn.addClass;

                        $.fn.addClass = function(){
                            var result = orig_addClass.apply(this, arguments);
                            $(this).trigger('ClassChanged');

                            return result;
                        };
                })(jQuery);
                ;(function($){
                        var orig_remove = $.fn.remove;

                        $.fn.remove = function(){
                            var result = orig_remove.apply(this, arguments);
                            $(this).trigger('remove');

                            return result;
                        };
                })(jQuery);
            });
        });

    };*/
    /*$.teaching.showAlertForUnsavedData = function ($parent)
    {
        if (null === $parent){
            return;
        }
        $('#button-container').before('<div id="alert-warning" class="alert alert-warning" style="display:none;"><a class="close" data-dismiss="alert">×</a><span>Please, save your changes.</span></div>');


        $.each(Joomla.getOptions('validationary').forms, function(formSelector)
        {
            $parent.find(formSelector).each(function()
            {
                var $form = $(this);

                if($form.attr('id') == 'member-registration')
                {
                    return;
                }

                var obj_old = {};


                $form.find('input[type!="checkbox"], select, textarea').each(function(index, element)
                {
                    var id = $(element).attr('id') || null;
                    var val = $(element).val() || null;
                    if (null !== id)
                    {
                        obj_old[id] = val;
                    }
                });
                $form.find('input[type="checkbox"]').each(function(index, element)
                {
                    var id = $(element).attr('id') || null;
                    var is_cheked = $(element).prop('checked');
                    if (null !== id)
                    {
                        if (is_cheked)
                        {
                            obj_old[id] = 1;
                        } else {
                            obj_old[id] = 0;
                        }
                    }
                });
                $form.on('mouseup', '.group-add', function()
                {
                    showAlert();
                });
                $form.on('mouseup', '.group-remove', function()
                {
                    var count_group_remove = $(this).closest('.subform-repeatable-wrapper').find('.group-remove').length;
                    var count_group_add = $(this).closest('.subform-repeatable-wrapper').find('.group-add').length;
                    if (1 < count_group_remove || (1 === count_group_remove && 2 ===  count_group_add))
                    {
                        // ##mygruz20161122132248 Comment this, as it kills confirmation alert but doesn't seem to be needed
                        // $(this).closest('.subform-repeatable-group').remove();
                        showAlert();
                    }
                });
                $form.on('mouseup', '.teaching-group-remove', function()
                {
                    var count_group_remove = $(this).closest('.teaching-subform-repeatable-wrapper').find('.teaching-group-remove').length;
                    var count_group_add = $(this).closest('.teaching-subform-repeatable-wrapper').find('.teaching-group-add').length;
                    if (1 < count_group_remove || (1 === count_group_remove && 2 ===  count_group_add))
                    {
                        // ##mygruz20161122132248 Comment this, as it kills confirmation alert but doesn't seem to be needed
                        // $(this).closest('.teaching-subform-repeatable-group').remove();
                        showAlert();
                    }
                });
                $form.on('change datechange', 'input, select, textarea', function()
                {
                    showAlert();
                });

                function showAlert()
                {
                    var obj_new = {};
                    var show = false;
                    $form.find('input[type!="checkbox"], select, textarea').each(function(index, element)
                    {
                        var id = $(element).attr('id') || null;
                        var val = $(element).val() || null;
                        if (null !== id)
                        {
                            obj_new[id] = val;
                        }
                    });
                    $form.find('input[type="checkbox"]').each(function(index, element)
                    {
                        var id = $(element).attr('id') || null;
                        var is_cheked = $(element).prop('checked');
                        if (null !== id)
                        {
                            if (is_cheked) {
                                obj_new[id] = 1;
                            } else {
                                obj_new[id] = 0;
                            }
                        }
                    });
                    for (var key_old in obj_old)
                    {
                        for (var key_new in obj_new)
                        {
                            if (key_old === key_new && obj_old[key_old] !== obj_new[key_new])
                            {
                                show = true;
                            } else if (key_old === key_new && obj_old[key_old] === obj_new[key_new])
                            {
                                $('#alert-warning').hide();
                            }
                        }
                    }
                    for (var key_new in obj_new)
                    {
                        if (!(key_new in obj_old) && null !== obj_new[key_new]){
                            show = true;
                        }
                    }
                    for (var key_old in obj_old)
                    {
                        if (!(key_old in obj_new)){
                            show = true;
                        }
                    }
                    if (show)
                    {
                        $('#alert-warning').show();
                    }
                }

            });
        });

    };*/

    $.teaching.toggleReceiveSMSCheckbox = function () {
        var phonefield = $('#jform_profile_default_phone');
        var smsCheckbox = $('#jform_profile_default_recieve_sms').parents('div.checkbox');

        if (phonefield.val() === '') {
            smsCheckbox.hide();
        }

        phonefield.keyup(function () {
            if ($(this).val() !== '') {
                smsCheckbox.show();
            } else {
                smsCheckbox.hide();
            }
        });

    };

    $.teaching.timezone = function ($parent) {
        var $e = $parent.find('#jform_profile_default_params_timezone');
        if ($e.val() == "") {
            //debugger;
            $e.val(moment.tz.guess(true));
            $e.trigger('chosen:updated');
            $e.trigger('change');
        }
    };

    $.teaching.changeEmail = function (btn) {
        let field = $('#jform_new_email');
        if ($(field).parent().hasClass('hide'))
            $(field).parent().removeClass('hide');
        else
            $(field).parent().addClass('hide');
        return $(field).parent().hasClass('hide');
    };

    $.teaching.meeting = {
        join: function (event) {
            getMeetingData(event);
        }
    };

    function getMeetingData(event) {
        $.ajax({
            url: '/index.php?option=com_teaching&task=meeting.join&id=' + event.id.toString() + '&' + Joomla.getOptions('teaching').token + '=1',
            headers: {
                'Accept': 'application/json'
            },
            dataType: 'json',
            cache: false,
            success: function (data) {
                if (data.data.redirect && (data.data.redirect.indexOf('calendar') === -1 || data.data.redirect.indexOf('calendar') > 1)) {
                    setTimeout(function () {
                        location.href = data.data.redirect;
                    }, 2000);
                } else {
                    eventWorker.stopWork();
                }

                if(!data.success) {
                    if(data.data.retry) {
                        setTimeout(function (){
                            getMeetingData(event);
                        }, 10000);
                    } else {
                        Notifier.warning(Joomla.JText._('COM_TEACHING_MEETING_START_ERROR'));
                    }
                }
            },
            error: function (xhr, code) {
                $('.modal.fade, .modal-backdrop').remove();
                $('body, html').animate({
                    scrollTop: 0
                }, 400, function (){
                    $('#system-message-container').html('<div id="system-message">\n' +
                        '        <div class="alert alert-warning">\n' +
                        '            <a class="close" data-dismiss="alert">×</a>\n' +
                        '            <div>\n' +
                        '                <div class="alert-message">Please reload the page</div>\n' +
                        '            </div>\n' +
                        '        </div>\n' +
                        '    </div>');
                });
            }
        });
    }

    $.teaching.addTimezonePicker = function () {
        return
    };


    $.teaching.confirmationModal = function (text, url) {
        const options =
            {
                header: 'Confirmation',
                modalSelector: 'delete-rol',
                body: `<span>${text}</span>`,
                body_append: `<div class="text-right">
                                <button id="mod_yes_btn" type="button" class="btn btn-primary">${Joomla.JText._('JYES')}</button>
                                <button id="mod_no_btn" type="button" class="btn">${Joomla.JText._('JNO')}</button>
                              </div>`,
                footer_close_button: false
            };

        const modal = $.teaching.runModal(options);
        let elm = $('select', modal);

        if ( elm.length ) {
            elm.selectize({
                create: true,
                sortField: {
                    direction: "asc"
                }
            });
        }

        $('#mod_yes_btn', modal).on('click', function (e) {
            location.href = url;
        });


        $('#mod_no_btn', modal).on('click', function (e) {
            modal.modal('hide');
        });
    };

    $(document).on('click', 'a[data-confirm-yes-no]', function (e) {
        e.preventDefault();
        e.stopPropagation();
        const href = $(this).attr('href');
        const url = location.protocol + "//" + location.host + href;
        const msg =$(this).data('confirm-text');

        $.teaching.confirmationModal(msg, url);
    });

    $.teaching.ratingConfirmationModal = function (text, url) {
        const options =
            {
                modalSelector: 'ratingChanges',
                body_append: `<div class="text-right">
                                <button id="mod_yes_btn" type="button" class="btn btn-primary">
                                    ${Joomla.JText._('COM_CONTENT_RATING_DELETE_MODAL_CONFIRM')}
                                </button>
                                <button id="mod_no_btn" type="button" class="btn">
                                    ${Joomla.JText._('COM_CONTENT_RATING_DELETE_MODAL_CANCEL')}
                                </button>
                            </div>`,
                body: `<span>${text}</span>`,
                header: `${Joomla.JText._('COM_CONTENT_RATING_DELETE_MODAL_HEADER')}`,
                footer_close_button: false
            };

        const modal = $.teaching.runModal(options);
        let confirmClick = false;

        $('#mod_yes_btn', modal).on('click', function (e) {
            confirmClick = true
            jQuery.ajax({
                url: url,
                type: 'post',
                async: true,
                success: function(response) {
                    var activeItem = $('input[id^=jform_profile_tutor_rate_types].active');
                    if ( response.success ) {
                        activeItem.attr('data-checked', '2');
                    } else {
                        resetActiveCheckbox(activeItem)
                    }
                    modal.modal('hide');
                }
            });
        });

        $('#mod_no_btn', modal).on('click', function (e) {
            var activeItem = $('input[id^=jform_profile_tutor_rate_types].active');
            resetActiveCheckbox(activeItem);
            modal.modal('hide');
        });

        $('#ratingChanges').on('hidden.bs.modal', function () {
            if(!confirmClick){
                var activeItem = $('input[id^=jform_profile_tutor_rate_types].active');
                resetActiveCheckbox(activeItem);
            }
        });

        function resetActiveCheckbox(activeItem) {
            activeItem.click();
            activeItem.removeClass('active');
        }
    };

    $.teaching.pdf_modal = function (file) {
        let pdfOptions =
            {
                modalSelector: 'pdf-modal',
                body: `<div class="pdfModalContent">${file}</div>`,
                header_html: `<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>`,
                footer_close_button: false
            };

        jQuery.teaching.runModal(pdfOptions);
    };

    $.teaching.instant_tutoring = function (btn = false) {
        // var btn = $(this),
        let val = btn.data('instant-tutoring');
        //     val = tutoring_state ? tutoring_state : btn.data('instant-tutoring');
        switch (val) {
            case 'on':
                $.ajax({
                    url: Joomla.getOptions('teaching').base_url + '/index.php?option=com_ajax&module=teaching_instant_tutoring&method=instantTutoringOnTmpl&format=json&' + Joomla.getOptions('teaching').token + '=1',
                    dataType: 'json',
                    success: function (data) {
                        if (data.result) {

                            var options =
                                {
                                    body_append: '<div class="text-right"><button type="button" class="btn btn-success">' + Joomla.JText._('JSAVE') + '</button></div>',
                                    modalSelector: 'instant-tutoring',
                                    body: data.html,
                                    header: Joomla.JText._('MOD_TEACHING_INSTANT_TUTORING'),
                                    header_html: `<span class="instant_tutoring_txt">${Joomla.JText._('MOD_TEACHING_INSTANT_TUTORING_UNDER_MODAL_TITLE')}</span>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>`,
                                    footer_close_button: false
                                };

                            var modal = $.teaching.runModal(options);

                            $('select', modal).selectize({
                                create: true,
                                sortField: {
                                    direction: "asc"
                                }
                            });

                            $('.datepicker', modal).each(function () {
                                var btn = $(this).next(),
                                    _this = $(this);
                                _this.datepicker({
                                    startDate: _this.val(),
                                    format: 'yyyy-mm-dd',
                                    showOnFocus: false
                                });
                                btn.on('click', function () {
                                    _this.datepicker('show');
                                });
                            });
                            $(modal).on('hidden.bs.modal hidden', function () {
                                if (!$(btn).hasClass('active disabled'))
                                {
                                    $('#instant_tutoring_off').attr('checked',true);
                                }
                            });
                            $('[type="button"]', modal).not(".close").on('click', function (e) {

                                var data = {
                                    start: $('#instant_tutoring_start', modal).val(),
                                    duration: $('#instant_tutoring_duration', modal).val(),
                                    date: $('#instant_tutoring_date', modal).val()
                                };

                                $.ajax({
                                    url: Joomla.getOptions('teaching').base_url + 'index.php?option=com_ajax&module=teaching_instant_tutoring&method=instantTutoringOn&format=json&' + Joomla.getOptions('teaching').token + '=1',
                                    data: data,
                                    method: 'POST',
                                    success: function (data) {
                                        let instant_modal = $('#instant-tutoring').find('.modal-body');
                                        if (data.hasOwnProperty('message') && typeof data.message == 'string' && data.message.length > 0)
                                        {
                                            instant_modal.find('.error_instant').remove();
                                            instant_modal.prepend(`
                                                <div class="error_instant">${data.message}</div>
                                            `);
                                            // Notifier.error(data.message);
                                        }
                                        else if (typeof data == 'string'){
                                            instant_modal.find('.error_instant').remove();
                                            instant_modal.prepend(`
                                                <div class="error_instant">${data}</div>
                                            `);
                                            // Notifier.error(data);
                                        }
                                        if (data.success) {
                                            if ($('#profile-show-result').val() == Joomla.JText._('JNO')) {
                                                Notifier.info(Joomla.JText._('MOD_TEACHING_PROFILE_NOT_INVOLVED_SEARCH'));
                                            }
                                            $('.instant_tutoring_switcher label[for="instant_tutoring_on"]').addClass('active');
                                            $('.instant_tutoring_switcher label[for="instant_tutoring_off"]').removeClass('inactive');
                                            btn.addClass('active disabled').siblings('#instant_tutoring_off').attr('checked', false).removeClass('active disabled');
                                            Notifier.success(Joomla.JText._('MOD_TEACHING_INSTANT_TUTORING_ON'));
                                            modal.modal('hide');
                                        }
                                    },
                                    error: function (jqXHR, textStatus, errorThrown) {
                                        Notifier.error(textStatus);
                                    }
                                });
                            });

                        } else {
                            Notifier.error(data.msg);
                        }
                        var date = new Date();
                        var currentTimeZoneOffsetInHours = date.getTimezoneOffset() / 60;
                        var profile_zone = $('.profile_value').val();

                        if (profile_zone == currentTimeZoneOffsetInHours) {
                            jQuery(document).find('.timezone_note').hide();
                        } else {
                            jQuery(document).find('.timezone_note').append('(UTC' + currentTimeZoneOffsetInHours + ')');
                        }
                    }
                });

                break;
            case 'off':

                $.ajax({
                    url: Joomla.getOptions('teaching').base_url + 'index.php?option=com_ajax&module=teaching_instant_tutoring&method=instantTutoringOff&format=raw&' + Joomla.getOptions('teaching').token + '=1',
                    success: function (data) {
                        if (data === '1') {
                            $('.instant_tutoring_switcher label[for="instant_tutoring_on"]').removeClass('active');
                            $('.instant_tutoring_switcher label[for="instant_tutoring_off"]').addClass('inactive');
                            btn.addClass('active disabled').siblings('#instant_tutoring_on').removeClass('active disabled').attr('checked', false);
                            btn.trigger('bind');
                            Notifier.success(Joomla.JText._('MOD_TEACHING_INSTANT_TUTORING_OFF'));
                        } else {
                            Notifier.error(data);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        Notifier.success(Joomla.JText._('ERROR'));
                    }
                });

        }

    };

    $.teaching.initCustomSelect = function(el) {
        let x, i, j, selElmnt, a, b, c;

        x = el.getElementsByClassName("custom-select");
        for (i = 0; i < x.length; i++) {
            selElmnt = x[i].getElementsByTagName("select")[0];
            a = document.createElement("DIV");
            a.setAttribute("class", "select-selected");
            a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
            x[i].appendChild(a);
            b = document.createElement("DIV");
            b.setAttribute("class", "select-items select-hide");
            for (j = 0; j < selElmnt.length; j++) {
                if (j == selElmnt.selectedIndex)
                {
                    continue;
                }
                c = document.createElement("DIV");
                c.innerHTML = selElmnt.options[j].innerHTML;
                c.addEventListener("click", function(e) {
                    var y, i, k, s, h;
                    s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                    h = this.parentNode.previousSibling;
                    for (i = 0; i < s.length; i++) {
                        if (s.options[i].innerHTML == this.innerHTML) {
                            s.selectedIndex = i;
                            h.innerHTML = this.innerHTML;
                            y = this.parentNode.getElementsByClassName("same-as-selected");
                            for (k = 0; k < y.length; k++) {
                                y[k].removeAttribute("class");
                            }
                            this.setAttribute("class", "same-as-selected");
                            s.value = s.options[i].value;
                            s.dispatchEvent(new Event('change'));
                            break;
                        }
                    }
                    h.click();
                });
                if(selElmnt.options[j].value) {
                    b.appendChild(c);
                }
            }
            x[i].appendChild(b);
            a.addEventListener("click", function(e) {
                e.stopPropagation();
                closeAllSelect(this);
                this.nextSibling.classList.toggle("select-hide");
                this.classList.toggle("select-arrow-active");
            });
        }
    };

    $(document).on('change', 'input[id^=jform_profile_tutor_rate_types]', function (e) {
        if ( !this.checked  && ( this.dataset.checked == 1 ) && parseInt(Joomla.getOptions('events_count')['tid_' + this.value]) > 0) {
            $(this).addClass('active');
            const msg = `${Joomla.JText._('COM_CONTENT_RATING_DELETE_CONFIRM_MODAL')}`;
            const url = "/index.php?option=com_teaching&task=profile.deleteSessionByType&tid=" +
                $(this).val();
            const text = msg.replace('%s', $(this).parent('label').text().trim());
            $.teaching.ratingConfirmationModal(text, url);
        }
    });

    /**
     * Gets Stripe bank account fields from server and makes function call on callback function with given data (fields)
     * @param country
     * @param callback
     * @returns {[]}
     */
    $.teaching.getStripeCountryFields = (country, callback) => {
        let country_fields = [];
        $.teaching.payment_account = $.teaching.payment_account || {};
        if ($.teaching.payment_account.hasOwnProperty(country)) {
            return callback($.teaching.payment_account[country]);
        }
        $.ajax({
            url: "index.php?option=com_teaching&task=profile.getStripeCountryFields&" + Joomla.getOptions('csrf.token') + "=1&country=" + country,
            cache: false,
            headers: {
                'Accept': 'application/json'
            },
            success: (res) => {
                $.teaching.payment_account[country] = res.data;
                if (res.success) {
                    callback(res.data);
                }
            }
        });
        return country_fields;
    };

    $.teaching.getDebitCardFields = () => {
        return {
            countries: {
                title: 'Country',
                name: 'country',
                type: 'Select from available countries',
                options: [
                    {
                        title: 'United states (debit card)',
                        value: 'US'
                    },
                ],
                regex: {}
            },
            debit_card: {
                title: 'Credit/Debit card',
                name: 'debit_card',
                type: '16 digits (1234-4567-7895-1234)',
                regex: {
                    php: '^(([0-9]{16})|([0-9]{4}([\\s\\-][0-9]{4}){3}))$'
                }
            }
        };
    };

    /**
     * Prepares payment account fields for Stripe bank account on tutors account page
     * @param el
     * @returns {{}}
     */
    $.teaching.prepareStripePaymentAccount = (el) => {
        if ($('#jform_profile_tutor_transfer input:checked').val().toLowerCase() !== 'stripe') {
            return true;
        }
        let pa_selector = Joomla.getOptions('teaching').profile.pa_selector;
        let data = $(pa_selector).val();
        $(pa_selector).closest('form').on('submit', (e) => {
            e.preventDefault();
            if (!$.teaching.eventWorker.checkWork()) {
                $.teaching.eventWorker.startWork();
            }
            if ($('#jform_profile_tutor_transfer input:checked').val().toLowerCase() !== 'stripe') {
                return true;
            }
            return $.teaching.validateStripePaymentAccount(e.currentTarget);
        });
        let country = $('#jform_profile_tutor_payment_country').val();
        try {
            if (Joomla.getOptions('teaching').profile.payment.country == country) {
                data = JSON.parse(data);
            } else {
                data = {};
            }
        } catch (e) {
            data = {};
        }
        if (!country) {
            el.innerHTML = '';
            $(pa_selector + '-lbl').hide();
            jQuery(el).closest('form').find('[type="submit"]').trigger('success');
            return {};
        }
        $(pa_selector + '-lbl').show();
        $.teaching.getStripeCountryFields(country, (fields) => {
            el.innerHTML = '';
            let pa_name = jQuery(Joomla.getOptions('teaching').profile.pa_selector).data('field-name');

            if (fields == null || fields.length === 0) {
                $(pa_selector + '-lbl').hide();
                return true;
            }
            $.each(fields, (index, field) => {
                let field_name = field.name.replaceAll(' ', '_');
                let selector = 'jform_profile_tutor_payment_account_stripe_bank_' + field_name;
                let field_value = data != null && typeof data == 'object' && data.hasOwnProperty(field.name) ? data[field.name] : '';
                let form_group = `
                    <div class="form-group col-sm-12 col-md-4 no-left-padding">
                        <label id="${selector}-lbl"
                         for="${selector}" class="showLabel control-label optional hasTooltip"
                          title="" data-original-title="<strong>${field.name}</strong>">${field.name}</label>
                        <input required id="${selector}" name="${pa_name}[${field.name}]" value="${field_value}" class="form-control doNotListenValidate" type="text" />
                    </div>
                `;
                if ((index+1) % 3 === 0) form_group += '<div class="clearfix"></div>';
                el.innerHTML += form_group;
            });
            let ext_errors = Joomla.getOptions('teaching').profile.errors && Joomla.getOptions('teaching').profile.errors.hasOwnProperty('payment_account')
                ? Joomla.getOptions('teaching').profile.errors.payment_account
                : [];

            $.each(fields, (index, field) => {
                let field_name = field.name.replaceAll(' ', '_');
                let selector = 'jform_profile_tutor_payment_account_stripe_bank_' + field_name;
                let field_input = jQuery('#' + selector);
                if (ext_errors.hasOwnProperty(field_name)) {
                    field_input.closest('form').find('[type="submit"]:not(.validation-init)').trigger('failure');
                    field_input.addClass('invalid error');
                    field_input.parent().append('<span class="error-field-text" style="color: red;font-size: 14px;line-height:1.1;display:block;">' + ext_errors[field_name].message + '. Example` ' + field.type + '</span>');
                    field_input.closest('form').find('[type="submit"]:not(.validation-init)').trigger('failure');
                }
                field_input.on('focusout', (e) => {
                    if (field_input[0].classList.contains('error')) {
                        field_input.removeClass('invalid error');
                        field_input.siblings('span.error-field-text').remove();
                    }
                    if (field.regex.hasOwnProperty('php')) {
                        let regex = field.regex.hasOwnProperty('params')
                            ? new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php,
                            field.regex.params)
                            : new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php);
                        if (!regex.test(e.currentTarget.value) || (
                            field.hasOwnProperty('size') && (!field_input[0].value || field_input[0].value.length != field.size)
                        )
                        ) {
                            field_input.closest('form').find('[type="submit"]').trigger('failure');
                            field_input.addClass('invalid error');
                            field_input.parent().append(
                                '<span class="error-field-text" style="color: red;font-size: 14px;line-height:1.1;display:block;">'
                                + Joomla.JText._('COM_TEACHING_INVALID_VALUE') + '. Example` ' + field.type + '</span>'
                            );
                            return false;
                        }
                    } else if (!field_input[0].value || field_input[0].value.length == 0) {
                        field_input.closest('form').find('[type="submit"]').trigger('failure');
                        field_input.addClass('invalid error');
                        field_input.parent().append(
                            '<span class="error-field-text" style="color: red;font-size: 14px;line-height:1.1;display:block;">'
                            + Joomla.JText._('COM_TEACHING_FIELD_REQUIRED') + '. Example` ' + field.type + '</span>'
                        );
                        return false;
                    }

                    if (ext_errors == null || ext_errors.length === 0)
                        field_input.closest('form').find('[type="submit"]').trigger('success');
                    return true;
                });
            });
            $(pa_selector + '-lbl').show();
        });
    };

    $.teaching.validateStripePaymentAccount = (the_form) => {
        if ($('#jform_profile_tutor_transfer input:checked').val().toLowerCase() !== 'stripe') {
            return true;
        }
        let has_error = false;
        let country = $('#jform_profile_tutor_payment_country').val();
        if (!country) {
            jQuery(the_form).find('[type="submit"]').trigger('success');
            the_form.submit();
            return true;
        }
        let selector_prefix = 'jform_profile_tutor_payment_account_stripe_bank_';
        $.teaching.getStripeCountryFields(country, (fields) => {
            if (fields == null || fields.length === 0) {
                the_form.submit();
            }
            $.each(fields, (index, field) => {
                let field_name = field.name.replaceAll(' ', '_');
                let selector = selector_prefix + field_name;
                let field_input = jQuery('#' + selector);
                if (field_input[0].classList.contains('error')) {
                    field_input.removeClass('invalid error');
                    field_input.siblings('span.error-field-text').remove();
                }
                if (field.regex.hasOwnProperty('php')) {
                    let regex = field.regex.hasOwnProperty('params')
                        ? new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php,
                            field.regex.params)
                        : new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php);
                    if (!regex.test(field_input[0].value) || (
                        field.hasOwnProperty('size') && (!field_input[0].value || field_input[0].value.length != field.size)
                    )
                    ) {
                        has_error = true;
                        field_input.closest('form').find('[type="submit"]').trigger('failure');
                        field_input.addClass('invalid error');
                        field_input.parent().append(
                            '<span class="error-field-text" style="color: red;font-size: 14px;line-height:1.1;display:block;">'
                            + Joomla.JText._('COM_TEACHING_INVALID_VALUE') + '. Example` ' + field.type + '</span>'
                        );
                        return false;
                    }
                } else if (!field_input[0].value || field_input[0].value.length == 0) {
                    has_error = true;
                    field_input.closest('form').find('[type="submit"]').trigger('failure');
                    field_input.addClass('invalid error');
                    field_input.parent().append(
                        '<span class="error-field-text" style="color: red;font-size: 14px;line-height:1.1;display:block;">'
                        + Joomla.JText._('COM_TEACHING_FIELD_REQUIRED') + '. Example` ' + field.type + '</span>'
                    );
                    return false;
                }
            });
            if (!has_error) {
                jQuery(the_form).find('[type="submit"]').trigger('success');
                the_form.submit();
                return;
            }
            $.teaching.eventWorker.stopWork();
        });
        return !has_error;
    };

    $.teaching.preparePayPalPaymentAccount = (el) => {
        let pa_selector = Joomla.getOptions('teaching').profile.pa_selector;
        $(pa_selector + '-lbl').hide();
        let data = $(pa_selector).val();
        $(pa_selector).closest('form').on('submit', (e) => {
            e.preventDefault();
            if (!$.teaching.eventWorker.checkWork()) {
                $.teaching.eventWorker.startWork();
            }
            if ($('#jform_profile_tutor_transfer input:checked').val().toLowerCase() !== 'paypal') {
                return true;
            }
            return $.teaching.validatePayPalPaymentAccount(e.currentTarget);
        });

        try {
            data = JSON.parse(data);
        } catch (e) {
            data = {}
        }
        let fields = {
            email: {
                name: 'paypal_email',
                placeholder: 'Email',
                title: Joomla.JText._('COM_TEACHING_PAYPAL_EMAIL'),
                type: 'john@example.com',
                regex: {
                    'php': /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,5})$/
                }
            }
        };
        el.innerHTML = '';
        let pa_name = jQuery(Joomla.getOptions('teaching').profile.pa_selector).data('field-name');

        $.each(fields, (index, field) => {
            let field_name = field.name.replaceAll(' ', '_');
            let selector = 'jform_profile_tutor_payment_account_stripe_bank_' + field_name;
            let field_value = data != null && typeof data == 'object' && data.hasOwnProperty(field.name) ? data[field.name] : '';
            let field_type = field_name.toLowerCase() == 'email' ? 'email' : 'text';
            let form_group = `
                    <div class="form-group col-sm-12 col-md-4 no-left-padding">
                        <label id="${selector}-lbl"
                         for="${selector}" class="showLabel control-label optional hasTooltip"
                          title="" data-original-title="<strong>${field.title}</strong>">${field.title}</label>
                        <input id="${selector}" name="${pa_name}[${field.name}]" value="${field_value}" class="form-control doNotListenValidate" type="${field_type}" placeholder="${field.placeholder}" />
                    </div>
                `;
            if ((index+1) % 3 === 0) form_group += '<div class="clearfix"></div>';
            el.innerHTML += form_group;
        });

        let ext_errors = Joomla.getOptions('teaching').profile.errors && Joomla.getOptions('teaching').profile.errors.hasOwnProperty('payment_account')
            ? Joomla.getOptions('teaching').profile.errors.payment_account
            : [];

        $.each(fields, (index, field) => {
            let field_name = field.name.replaceAll(' ', '_');
            let selector = 'jform_profile_tutor_payment_account_stripe_bank_' + field_name;
            let field_input = jQuery('#' + selector);
            if (ext_errors.hasOwnProperty(field_name)) {
                field_input.closest('form').find('[type="submit"]:not(.validation-init)').trigger('failure');
                field_input.addClass('invalid error');
                field_input.parent().append('<span class="error-field-text" style="color: red;font-size: 14px;line-height:1.1;display:block;">' + ext_errors[field_name].message + '. Example` ' + field.type + '</span>');
                field_input.closest('form').find('[type="submit"]:not(.validation-init)').trigger('failure');
            }
            field_input.on('focusout', (e) => {
                if (field_input[0].classList.contains('error')) {
                    field_input.removeClass('invalid error');
                    field_input.siblings('span.error-field-text').remove();
                }
                if (!field_input[0].value.length) {
                    return true;
                }
                if (field.regex.hasOwnProperty('php')) {
                    let regex = field.regex.hasOwnProperty('params')
                        ? new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php,
                            field.regex.params)
                        : new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php);
                    if (!regex.test(e.currentTarget.value)) {
                        field_input.closest('form').find('[type="submit"]').trigger('failure');
                        field_input.addClass('invalid error');
                        field_input.parent().append('<span class="error-field-text" style="color: red;font-size: 14px;line-height:1.1;display:block;">' + Joomla.JText._('COM_TEACHING_PAYPAL_INVALID_EMAIL') + '. Example` ' + field.type + '</span>');
                        return false;
                    }
                }

                if (ext_errors == null || ext_errors.length === 0)
                    field_input.closest('form').find('[type="submit"]').trigger('success');
                return true;
            });
        });

    };

    $.teaching.validatePayPalPaymentAccount = (the_form) => {
        if ($('#jform_profile_tutor_transfer input:checked').val().toLowerCase() !== 'paypal') {
            return true;
        }
        let has_error = false;

        let fields = {
            email: {
                name: 'paypal_email',
                placeholder: Joomla.JText._('COM_TEACHING_EMAIL'),
                type: 'john@example.com',
                regex: {
                    'php': /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,5})$/
                }
            }
        };

        $.each(fields, (index, field) => {
            let field_name = field.name.replaceAll(' ', '_');
            let selector = 'jform_profile_tutor_payment_account_stripe_bank_' + field_name;
            let field_input = jQuery('#' + selector);
            if (field_input[0].classList.contains('error')) {
                field_input.removeClass('invalid error');
                field_input.siblings('span.error-field-text').remove();
            }
            if (!field_input[0].value.length) {
                return true;
            }
            if (field.regex.hasOwnProperty('php')) {
                let regex = field.regex.hasOwnProperty('params')
                    ? new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php,
                        field.regex.params)
                    : new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php);
                if (!regex.test(field_input[0].value)) {
                    has_error = true;
                    field_input.closest('form').find('[type="submit"]').trigger('failure');
                    field_input.addClass('invalid error');
                    field_input.parent().append('<span class="error-field-text" style="color: red;font-size: 14px;line-height:1.1;display:block;">' + Joomla.JText._('COM_TEACHING_PAYPAL_INVALID_EMAIL') + '. Example` ' + field.type + '</span>');
                    if ($.teaching.eventWorker.checkWork()) {
                        $.teaching.eventWorker.stopWork();
                    }
                    return false;
                }
            }
        });
        if (!has_error) {
            the_form.submit();
            return true;
        }
        $.teaching.eventWorker.stopWork();
        return !has_error;
    };

    $.teaching.preparePayoneerPaymentAccount = (el) => {
        let pa_selector = Joomla.getOptions('teaching').profile.pa_selector;
        $(pa_selector + '-lbl').hide();
        let data = $(pa_selector).val();
        console.log(pa_selector);
        $(pa_selector).closest('form').on('submit', (e) => {
            e.preventDefault();
            if (!$.teaching.eventWorker.checkWork()) {
                $.teaching.eventWorker.startWork();
            }
            if ($('#jform_profile_tutor_transfer input:checked').val().toLowerCase() !== 'payoneer') {
                return true;
            }
            return $.teaching.validatePayoneerPaymentAccount(e.currentTarget);
        });

        try {
            data = JSON.parse(data);
        } catch (e) {
            data = {}
        }
        let fields = {
            email: {
                name: 'payoneer_email',
                placeholder: 'Email',
                title: Joomla.JText._('COM_TEACHING_PAYONEER_EMAIL'),
                type: 'john@example.com',
                regex: {
                    'php': /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,5})$/
                }
            }
        };
        el.innerHTML = '';
        let pa_name = jQuery(Joomla.getOptions('teaching').profile.pa_selector).data('field-name');

        $.each(fields, (index, field) => {
            let field_name = field.name.replaceAll(' ', '_');
            let selector = 'jform_profile_tutor_payment_account_stripe_bank_' + field_name;
            let field_value = data != null && typeof data == 'object' && data.hasOwnProperty(field.name) ? data[field.name] : '';
            let field_type = field_name.toLowerCase() == 'email' ? 'email' : 'text';
            let form_group = `
                    <div class="form-group col-sm-12 col-md-4 no-left-padding">
                        <label id="${selector}-lbl"
                         for="${selector}" class="showLabel control-label optional hasTooltip"
                          title="" data-original-title="<strong>${field.title}</strong>">${field.title}</label>
                        <input id="${selector}" name="${pa_name}[${field.name}]" value="${field_value}" class="form-control doNotListenValidate" type="${field_type}" placeholder="${field.placeholder}" />
                    </div>
                `;
            if ((index+1) % 3 === 0) form_group += '<div class="clearfix"></div>';
            el.innerHTML += form_group;
        });

        let ext_errors = Joomla.getOptions('teaching').profile.errors && Joomla.getOptions('teaching').profile.errors.hasOwnProperty('payment_account')
            ? Joomla.getOptions('teaching').profile.errors.payment_account
            : [];

        $.each(fields, (index, field) => {
            let field_name = field.name.replaceAll(' ', '_');
            let selector = 'jform_profile_tutor_payment_account_stripe_bank_' + field_name;
            let field_input = jQuery('#' + selector);
            if (ext_errors.hasOwnProperty(field_name)) {
                field_input.closest('form').find('[type="submit"]:not(.validation-init)').trigger('failure');
                field_input.addClass('invalid error');
                field_input.parent().append('<span class="error-field-text" style="color: red;font-size: 14px;line-height:1.1;display:block;">' + ext_errors[field_name].message + '. Example` ' + field.type + '</span>');
                field_input.closest('form').find('[type="submit"]:not(.validation-init)').trigger('failure');
            }
            field_input.on('focusout', (e) => {
                if (field_input[0].classList.contains('error')) {
                    field_input.removeClass('invalid error');
                    field_input.siblings('span.error-field-text').remove();
                }
                if (!field_input[0].value.length) {
                    return true;
                }
                if (field.regex.hasOwnProperty('php')) {
                    let regex = field.regex.hasOwnProperty('params')
                        ? new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php,
                            field.regex.params)
                        : new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php);
                    if (!regex.test(e.currentTarget.value)) {
                        field_input.closest('form').find('[type="submit"]').trigger('failure');
                        field_input.addClass('invalid error');
                        field_input.parent().append('<span class="error-field-text" style="color: red;font-size: 14px;line-height:1.1;display:block;">' + Joomla.JText._('COM_TEACHING_PAYONEER_INVALID_EMAIL') + '. Example` ' + field.type + '</span>');
                        return false;
                    }
                }

                if (ext_errors == null || ext_errors.length === 0)
                    field_input.closest('form').find('[type="submit"]').trigger('success');
                return true;
            });
        });

    };

    $.teaching.validatePayoneerPaymentAccount = (the_form) => {
        if ($('#jform_profile_tutor_transfer input:checked').val().toLowerCase() !== 'payoneer') {
            return true;
        }
        let has_error = false;

        let fields = {
            email: {
                name: 'payoneer_email',
                placeholder: Joomla.JText._('COM_TEACHING_EMAIL'),
                type: 'john@example.com',
                regex: {
                    'php': /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,5})$/
                }
            }
        };

        $.each(fields, (index, field) => {
            let field_name = field.name.replaceAll(' ', '_');
            let selector = 'jform_profile_tutor_payment_account_stripe_bank_' + field_name;
            let field_input = jQuery('#' + selector);
            if (field_input[0].classList.contains('error')) {
                field_input.removeClass('invalid error');
                field_input.siblings('span.error-field-text').remove();
            }
            if (!field_input[0].value.length) {
                return true;
            }
            if (field.regex.hasOwnProperty('php')) {
                let regex = field.regex.hasOwnProperty('params')
                    ? new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php,
                        field.regex.params)
                    : new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php);
                if (!regex.test(field_input[0].value)) {
                    has_error = true;
                    field_input.closest('form').find('[type="submit"]').trigger('failure');
                    field_input.addClass('invalid error');
                    field_input.parent().append('<span class="error-field-text" style="color: red;font-size: 14px;line-height:1.1;display:block;">' + Joomla.JText._('COM_TEACHING_PAYONEER_INVALID_EMAIL') + '. Example` ' + field.type + '</span>');
                    if ($.teaching.eventWorker.checkWork()) {
                        $.teaching.eventWorker.stopWork();
                    }
                    return false;
                }
            }
        });
        if (!has_error) {
            the_form.submit();
            return true;
        }
        $.teaching.eventWorker.stopWork();
        return !has_error;
    };

    $.teaching.prepareDebitCardPaymentAccount = (el) => {
        if ($('#jform_profile_tutor_transfer input:checked').val().toLowerCase() !== 'debit_card') {
            return true;
        }
        let pa_selector = Joomla.getOptions('teaching').profile.pa_selector;
        $(pa_selector + '-lbl').hide();
        let data = $(pa_selector).val();

        $(pa_selector).closest('form').on('submit', (e) => {
            e.preventDefault();
            if (!$.teaching.eventWorker.checkWork()) {
                $.teaching.eventWorker.startWork();
            }
            if ($('#jform_profile_tutor_transfer input:checked').val().toLowerCase() !== 'debit_card') {
                return true;
            }
            return $.teaching.validateDebitCardPaymentAccount(e.currentTarget);
        });

        try {
            data = JSON.parse(data);
        } catch (e) {
            data = {};
        }
        el.innerHTML = '';
        let fields = $.teaching.getDebitCardFields();
        let pa_name = jQuery(Joomla.getOptions('teaching').profile.pa_selector).data('field-name');

        if (fields == null || fields.length === 0) {
            return true;
        }
        $.each(fields, (index, field) => {
            let field_name = field.name.replaceAll(' ', '_');
            let selector = 'jform_profile_tutor_payment_account_debit_card_' + field_name;
            let field_value = data != null && typeof data == 'object' && data.hasOwnProperty(field.name) ? data[field.name] : '';
            let _field_ = `<input required id="${selector}" name="${pa_name}[${field.name}]" value="${field_value}" class="form-control doNotListenValidate" type="text" />`;
            if (field.hasOwnProperty('options')) {
                let options = field.options.reduce((acc, option) => {
                    return `<option value="${option.value}">${option.title}</option>`;
                }, '');
                _field_ = `<select required id="${selector}" name="${pa_name}[${field_name}]" class="form-control doNotListenValidate">${options}</select>`;
            }
            let form_group = `
                <div class="form-group col-sm-12 col-md-4 no-left-padding">
                    <label id="${selector}-lbl"
                     for="${selector}" class="showLabel control-label optional hasTooltip"
                      title="" data-original-title="<strong>${field.title}</strong>">${field.title}</label>
                    ${_field_}
                </div>
            `;
            if ((index+1) % 3 === 0) form_group += '<div class="clearfix"></div>';
            el.innerHTML += form_group;
        });
        jQuery(el).find('select').selectize();
        let ext_errors = Joomla.getOptions('teaching').profile.errors && Joomla.getOptions('teaching').profile.errors.hasOwnProperty('payment_account')
            ? Joomla.getOptions('teaching').profile.errors.payment_account
            : [];

        $.each(fields, (index, field) => {
            let field_name = field.name.replaceAll(' ', '_');
            let selector = 'jform_profile_tutor_payment_account_debit_card_' + field_name;
            let field_input = jQuery('#' + selector);
            if (ext_errors.hasOwnProperty(field_name)) {
                field_input.closest('form').find('[type="submit"]:not(.validation-init)').trigger('failure');
                field_input.addClass('invalid error');
                field_input.parent().append('<span class="error-field-text" style="color: red;font-size: 14px;line-height:1.1;display:block;">' + ext_errors[field_name].message + '. Example` ' + field.type + '</span>');
                field_input.closest('form').find('[type="submit"]:not(.validation-init)').trigger('failure');
            }
            field_input.on('focusout', (e) => {
                if (field_input[0].classList.contains('error')) {
                    field_input.removeClass('invalid error');
                    field_input.siblings('span.error-field-text').remove();
                }
                if (field.regex.hasOwnProperty('php')) {
                    let regex = field.regex.hasOwnProperty('params')
                        ? new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php,
                            field.regex.params)
                        : new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php);
                    if (!regex.test(e.currentTarget.value) || (
                        field.hasOwnProperty('size') && (!field_input[0].value || field_input[0].value.length != field.size)
                    )
                    ) {
                        field_input.closest('form').find('[type="submit"]').trigger('failure');
                        field_input.addClass('invalid error');
                        field_input.parent().append(
                            '<span class="error-field-text" style="color: red;font-size: 14px;line-height:1.1;display:block;">'
                            + Joomla.JText._('COM_TEACHING_INVALID_VALUE') + '. Example` ' + field.type + '</span>'
                        );
                        return false;
                    }
                }

                if (ext_errors == null || ext_errors.length === 0)
                    field_input.closest('form').find('[type="submit"]').trigger('success');
                return true;
            });
        });
    };

    $.teaching.validateDebitCardPaymentAccount = (the_form) => {
        if ($('#jform_profile_tutor_transfer input:checked').val().toLowerCase() !== 'debit_card') {
            return true;
        }
        let has_error = false;
        let country = $('#jform_profile_tutor_payment_country').val();
        if (!country) {
            jQuery(the_form).find('[type="submit"]').trigger('success');
            the_form.submit();
            return true;
        }
        let fields = $.teaching.getDebitCardFields();
        if (fields == null || fields.length === 0) {
            the_form.submit();
        }
        $.each(fields, (index, field) => {
            let field_name = field.name.replaceAll(' ', '_');
            let selector = 'jform_profile_tutor_payment_account_debit_card_' + field_name;
            let field_input = jQuery('#' + selector);
            if (field_input[0].classList.contains('error')) {
                field_input.removeClass('invalid error');
                field_input.siblings('span.error-field-text').remove();
            }
            if (field.regex.hasOwnProperty('php')) {
                let regex = field.regex.hasOwnProperty('params')
                    ? new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php,
                        field.regex.params)
                    : new RegExp(field.regex.hasOwnProperty('js') ? field.regex.js : field.regex.php);
                if (!regex.test(field_input[0].value) || (
                    field.hasOwnProperty('size') && (!field_input[0].value || field_input[0].value.length != field.size)
                )
                ) {
                    has_error = true;
                    field_input.closest('form').find('[type="submit"]').trigger('failure');
                    field_input.addClass('invalid error');
                    field_input.parent().append(
                        '<span class="error-field-text" style="color: red;font-size: 14px;line-height:1.1;display:block;">'
                        + Joomla.JText._('COM_TEACHING_INVALID_VALUE') + '. Example` ' + field.type + '</span>'
                    );
                    return false;
                }
            }
        });
        if (!has_error) {
            jQuery(the_form).find('[type="submit"]').trigger('success');
            the_form.submit();
            return;
        }
        $.teaching.eventWorker.stopWork();
        return !has_error;
    };

    $.teaching.errorSpan = (msg, line_height='1.1') => {
        return `<span class="validation-error-message error-field-text" style="color: red;font-size: 14px;line-height:${line_height};display:block;">${msg}</span>`;
    };

    $.teaching.addErrorToInput = (input, error_text) => {
        let $input = jQuery(input);
        let line_height = '1.1';
        if ($input.length === 0) {
            return false;
        }
        if ($input[0].tagName.toLowerCase() === 'select' && $input[0].hasOwnProperty('selectize')) {
            $input.siblings('.selectize-control').addClass('selectize-error');
            $input = $input.siblings('.selectize-control').find('.selectize-input');
        }
        if ($input[0].tagName.toLowerCase() == 'input') {
            if ($input.attr('name').includes('[default][dob]')) {
                line_height = '2.1';
                return false;
            }
        }
        $input.addClass('invalid error');
        $input.siblings('span.error-field-text').remove();
        if (typeof error_text === 'object') {
            jQuery.each(error_text, (i, txt) => {
                $input.parent().append($.teaching.errorSpan(txt, line_height));
            });
        } else {
            $input.parent().append($.teaching.errorSpan(error_text, line_height));
        }

    };

    $.teaching.prepareProfileErrors = () => {
        let teaching = Joomla.getOptions('teaching', null);
        if (teaching && teaching.profile && teaching.profile.errors) {
            jQuery.each(teaching.profile.errors, (key, val) => {
                $.teaching.addErrorToInput(jQuery(`[name="${key}"]`), val);
            });
        }
    };
    $.teaching.getUrlParameter = (sParam) => {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
        return false;
    };

    $.teaching.displayCertificateFile = () => {
        $(document).on('click', '.search-tutor-item .uploadCertificateImg', function () {
            let file,
                uploadUrl = $(this).closest('.uploadCertificateUploaded').find('.uploadCertificateImg > img').data('value');

            if (uploadUrl) {
                let fileFormat = uploadUrl.split(/\.(?=[^\.]+$)/)[1];
                if (fileFormat === 'pdf') {
                    file = `<iframe src="${uploadUrl}" frameBorder="0"></iframe>`;
                } else {
                    file = `<img src="${uploadUrl}" />`;
                }
                $.teaching.pdf_modal(file);
            }
        });
    };

})(jQuery);


jQuery(document).ready(function () {
    jQuery('input[id^=jform_profile_tutor_rate_types]')
        .each(function(index) {
            var elm = jQuery(this);
            elm.attr('data-checked', elm.is(':checked') ? 1 : 2);
        });

    jQuery('.start-session').on('click', function(e) {
        e.preventDefault();
        jQuery.teaching.meeting.join(e.target);
    });
    selectizeUniversityAndBook();

    jQuery('div.subform-repeatable').on('subform-row-add', function (e, row) {
        selectizeUniversityAndBook();
    });

    jQuery(document).on('focus', '#instant_tutoring_duration-selectized, #instant_tutoring_start-selectized', function () {
        const modal = jQuery(this).closest('.modal');
        modal.css({pointerEvents: 'none'});
        modal.find('.selectize-dropdown').css({pointerEvents: 'all'});
    });

    jQuery(document).on('blur', '#instant_tutoring_duration-selectized, #instant_tutoring_start-selectized', function () {
        const modal = jQuery(this).closest('.modal');
        setTimeout(function () {
            modal.css({pointerEvents: 'all'});
            modal.find('.selectize-dropdown').css({pointerEvents: 'none'});
        }, 500)
    });
});

function selectizeUniversityAndBook() {
    jQuery('select.ajax-select').each(function () {
        const type = jQuery(this).data('type');
        let sel = jQuery(this).selectize({
            valueField: 'value',
            labelField: 'name',
            searchField: 'name',
            options: [],
            create: false,
            load: function (query, callback) {
                if (!query.length) return callback();
                let selected = [];
                if(type == 'books') {
                    jQuery('select[data-type="books"]').each(function () {
                        if (jQuery(this).val() != '') {
                            selected.push(jQuery(this).val());
                        }
                    });
                }
                const url = `${Joomla.getOptions('teaching').base_url}index.php?option=com_teaching&task=tutor.${type}&${Joomla.getOptions('csrf.token')}=1&q=${query}`;
                jQuery.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'json',
                    data: {
                        name: query,
                        selected: selected
                    },
                    error: function (e) {
                        callback();
                    },
                    success: function (res) {
                        callback(res.data.options);
                    }
                });
            }
        });

        sel.next('div').find('input').on('keyup', function () {
            if (!jQuery(this).val())
                clearSelect(sel);
        })
    });

    function clearSelect(sel) {
        let select_sel = sel[0].selectize;
        // sel[0].innerHTML = '';
        sel.next('div').find('.selectize-dropdown-content').html('');
        select_sel.clearOptions();
        select_sel.clear();
        selectizeUniversityAndBook();
    }
}

jQuery(document).ready(function ($) {

    let filterForm = $('#filterForm');
    $(document).on('click', ".custom-container-balance .pagination li > a.pagenav", function (e) {
        e.preventDefault();
        let start = $(this).attr('href').split('start=')[1];
        createTransactionTable(filterForm, start);
    });

    filterForm.on({
        submit: function (e) {
            e.preventDefault();
            let self = $(this);
            createTransactionTable(self);
        }
    });

    function createTransactionTable(self, start = null){
        let data = self.serialize();
        let start_count = '';
        if (start) {
            start_count = '&start=' + start;
        }
        let url_href = window.location.href.replace(/\#(.*)+$/, '');
        let url = `${url_href}${url_href.includes('?') ? '&format=json' : '?format=json'}${start_count}`;
        $.teaching.eventWorker.startWork();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            success: function (result) {
                let table = result.data.table;
                let title = result.data.title;
                let pagination = result.data.pagination;
                let status_content = result.data.status_content;
                $('.balance-history__table-wrapper').html(table);
                $('.withdrawContent-buttons .balance-title').text(title);
                $('.custom-container-balance [aria-label="Page navigation"]').html(pagination);
                if (status_content) {
                    $('.custom-select-wrapper.operation-status-block div.custom-select').html(status_content);
                    $.teaching.initCustomSelect($('.custom-select-wrapper.operation-status-block')[0]);
                }
                $.teaching.eventWorker.stopWork();
            },
            error: function (error) {
                $.teaching.eventWorker.stopWork();
            }
        });
    }

});


