/*
 jQuery(document).ready(function($) {
 if (Joomla.optionsStorage.teaching.guest) {
 return;
 }

 var logoutTimer,
 logoutTimerInt = function () {
 logoutTimer = setTimeout(function () {
 location.href = '?option=com_users&task=user.logout&'+Joomla.optionsStorage.teaching.token+'=1';
 }, Joomla.optionsStorage.teaching.logout_timer);
 };

 logoutTimerInt();

 $(document).on('mousemove, keydown', function () {
 clearTimeout(logoutTimer);
 logoutTimerInt();
 });

 });
 */

jQuery(document).ready(function ($) {
    if (Joomla.getOptions('teaching').guest) {
        return;
    }

    var idleSeconds = Joomla.getOptions('teaching').logout_timer;
    var idleSecondsCal = Joomla.getOptions('teaching').logout_timer_calendar;
    var logout_timer_countdown = Joomla.getOptions('teaching').logout_timer_countdown;
    var idleTimer;
    var idleTimerCal;

    function resetTimer() {
        clearTimeout(idleTimer);
        idleTimer = setTimeout(whenUserIdle, idleSeconds * 1000);
    }

    function resetTimerCal() {
        clearTimeout(idleTimerCal);
        idleTimerCal = setTimeout(whenUserIdleCal, idleSecondsCal * 1000);
    }

    function whenUserIdleCal() {
        var uid = $('#cal_uid').val();
        var student = $('#cal_student').val();
        $.teaching.refreshCalCells(uid, student);
    }

    function resetCountdown() {
        clearTimeout(tout);
    }

    function logoutPopupC(modal) {
        if (!modal) {
            return;
        }
        var sec = parseInt(modal.find('#logoutsec').html());
        if(isNaN(sec))
        {
            sec = 30;
        }
        sec = sec - 1;
        if (sec < 0) {
            modal.button('loading');
            modal.find('form').submit();
            //modal.modal('hide');
            //logout();
            resetCountdown();
            //closePopup();
        }
        else {
            modal.find('#logoutsec').html(sec);
        }
        window.tout = setTimeout(function () {
            logoutPopupC(modal);
        }, 1000);
    }

    function whenUserIdle() {
        var modalSelector = 'timeout';
        var modal;

        if ($('#' + modalSelector).length) {
            modal = $('#' + modalSelector).modal('show');

        } else {
            //var body_prepend = '<span class="notice">You are inactive for a while and will be logged out of the system in next <span id="logoutsec">'+logout_timer_countdown+'</span> seconds</span>';
            var body_prepend = '<span class="notice">' + Joomla.JText._('COM_TEACHING_LOGOUT_NOTICE').replace('%s', logout_timer_countdown) + '</span>';
            body_prepend += '<br/><br/><p>';
            var body_append = '&nbsp;&nbsp;<button type="button" class="btn btn-primary " data-dismiss="modal">' + Joomla.JText.strings.JCANCEL + '</button></p>';
            // $('body').append('<div class="bg-overlay"></div><div class="cal-popup"><span onclick="closePopup()" id="sbox-btn-close"></span>' + html + '</div>');
            var options =
                {
                    modalSelector: modalSelector,
                    body_prepend: body_prepend,
                    body_append: body_append,
                    header_dismiss: false,
                    url_element_selector: 'div.logout form',
                    url_response_class: 'form-inline pull-left',
                    backdrop: 'static',
                    keyboard: false,
                    header: Joomla.JText._('COM_TEACHING_LOGOUT_HEADER'),
                    footer_close_button: false
                };

            $.ajax({
                url: Joomla.getOptions('teaching').base_url + 'index.php?option=com_users&tmpl=raw&'+Joomla.getOptions('teaching').token + '=1',
                data: {'expired_logout': true},
                success: function (data, success, dataType)
                {
                    options.body = $(data).find('form').hide().parent().html();
                    modal = $.teaching.runModal(options);
                    modal.on('hide.bs.modal', function (e) {
                        resetCountdown();
                        $(this).remove();
                    });
                    window.tout = setTimeout(function () {
                        logoutPopupC(modal);
                    }, 1000);
                }
            });
        }
    }

    $(document).mousemove(resetTimer);
    $(document).click(resetTimer);
    $(document).keyup(resetTimer);

    resetTimer();
    //resetTimerCal();
});
