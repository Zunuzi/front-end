var eventWorker = {
	jobs: 0,
	loader: function () {
		return jQuery('#loader');
	},
	work: false,
	canReload: true,
	checkWork: function () {
		return this.work;
	},
	startWork: function () {
		this.work = true;
		if (this.jobs === 0)
			this.loader().show();
		this.jobs++;
	},
	stopWork: function () {
		this.jobs--;
		if (this.jobs === 0) {
			this.work = false;
			this.loader().hide();
		}
	},
	reservation: null
};

jQuery(document).ready(function($)
{
	// Object to share our data between JQuery blocks
	$.teaching = $.teaching || {};

    $.teaching.addTimezonePicker();
	$.teaching.prepareProfileErrors();

	$('div.logout form').submit(function(e){
		$.teaching.addLoading();
	});
	$.teaching.eventWorker = eventWorker;
	$.teaching.checkAccent = function ($el, $class, $level)
	{
		var a = 'hide', f = $el.closest('form'), l = $level != '' ? $('.'+$level, f) : 0;
		$('select.'+$class, f).each(function() {
			if(this.value == '16') {
				a = 'show';
				return false;
			}
		});
		$el.closest('.form-group')[a]();
		if(l) l[a]();
		if(a == 'hide')
		{
			$el.children().prop('selected', false) && $el.trigger('liszt:updated');
			if(l) l.find('label').removeClass('active').find('input').prop('checked', false);
		}
	};

	$.teaching.repeatUnique = function ($el)
	{
		var ids = [], sels = [];
		$('.subform-repeatable-group', $el).each(function() {
			var sel = $('select:not([data-unique])', this);
			if(!sel.length || sel.hasClass('not-unique')) return true;
			ids.push(sel[0].value);
			sels.push($(sel[0]));
		});
		$.each(sels, function(k, v) {
			v.children().each(function(i, o) {
				o.disabled = o.value != '' && !o.selected && $.inArray(o.value, ids) > -1;
			});
			v.trigger('liszt:updated');
		});
	};

	$('.subform-repeatable').on('change', '.subform-repeatable-group select', function() {
		$.teaching.repeatUnique(
			$(this).closest('.subform-repeatable')
		);
	}).on('click', '.group-add, .group-remove', function() {
		var p = $(this).closest('.subform-repeatable');
		setTimeout(function() {
			$.teaching.repeatUnique(p);
		}, 100);
	}).each(function() {
		$.teaching.repeatUnique($(this));
	});

	// This function is used
	// We need reinitiate our behavor on modals
	// out behaviour on modals
	$.teaching.autoloader = function ($parent)
	{

		$parent = $parent || $('body');

		$parent.find('a[data-toggle="modall"], button[data-toggle="modall"]').click(function(e) {
			e.preventDefault();
			$.teaching.runModal($.teaching.prepareOptionsFromElement(this));
		});

		var $selectbox = $parent.find('.quickselect');

		if ($selectbox.length)
		{
			var breakOutValues = [];
			var max_options = $selectbox.data('showoptionsnumber');

			$selectbox.find('option').each(function(index, element){
				if (max_options == index)
				{
					return false;
				}
				breakOutValues.push(element.value);
			});

			$selectbox.quickselect({
				activeButtonClass: 'btn-primary active',
				breakOutValues: breakOutValues,
				buttonClass: 'btn btn-default',
				DISselectDefaultText: 'More ... ',
				wrapperClass: 'btn-group',
				buttonTag: 'a'
			});
		}

		$.teaching.highlightActiveRadios($parent, ":radio:not(.btn)");
		$.teaching.highlightActiveRadios($parent, ":radio.btn");

		$parent.find('.registration :password').pwstrength({
				ui: { showVerdictsInsideProgressBar: true }
		});

		if ($.validationary)
		{
			$.validationary.validateAjax($parent);
		}

		$.teaching.togglePassword($parent);

		$.teaching.autoPopulateHiddenFieldsonSubmit($parent.find('#member-registration').parent());

		$.teaching.attachPopovers($parent);

		$.teaching.hintOn($parent);

		$.teaching.tabsChosenFix($parent);

		$.teaching.tabsMakeDependant($parent);

		$.teaching.chainRates($parent);

		$.teaching.repeatable($parent);

		//$.teaching.attachPopoverToSubmitButton($parent);

		//$.teaching.highlighTabWithWrongFields($parent);
			
		//$.teaching.showAlertForUnsavedData($parent);

		$.teaching.timezone($parent);
		
		$.teaching.toggleReceiveSMSCheckbox();
	};

	$.teaching.autoloader();

	$.teaching.addLoading = function ()
	{
		$('body').addClass('loading');
	};

	$.teaching.removeLoading = function ()
	{
		$('body').addClass('loading');
	};

	jQuery(document).on('subform-row-add', function(event, row){
		$(row).find('select').selectize({
            create: true,
            sortField: {
                direction: "asc"
            }
        });
	});
	
    $(document).on('keyup', '.form-group', function(e) {
        if (e.which == 9)
        {
        	var field = $(this).find('input');

        	if (!field.length)
			{
				field = $(this).find('textarea');
			}

        	field.focus();

        	return false;
        }
    });

	$('#refillBalanceForm').on('submit', function (e) {
		e.preventDefault();
		$.ajax({
			url: e.currentTarget.getAttribute('action') + '&returnType=raw',
			data: $(e.currentTarget).serializeArray(),
			method: 'post',
			success: function (data) {
				$.teaching.runModal({
					body: data
				});
			}
		})
	});

	$('input#paymentRequestAmount').on('change blur', function (e){
		const min = +this.min;
		if(e.target.value < min) {
			e.preventDefault();
			this.value = '';
		}
	});

	$('form#paymentRequest').on('submit', function (e) {
		// paymentRequestModal
		e.preventDefault();
		let payments = Joomla.getOptions('teaching').payments;
		let balance = Joomla.getOptions('teaching').balance;
		let commission 	= parseInt(payments.commission !== null && payments.commission !== undefined ? payments.commission : 30);
		let amount		= parseInt($(this).find('input#paymentRequestAmount').val() || 0);
		let available	= parseInt(balance.available || 0);
		let giftAvailable	= parseInt(balance.gift_available || 0);
		let min			= parseInt(payments.request_min || 30);
		let form 		= this;
		Joomla.removeMessages();

		if (payments.has_request) {
			Notifier.error(Joomla.JText._('COM_TEACHING_PAYMENT_REQUEST_EXISTS_ALREADY'));
			jQuery('input#payments_label[type="radio"]').trigger('click');
			jQuery([document.documentElement, document.body]).animate({
				scrollTop: jQuery("section.balance-history").offset().top - jQuery('#header').height() - jQuery('.header-second.small-hide').height()
			}, 2000);
			return false;
		}

		if (amount < min) {
			Joomla.renderMessages({
				'error': [
					Joomla.JText._('COM_TEACHING_PAYMENT_MIN_MSG').replace('%s', min)
				]
			});
			location.href = '#main-section';

			return false;
		}

		// if (commission + amount > available) {
		if (amount > available || commission + amount > available + giftAvailable) {
			Joomla.renderMessages({
				'error': [
					Joomla.JText._('COM_TEACHING_PAYMENT_REQUEST_LOW_BALANCE')
				]
			});
			location.href = '#main-section';
			return false;
		}

		let modal = $('#paymentRequestModal');
		let commissionCredit = 0;
		if (commission > 0) {
			let displayCommission = '';
			let information = '';
			if (giftAvailable) {
				if (giftAvailable > commission) {
					information = `${Joomla.JText._('COM_TEACHING_COMMISSION_ONLY_GIFT')}`;
					commissionCredit = 0;
					displayCommission = commission;
				} else {
					information = `${Joomla.JText._('COM_TEACHING_INCLUDING_COMMISSION_CREDIT_GIFT')}`;
					information = information.replace('%s', giftAvailable.toString());
					commissionCredit = commission - giftAvailable;
					displayCommission = commissionCredit;
				}
			} else {
				commissionCredit = commission;
				displayCommission = commissionCredit;
			}

			modal.find('.accept-modal-content').addClass('hasCommission');
			modal.find('.accept-modal-title').text(Joomla.JText._('COM_TEACHING_EARLY_WITHDRAW'));
			modal.find('.accept-modal-note').html(`<span>`+ information +`</span>`);

			modal.find('.accept-modal-price-item-price .requested-amount').html('$ ' + amount);
			modal.find('.accept-modal-price-item-price .commission-modal').html('$ ' + displayCommission);
			modal.find('.accept-modal-price-item-price .total strong').html('$ ' + (amount + commissionCredit));

		} else {
			modal.find('.accept-modal-title').text(Joomla.JText._('COM_TEACHING_WITHDRAW'));
			modal.find('.accept-modal-price-item-text .commission-modal, .total').css("display", "none");
			modal.find('.accept-modal-price-item-price .requested-amount').html('$ ' + amount);
		}

		modal.modal({
			backdrop: 'static'
		}).on('click', 'button.accept-modal-content__confirm', function (e) {
			form.submit();
			// jQuery('#withdrawBtn').toggleClass('my-balance__btn-open');
			// jQuery('#withdraw-txt').slideToggle('hide');
			jQuery('.my-balance__buying-credit').slideToggle('slow');
		});
	});


	$('.land-arrow-box').on('click', function (){
		$('html, body').animate({
			scrollTop: $('.tutoring-jobs').offset().top - 15
		}, 1000);
	});

	// Start Landing Page Animation
	if ( $('.tutoring-jobs').length && $('.steps-section').length ){
		$('.joinfree .land-btn.animate__animated').addClass('animate__pulse');

		const tutoringJobsTop  	 = $('.tutoring-jobs').offset().top - 500;
		const stepsSectionTop 	 = $('.steps-section').offset().top - 500;
		// const signUpBtnTop 	  	 = $('.joinfree-section').offset().top - 500;

		let windowScroll = 0;
		$(window).scroll(function () {
			windowScroll = $(window).scrollTop();
			if (windowScroll >= tutoringJobsTop)
				$('.tutoring-jobs-ul li.animate__animated, .tutoring-jobs-desc span.animate__animated').addClass('animate__pulse');
			if (windowScroll >= stepsSectionTop)
				$('.steps-section-item-title.animate__animated').addClass('animate__pulse');
			// if (windowScroll >= signUpBtnTop)
			// 	$('.joinfree-section .land-btn.animate__animated').addClass('animate__pulse');
		});
	}
	// End Landing Page Animation


});


