function cps_handle_pay_button(publishable_key, account_id, pg_transaction_id) {
    if (typeof window['cp_stripe_session_id_' + pg_transaction_id] != 'undefined') {
        var session_id = window['cp_stripe_session_id_' + pg_transaction_id];
        var stripe = Stripe(publishable_key);
        stripe.redirectToCheckout({sessionId: session_id}).then(function (result) {
            alert(result.error.message);
        });
        return;
    }
    url = '?option=com_payage&task=make_session&aid=' + account_id + '&tid=' + pg_transaction_id + '&tmpl=component&format=raw&nor=1';
    jQuery.ajax({
        url: url,
        dataType: "json",
        type: "GET",
        success: function (responseText, status, xhr) {
            cps_handle_response(responseText);
        },
        error: function (xhr, status, error) {
            cps_handle_response(error + ': ' + xhr.responseText);
        }
    });
}

function cps_handle_response(response) {
    if (typeof response !== 'object') {
        alert('Javascript error: ' + response);
        return;
    }
    if (response['error'] != '') {
        alert(response['error']);
        return;
    }
    if (response['session_id'].length == 0) {
        alert('Stripe Session ID not created');
        return;
    }
    var pg_transaction_id = response['pg_transaction_id'];
    var session_id = response['session_id'];
    window['cp_stripe_session_id_' + pg_transaction_id] = session_id;
    //window.location.href = paymentPage;
    //var stripe = Stripe(response['publishable_key']);
    // stripe.redirectToCheckout({sessionId: session_id}).then(function (result) {
    //     alert(result.error.message);
    // });
}