jQuery(document).ready(function ($) {

    $('a', 'li.group').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();
        var group_id = $(this).attr('data-group'),
            href = $(this).attr('href');
        $.ajax({
            url: '?option=com_ajax&module=teaching_usergroups&format=raw&method=setUserGroup&' + Joomla.optionsStorage.teaching.token + '=1',
            method: 'POST',
            cache: false,
            data: {group_id: group_id},
            success: function (data) {
                if (data == 1) {
                    //location.reload();
                    location.href = location.protocol + "//" + location.host + href;
                } else {
                    Notifier.warning(data, 'Warning');
                    console.log(data);
                }
            },
            error: function (r, e) {
                console.log(e);
            }
        });
    });

    /*$('button.role', 'div.addAnotherRole').on('click', function (e) {
     e.preventDefault();
     var group_id = $(this).attr('data-group');
     $.ajax({
     url: '?option=com_ajax&module=teaching_usergroups&format=raw&method=addUserGroup&' + Joomla.optionsStorage.teaching.token + '=1',
     method: 'POST',
     data: { group_id : group_id },
     success: function (data) {
     if(data == 1)
     {
     location.href = location.protocol + "//" + location.host + '/' + Joomla.optionsStorage.teaching.redirect_url_add_role;
     }
     else
     {
     Notifier.warning(data, 'Warning');
     console.log(data);
     }
     },
     error: function (r,e) {
     console.log(e);
     }
     });

     });*/

});

function addUserRole(group_id) {
    jQuery.ajax({
        url: '?option=com_ajax&module=teaching_usergroups&format=raw&method=addUserGroup&' + Joomla.optionsStorage.teaching.token + '=1',
        method: 'POST',
        cache: false,
        data: {group_id: group_id},
        success: function (data) {
            data = JSON.parse(data);
            if (data.success === true) {
                location.href = location.protocol + "//" + location.host + data.redirect_link;
            } else {
                Notifier.warning(data, 'Warning');
                console.log(data);
            }
        },
        error: function (r, e) {
            console.log(e);
        }
    });

}
