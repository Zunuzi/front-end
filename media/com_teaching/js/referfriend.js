jQuery(function ($) {

    // Start Copy Function
    const copyToClipboard = str => {
        const el = document.createElement('textarea');
        el.value = str;
        document.body.appendChild(el);
        el.select();
        document.execCommand('copy');
        document.body.removeChild(el);
    };

    $('.referral-copy-btn').on('click', function (){
        copyToClipboard($('.referral-copy-text').text());
        Notifier.success(Joomla.JText._('COM_TEACHING_REFERRAL_COPIED'));
    });
    // End Copy Function

    $(document).on('click', '.invite-now', function (){
        $.when(
            $('html, body').animate({
                scrollTop: $('.referral-email-title').offset().top
            }, 1000)
        ).done(function() {
            $('#jform_friend_email').focus();
        });
    });
})