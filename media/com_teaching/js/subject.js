jQuery(document).ready(function ($) {

    $('select.search-subject').on('change', function() {
        if($(this).val()) {
            $(this).parents('#filter-form').find('[type=submit]').attr('disabled', false);
        } else {
            $(this).parents('#filter-form').find('[type=submit]').attr('disabled', true);
        }
    });

    function SubjectAreaListener(parent, children) {
        var $parent;

        $parent = parent.selectize({
            onChange: function (value) {
                let select_province = children[0].selectize;
                children[0].innerHTML = '';
                select_province.disable();
                select_province.clearOptions();
                select_province.clear();
                if (!value.length) return;
                selectProvinceOption(value, children)
            }
        });

        return $parent;
    }

    function selectProvinceOption(value, children) {
        var select_province = children[0].selectize;
        var xhr;
        select_province.disable();
        select_province.clearOptions();
        let req_data = {'subjectarea': value};
        req_data[Joomla.getOptions('teaching').token] = 1;
        select_province.load(function (callback) {
            xhr && xhr.abort();
            xhr = $.ajax({
                url: "?option=com_teaching&task=tutor.subjects",
                data: req_data,
                dataType: "json",
                cache: false,
                method: 'POST',
                success: function (data) {
                    select_province.enable();
                    callback(data.data.options);
                },
                error: function () {
                    callback();
                }
            });
        });
    }

    $('[data-subject-area]').each(function () {
        var parent = $('[name="' + $(this).attr('data-subject-area') + '"]');
        var children = $(this);
        var val = $(this).val();
        $(this).next().html("this button is disabled");
        var $children;
        var $parent;

        $children = children.selectize({
            valueField: 'value',
            labelField: 'text',
            searchField: ['text']
        });
        $parent = SubjectAreaListener(parent, $children);
        parent = $parent[0].selectize;

        if (!val) {
            var selectedSubjectArea = $($parent[0]).val();
            parent.trigger('change', selectedSubjectArea);
        }
    });

    $('div.subform-repeatable').on('subform-row-add', function (e, row) {

        var row_obj = $(row);

        $('[data-subject-area]', this).each(function () {
            if (!$(this).data('parent-listen')) {
                var groupnew = row_obj.data('group'),
                    base_name = row_obj.data('base-name'),
                    group = base_name + 'X',
                    name = $(this).data('subject-area'),
                    new_name = name.replace('[' + group + '][', '[' + groupnew + '][');
                $(this).data('parent-listen', '1');
                var parent = $('[name="' + new_name + '"]');
                if (parent.length) {
                    SubjectAreaListener(parent, $(this).selectize({
                        valueField: 'value',
                        labelField: 'text',
                        searchField: ['text']
                    }));
                }
            }
        });
    });

});
