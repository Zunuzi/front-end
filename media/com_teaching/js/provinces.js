jQuery(document).ready(function ($) {

    function parentListener(parent, children) {
        var $parent;

        $parent = parent.selectize({
            onChange: function (value) {
                let select_province = children[0].selectize;
                children[0].innerHTML = '';
                select_province.disable();
                select_province.clearOptions();
                select_province.clear();
                if (!value.length) return;
                selectProvinceOption(value, children)
            }
        });

        return $parent;
    }

    function selectProvinceOption(value, children) {
        var select_province = children[0].selectize;
        var xhr;
        select_province.disable();
        select_province.clearOptions();
        let req_data = {'country': value};
        req_data[Joomla.getOptions('teaching').token] = 1;
        select_province.load(function (callback) {
            xhr && xhr.abort();
            xhr = $.ajax({
                url: "?option=com_teaching&task=geonames.getProvinces",
                data: req_data,
                dataType: "json",
                cache: false,
                method: 'POST',
                success: function (data) {
                    select_province.enable();
                    callback(data.data.options);
                },
                error: function () {
                    callback();
                }
            });
        });
    }

    $('[data-parent]').each(function () {
        var parent = $('[name="' + $(this).attr('data-parent') + '"]');
        var children = $(this);
        var val = $(this).val();
        $(this).next().html("this button is disabled");
        var $children;
        var $parent;

        $children = children.selectize({
            valueField: 'value',
            labelField: 'text',
            searchField: ['text']
        });
        $parent = parentListener(parent, $children);
        parent = $parent[0].selectize;

        if (!val) {
            var selectedCountry = $($parent[0]).val();
            parent.trigger('change', selectedCountry);
        }
    });

    $('div.subform-repeatable').on('subform-row-add', function (e, row) {

        var row_obj = $(row);

        $('[data-parent]', this).each(function () {
            if (!$(this).data('parent-listen')) {

                var groupnew = row_obj.data('group'),
                    base_name = row_obj.data('base-name'),
                    group = base_name + 'X',
                    name = $(this).data('parent'),
                    new_name = name.replace('[' + group + '][', '[' + groupnew + '][');
                $(this).data('parent-listen', '1');
                var parent = $('[name="' + new_name + '"]');
                if (parent.length) {
                    parentListener(parent, $(this).selectize({
                        valueField: 'value',
                        labelField: 'text',
                        searchField: ['text']
                    }));
                }
            }
        });
    });

});
