jQuery(document).ready(function ($) {

    var start = new moment(),
        end = new moment().add(1, 'hours');

    $.fn.iniDatePicker = function (dt) {
        if ($(this).data("DateTimePicker") != undefined) {
            $(this).datetimepicker('destroy');
        }
        $(this).datetimepicker({
            toolbarPlacement: 'top',
            date: dt,
            format: 'YYYY-MM-DD HH:mm',
            sideBySide: true,
            debug: true,
            showClose: true,
            minDate:start
        });
        return $(this);
    };

    var startEvent = $('#jform_start'),
        endEvent = $('#jform_end');

    startEvent.iniDatePicker(start);

    endEvent.iniDatePicker(end);

    startEvent.on("dp.change", function (e) {

        console.log(e);

        console.log('startEvent change ' + e.date.format('YYYY-MM-DD HH:mm'));

        /*console.log(endEvent.data("DateTimePicker").date().format('HH:mm'));
         console.log(e.oldDate.format('HH:mm'));
         console.log(endEvent.data("DateTimePicker").date().format('HH:mm'));*/
        if (e.date.diff(endEvent.data("DateTimePicker").date(), 'minute') > 0) {
            var diff = e.date.diff(endEvent.data("DateTimePicker").date(), 'minute');
            e.date.add(diff, 'minutes');
            endEvent.data("DateTimePicker").date(e.date);
        }
    });

    endEvent.on("dp.change", function (e) {

        console.log('endEvent change ' + e.date.format('YYYY-MM-DD HH:mm'));

        if (e.date.diff(startEvent.data("DateTimePicker").date(), 'minute') < 0) {
            var diff = e.date.diff(startEvent.data("DateTimePicker").date(), 'minute');
            e.date.add(diff, 'minutes');
            startEvent.data("DateTimePicker").date(e.date);
        }
    });

});

Joomla.submitbutton = function(task)
{
    Joomla.submitform(task, document.getElementById("eventForm"));
};