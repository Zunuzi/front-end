jQuery(document).ready(function ($) {

    /*calendar listeners*/
    var calendarInStep = $('#calendar');
    if (calendarInStep.length) {
        var calendar_fieled = calendarInStep.siblings('input[type="hidden"]');
        calendarInStep.on('afterRenderEvents', function (event, data) {
            calendar_fieled.val(data.events.length);
        });
        calendarInStep.on('removeEvent', function (event, event_id) {
            fieldVal = parseInt(calendar_fieled.val());
            if (fieldVal > 0) {
                fieldVal--
            }
            calendar_fieled.val(fieldVal);
        })
    }


    //jQuery time
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches

    var progressbar = $('#progressbar'),
            completion_steps = $('#completion-steps');
    window.fieldsets = completion_steps.find('fieldset');

    if (fieldsets.has(':visible').find('#calendar').length) {
        calendarInStep.fullCalendar('removeEvents');
        calendarInStep.fullCalendar('refetchEvents');
    }

    /*only dev*/
    progressbar.on('click', 'li', function () {

        if ($(this).hasClass('active')) {

            var step = $(this).attr('class').substr(4, 1),
                current_fs_step = $('#progressbar li.active').last().attr('class').substr(4, 1),
                current_fs = $('fieldset.step'+current_fs_step);

            if (step === current_fs_step) {
                return false;
            }

            var previous_fs = $('fieldset.step' + step, completion_steps);

            $('.progressbar_steps_title').removeClass('active');
            $(`.progressbar_steps_title.step${step}`).addClass('active');

            progressbar.find('li').each(function () {
                var li_step = $(this).attr('class').substr(4, 1);

                if (li_step <= step) {
                    $(this).addClass('active').prev().addClass('active-success');
                } else {
                    $(this).removeClass('active').prev().removeClass('active-success');
                }
            });

            //show the previous fieldset
            previous_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale previous_fs from 80% to 100%
                    scale = 0.8 + (1 - now) * 0.2;
                    //2. take current_fs to the right(50%) - from 0%
                    left = ((1 - now) * 50) + "%";
                    //3. increase opacity of previous_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'left': left});
                    previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
                },
                duration: 800,
                complete: function () {
                    current_fs.hide();
                    animating = false;
                    previous_fs.focus();

                    /*refetch calendar*/
                    if (previous_fs.find('calendar').length) {
                        calendarInStep.fullCalendar('removeEvents');
                        calendarInStep.fullCalendar('refetchEvents');
                    }

                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });


        }
    });

    function bodyAnimateTop() {
        $('body, html').animate({
            scrollTop: 0
        }, '0');
    }

    let noError = [];
    let hasError = [];
    let hasErrorMessages = [];
    let errorFieldTitle;

    fieldsets.on('click', '.next', function (e) {
        if ($('#jform_profile_default_avatar').val() != '') {
            let img = $('#jform_profile_default_avatar');
            img.closest('.custom-file-input').append(`
                    <input
                    type="hidden"
                    name="jform[profile][default][avatar]"
                    value="" />
                `);
            img.remove();
        }
        if ($('#jform_profile_tutor_video').val() != '') {
            let video = $('#jform_profile_tutor_video');
            video.closest('.custom-file-input').append(`
                    <input
                    type="hidden"
                    name="jform[profile][tutor][video]"
                    value="" />
                `);
            video.remove();
        }
        hasError = [];
        noError = [];
        hasErrorMessages = [];
        if (animating)
            return false;
        animating = true;
        current_fs = $(this).closest('fieldset');
        $('#step').val(current_fs.data('step'));
        next_fs = $(this).closest('fieldset').next();

        var data = new FormData($('<form></form>').append(current_fs.clone())[0]);

        var save_data = 1;
        if (current_fs.find('#calendar').length) {
            save_data = 0;
        }
        data.append('step', $('#step').val());
        completionValidate(data, save_data).then(function () {

            if (current_fs.find('[name="activate"]').length) {
                location.href = location.protocol + "//" + location.host + '/';
            }
            $('.error-field-text').remove();
            //activate next step on progressbar using the index of next_fs

            progressbar.children('li.' + next_fs.attr('class')).addClass("active");
            $('#progressbar li.' + next_fs.attr('class')).prevAll().addClass("active active-success");

            $('.progressbar_steps_title').removeClass('active');
            $(`.progressbar_steps_title.${next_fs.attr('class')}`).addClass('active');

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function (now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50) + "%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale(' + scale + ')'});
                    next_fs.css({'left': left, 'opacity': opacity});
                },
                duration: 800,
                complete: function () {
                    current_fs.hide();
                    animating = false;
                    next_fs.focus();
                    /*refetch calendar*/
                    if (next_fs.find('#calendar').length) {
                        calendarInStep.fullCalendar('removeEvents');
                        calendarInStep.fullCalendar('refetchEvents');
                    }
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });

        }, function () {
            animating = false;
        }).then(function (){
            if(animating) {
                bodyAnimateTop();
            }
        });

        if ($('.view-completion #progressbar li.step3').hasClass('active')) {
            setTimeout(function () {
                $('form#completion-steps .step3').find(':input')
                    .not('#jform_profile__tutor__tutoring_data__tutoring_since-selectized')
                    .parent('.selectize-input').each(function () {
                    if ($(this).hasClass('error')) {
                        if($(this).parents().eq(9).find('h2.rl_tabs-title').find('a.anchor').length > 0) {
                            errorFieldTitle = $.trim($(this).parents().eq(9).find('h2.rl_tabs-title').first().text());
                        } else {
                            errorFieldTitle = $.trim($(this).parents().eq(8).find('label.control-label').first().text());
                        }
                        if (hasErrorMessages.indexOf(errorFieldTitle) == -1) {
                            hasErrorMessages.push(errorFieldTitle);
                        }
                        hasError.push($(this));
                    } else {
                        noError.push($(this));
                    }
                });
                if (noError.length && hasError.length) {
                    let  message = Joomla.JText._('COM_CONTENT_REMOVE_EMPTY_FIELDS_MODAL_CONTENT');
                    message = message.replace('%s', hasErrorMessages.join(', '));
                    let removeItemsProfile = {
                        header: `${Joomla.JText._('COM_TEACHING_CONFIRMATION')}`,
                        body_append: `<div class="removeItemsProfile text-right">
                                <button id="mod_yes_btn" type="button" class="btn btn-primary">
                                    ${Joomla.JText._('COM_CONTENT_RATING_DELETE_MODAL_CONFIRM')}
                                </button>
                                <button id="mod_no_btn" type="button" class="btn" data-dismiss="modal" aria-label="Close">
                                    ${Joomla.JText._('COM_CONTENT_RATING_DELETE_MODAL_CANCEL')}
                                </button>
                            </div>`,
                        modalSelector: 'confirm',
                        body: `<span>`+ message +`</span>`,
                        footer_close_button: false
                    };
                    jQuery.teaching.runModal(removeItemsProfile);
                }
            }, 1000);
        }
    });

    $(document).on('click', '.removeItemsProfile .btn-primary', function (e) {
        e.preventDefault();
        $('#confirm').modal('hide');
        $(hasError).each(function(index, value){
            value.closest('.subform-repeatable-group').remove();
        });
        hasError = [];
        noError = [];
        $(document).find('#completion-steps .step3 .profile-activate-btn').removeClass('disabled').click();
    });

    fieldsets.on('click', '.previous', function () {
        if (animating)
            return false;
        animating = true;

        current_fs = $(this).closest('fieldset');
        previous_fs = $(this).closest('fieldset').prev();

        //de-activate current step on progressbar
        progressbar.children('li.' + current_fs.attr('class')).removeClass("active").prev().removeClass('active-success');

        //show the previous fieldset
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function (now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale previous_fs from 80% to 100%
                scale = 0.8 + (1 - now) * 0.2;
                //2. take current_fs to the right(50%) - from 0%
                left = ((1 - now) * 50) + "%";
                //3. increase opacity of previous_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({'left': left});
                previous_fs.css({'transform': 'scale(' + scale + ')', 'opacity': opacity});
            },
            duration: 800,
            complete: function () {
                current_fs.hide();
                animating = false;
                previous_fs.focus();
                /*refetch calendar*/
                if (previous_fs.find('calendar').length) {
                    calendarInStep.fullCalendar('removeEvents');
                    calendarInStep.fullCalendar('refetchEvents');
                }

            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });
        bodyAnimateTop();
    });

    fieldsets.on('click', '.op-cancel', function () {
        var storage_teaching = Joomla.getOptions('csrf.token');
        return $.ajax({
            url: '/index.php?option=com_teaching&task=completion.cancel',
            data: storage_teaching + "=1",
            cache: false,
            dataType: 'json',
            method: 'post',
            success: function (success) {
                if(success.success){
                    window.location.href = success.data;
                }
            }
        });
    });

    $(".submit").click(function () {
        return false;
    });

    function completionValidate(data, save) {
        let step = data.get('step');

        var processData = true,
                contentType = 'application/x-www-form-urlencoded; charset=UTF-8';

        if (typeof data === 'object')
        {
            processData = false;
            contentType = false;
        }
        data.append(storage_teaching.token, 1);

        var dfd = $.Deferred();
        $.ajax({
            url: '/index.php?option=com_teaching&task=completion.validate&save=' + save,
            data: data,
            method: 'POST',
            dataType: 'json',
            processData: processData,
            contentType: contentType,
            success: function (data) {
                var result = data.data;
                Joomla.renderMessages(data.messages);
                if (result.continue === false) {
                    if (result.messages['jform[profile][default][dob]']) {
                        $('.op-user-date > div').each(function () {
                            if ($(this).find('select').val() == '') {
                                result.messages[$(this).find('select').attr("name")] = 'Field required: ' + $(this).find('select').attr("placeholder");
                            }
                        });
                    }
                    jQuery.each(result.messages, function (key, message) {

                        let field = $('[name="' + key + '"]');

                        if (field.length) {
                            field.addClass('do-validate');
                            field.addClass('error').data('validation-error', message);
                            field.parent().find('.chzn-container').addClass('error');
                            // field.closest('.form-group:not(".op-user-date")').addClass('has-error');
                            if (key.indexOf('rate_types') > 0) {
                                $.teaching.addErrorToInput(field.closest('.checkbox'), message)
                            } else {
                                if ( field.is( 'input[type="file"]' ) ) {
                                    field = field.parent();
                                }
                                $.teaching.addErrorToInput(field, message);
                            }

                            if (field[0].id === 'jform_profile_default_avatar'){
                                if ($('.upload-file .validation-error-message').length === 0){
                                    $($.teaching.errorSpan(message)).insertBefore(
                                        $(field).closest('.custom-file-input').next()
                                    );
                                }
                            }

                        } else {
                            if (step != 2) {
                                Notifier.warning(message, 'Warning');
                            }
                        }
                    });

                    $.teaching.validateTabs($('form#completion-steps'));
                    let _has_error_ = $('.error[name]');
                    if (_has_error_.length > 0) {
                        _has_error_ = _has_error_[0];
                        if (_has_error_.classList.contains('selectized')) {
                            _has_error_ = _has_error_.previousElementSibling;
                        }
                        $([document.documentElement, document.body]).animate({
                            scrollTop: $(_has_error_).offset().top - window.innerHeight/2
                        }, 300);
                    }
                    dfd.reject();
                } else {
                    dfd.resolve();
                }
            },
            error: function () {
                dfd.reject();
            }
        });
        return dfd;
    }

    $("#completion-steps .price-matrix .price-chain").on('change', function (){
        $(this).css('color', '#555555');
    });

});

