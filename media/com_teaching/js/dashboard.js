var storage_teaching = Joomla.getOptions('teaching');

jQuery(function ($) {
    if (typeof $.fn.slick === 'function') {
        $('.js-tutor-slick-slider').slick({
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: false,
            prevArrow: '<button class="slick-prev slick-arrow" type="button">' +
                '<svg width="7" height="17" viewBox="0 0 7 17" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
                '    <path d="M1 1L6.5 9.5L1 16.5" stroke="#C4C4C4" stroke-width="0.5"/>\n' +
                '</svg></button>',
            nextArrow: '<button class="slick-next slick-arrow" type="button">' +
                '<svg width="7" height="17" viewBox="0 0 7 17" fill="none" xmlns="http://www.w3.org/2000/svg">\n' +
                '    <path d="M1 1L6.5 9.5L1 16.5" stroke="#C4C4C4" stroke-width="0.5"/>\n' +
                '</svg></button>',
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }

    function updateTotalTime() {
        const element = $('.reverse-report');
        const lessonStartDate = element.data('start');
        const lessonStartNow = element.data('now');
        if (element.length != 0) {
            const minutes = Math.floor(Math.abs(new Date(lessonStartNow.replace(/-/g, '/')) - new Date(lessonStartDate.replace(/-/g, '/'))) / 60000);
            element.html(minutes);
        }
    }

    updateTotalTime();
    setInterval(function () {
        updateTotalTime();
    }, 3600);

    $('#moreUpcomingEvents').on('click', function () {
        $(this).prop('disabled', true);
        let start = jQuery(this).data('start');
        const url = `${Joomla.getOptions('teaching').base_url}index.php?option=com_teaching&task=dashboard.getUpcomingEvents`;

        var data = {
            start: start,
            [storage_teaching.token]: 1
        }
        jQuery.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: data,
            error: function (e) {
                console.log(e);
            },
            success: function (res) {
                if(res.data.start == null) {
                    $('#moreUpcomingEvents').remove();
                } else {
                    $('#moreUpcomingEvents').data('start', res.data.start);
                    $('#moreUpcomingEvents').prop('disabled', false);
                }
                if(start == 0) {
                    $('.upcomming-lesson').remove();
                }
                $.each(res.data.lessons, function( index, value ) {
                    let avatars = value["avatar"].split('-,_');
                    let avatar = '';
                    $.each(avatars, function( index, value ) {
                        avatar += '<img class="upcomming-lesson-img" src="/images/users/avatars/'+ value +'" alt="">';
                    });
                    $( '<div class="upcomming-lesson">'+
                        '<span class="upcomming-lesson-time">'+
                        value["start"] +'</span>' +
                        '<div class="flex-align-center">' +
                        '<span class="upcomming-lesson-name">'+ value["subject_text"] +' with</span>'+
                        ''+ avatar +''+
                        '</div>'+
                        '</div>').insertBefore( $( ".my-upcomming-lesson .text-center" ) );
                });

            }
        });
    });

})