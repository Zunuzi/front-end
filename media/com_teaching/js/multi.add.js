(function ($) {

    $.fn.addEvents = function (_start, _end, updateEvent = false) {
        var _this = $(this);

        _this.modal('show');

        //Resize modal window height
        var window_height = $(window).height();
        var modal_height = null;
        if (!updateEvent) {
            $(window).resize(function () {
                window_height = $(window).height();
                modal_height = window_height - 260;
                $('.modal-body', _this).css('height', modal_height + 'px');
            });
            modal_height = window_height - 260;
        }
        $('.modal-body', _this).css('height', modal_height + 'px');

        /*
                var block_height = $('#block1', this).height();
                $('#block3', _this).css('height', block_height + 'px');
        */

        var preview_calendar = $('#preview-calendar', _this);
        preview_calendar.show();

        var updateEndDatetime = function (event_date, end_time, start_time) {
            var init_start = moment(event_date.val() + ' ' + start_time, 'D MMMM YYYY HH:mm'),
                init_end, self,
                notValidDatetime,
                selected = false;
            init_end = moment(event_date.val() + ' ' + end_time.val(), 'D MMMM YYYY HH:mm');
            notValidDatetime = ( parseInt(init_end.format('X')) < parseInt(init_start.format('X')) ||
                Math.abs(parseInt(init_end.format('X')) - parseInt(init_start.format('X'))) < 3600 ||
                (end_time.hasClass('event-update') && parseInt(init_end.format('X')) - parseInt(init_start.format('X')) > 3600));

            $('#time2 + .selectize-control .option').each(function () {
                self = $(this);
                init_end = moment(event_date.val() + ' ' + self.data('value'), 'D MMMM YYYY HH:mm');
                if (
                    (parseInt(init_end.format('X')) < parseInt(init_start.format('X')) ||
                    Math.abs(parseInt(init_end.format('X')) - parseInt(init_start.format('X'))) < 3600)  && self.data('value') != '00:00'
                ) {
                    self.removeAttr('data-selectable');
                    self.removeClass('selected')
                } else {
                    if ( !selected && notValidDatetime) {
                        $("#time2")[0].selectize
                            .setValue( init_end.format( 'HH:mm' ) );
                        self.addClass( 'selected' );
                        selected = true;
                    }
                    self.attr( 'data-selectable' , '');
                }
            });
        }

        $('#time1', _this).on('change', function () {
            var self = $(this);
            var message = '';
            var event_date = $('#date1', _this);
            var end_time = $('#time2', _this);
            updateEndDatetime(event_date, end_time, self.val());
            var init_start = moment(event_date.val() + ' ' + self.val(), 'D MMMM YYYY HH:mm');
            var init_end = moment(event_date.val() + ' ' + end_time.val(), 'D MMMM YYYY HH:mm');
            if (parseInt(init_end.format('X')) > parseInt(init_start.format('X'))) {
                if (Math.abs(parseInt(init_end.format('X')) - parseInt(init_start.format('X'))) < 3600) {
                    init_start = init_end.clone().subtract(1, 'hour');
                    self.next().find('.chzn-single span').html(init_start.format('HH:mm'));
                    self.find('option').each(function () {
                        $(this).removeAttr('selected');
                    });
                    self.find('option[value="' + init_start.format('HH:mm') + '"]').attr('selected', 'selected');
                    message = 'Less than one hour';
                    showalert(message);
                }
            } else if(end_time.val() != '00:00'){
                init_end = init_start.clone().add(1, 'hour');
                $('#time2').next().find('.chzn-single span').html(init_end.format('HH:mm'));
                self.find('option').each(function () {
                    $(this).removeAttr('selected');
                });
                self.find('option[value="' + init_start.format('HH:mm') + '"]').attr('selected', 'selected');
                message = 'End time must be higher than start';
                showalert(message);
            }
        });
        // Check end time
        $('#time2', _this).on('change', function () {
            var self = $(this);
            var message = '';
            var event_date = $('#date1', _this);
            var start_time = $('#time1', _this);
            var init_start = moment(event_date.val() + ' ' + start_time.val(), 'D MMMM YYYY HH:mm');
            var init_end = moment(event_date.val() + ' ' + self.val(), 'D MMMM YYYY HH:mm');
            if (parseInt(init_end.format('X')) > parseInt(init_start.format('X'))) {
                if (Math.abs(parseInt(init_end.format('X')) - parseInt(init_start.format('X'))) < 3600) {
                    init_end = init_start.clone().add(1, 'hour');
                    self.next().find('.chzn-single span').html(init_end.format('HH:mm'));
                    self.find('option').each(function () {
                        $(this).removeAttr('selected');
                    });
                    self.find('option[value="' + init_end.format('HH:mm') + '"]').attr('selected', 'selected');
                    message = 'Less than one hour';
                    showalert(message);
                }
            } else if(self.val() != '00:00'){
                init_end = init_start.clone().add(1, 'hour');
                self.next().find('.chzn-single span').html(init_end.format('HH:mm'));
                self.find('option').each(function () {
                    $(this).removeAttr('selected');
                });
                self.find('option[value="' + init_end.format('HH:mm') + '"]').attr('selected', 'selected');
                message = 'End time must be higher than start';
                showalert(message);
            }
        });

        $(document).on('change', '#modalMultiAdd #date1', function (){
            let date = moment( $(this).val() ).format("MMM D, YYYY")
            $('.modalTitleDate').html(`on ${date}`);
        });

        // Check value of occurencies-input
        $('#form-repeat-event-radio-after-occurencies-input', _this).on('change', function () {
            var self = $(this);
            var message = '';
            var value = self.val();
            if (200 <= value) {
                self.val(199);
                message = 'maximum 199';
                showalert(message);
            }
            updateEvents();
        });

        // Open events panel
        $('#form-repeat-event-change', _this).on('click', function (e) {
            e.preventDefault();
            var self = $(this);
            var value = self.attr('data-value');
            var form_block = $('#form-repeat-event-block', _this);
            if (0 === parseInt(value)) {
                self.html('Disable');
                self.attr('data-value', 1);
                form_block.show();
            } else {
                self.html('Enable');
                self.attr('data-value', 0);
                form_block.hide();
            }
            updateEvents();
        });

        // Select daily/weekly/montly
        $('input[name="form_repeat_event_radio[]"]', _this).on('change', function () {
            var self = $(this);
            var value = self.val();
            var blocks = $('#blocks');
            var daily_left = $('#form-repeat-event-daily-every', _this);
            var weekly_left = $('#form-repeat-event-weekly-every', _this);
            var montly_left = $('#form-repeat-event-monthly-every', _this);
            var repeat_limit = $('#form-repeat-event-after', _this);
            if ('daily' === value) {
                daily_left.show();
                weekly_left.hide();
                montly_left.hide();
                repeat_limit.show();
            } else if ('weekly' === value) {
                daily_left.hide();
                weekly_left.show();
                montly_left.hide();
                repeat_limit.show();
            } else if ('montly' === value) {
                daily_left.hide();
                weekly_left.hide();
                montly_left.show();
                repeat_limit.show();
            } else {
                if (self[0].checked) {
                    blocks.hide();
                    daily_left.hide();
                    weekly_left.hide();
                    montly_left.hide();
                    repeat_limit.hide();
                    $('#block1 [type=radio]').each(function (){
                        this.checked = false;
                        $(this).parent().removeClass('active');
                    });
                } else {
                    blocks.show();
                    $('#form-repeat-event-radio-daily')[0].checked = true;
                    $('#form-repeat-event-radio-daily').parent().addClass('active');
                    daily_left.show();
                    weekly_left.hide();
                    montly_left.hide();
                    repeat_limit.show();
                }
            }
            updateEvents();
        });

        // Select week days
        $('#form-repeat-event-weekly-every-week-weekdays li', _this).on('click', function () {
            var self = $(this);
            var active = self.attr('data-day-active');
            if (1 === parseInt(active)) {
                self.removeClass('selected active');
                self.attr('data-day-active', 0);
            } else {
                self.addClass('selected active');
                self.attr('data-day-active', 1);
            }
            updateEvents();
        });

        // Select every day or every workday
        $('input[name="form_repeat_event_radio_every[]"]', _this).on('change', function () {
            var self = $(this);
            var value = self.val();
            var input = $('#form-repeat-event-radio-daily-every-day-input', _this);
            if ('every_day' === value) {
                input.attr('disabled', false);
            } else if ('every_workday' === value) {
                input.attr('disabled', true);
            }
            updateEvents();
        });

        $('#form-repeat-event-radio-daily-every-day-input').on('change', function (){
            if($(this).val().trim() < 1) {
                $(this).val(1);
            }
        });

        // Select monthly days
        $('#form-repeat-event-montly-every-day li', _this).on('click', function () {
            var self = $(this);
            var active = self.attr('data-day-active');
            if (1 === parseInt(active)) {
                self.removeClass('selected active');
                self.attr('data-day-active', 0);
            } else {
                self.addClass('selected active');
                self.attr('data-day-active', 1);
            }
            updateEvents();
        });

        // Select after occurencies or after end day
        $('input[name="form_repeat_event_radio_after[]"]', _this).on('change', function () {
            var self = $(this);
            var value = self.val();

            var input = $('#form-repeat-event-radio-after-occurencies-input', _this);
            var select = $('#form-repeat-event-radio-after-endday-select', _this);
            if ('after_occurencies' === value) {
                input.attr('disabled', false);
                select.attr('disabled', true);
            } else if ('after_endday' === value) {
                input.attr('disabled', true);
                select.attr('disabled', false);
            }
            updateEvents();
        });

        $('#form-repeat-event-radio-daily-every-day-input', _this).on('change', function () {
            if($(this).val().trim().length > 0){
                updateEvents();
            }
        });

        $('#preview-calendar', _this).datepicker('destroy').datepicker({
            format: "dd/mm/yyyy",
            multidate: true,
            todayHighlight: true,
            container: '#modalMultiAdd .modal-body'
        }).datepicker("setDate", new Date(_start.format('YYYY-MM-DD HH:mm:ss')));

        $('.datepicker-inline', _this).css('background', '#f4f4f4');

        function updateMultiDatepicker(container){
            $('.input-group.date', _this).datepicker('destroy').datepicker({
                format: "dd MM yyyy",
                startDate: new Date(),
                todayHighlight: true,
                container: container,
                autoclose: true,
            }).on("changeDate", function (e) {
                $('[data-toggle="popover"]').popover('hide');
                updateEvents();
            }).on('hide', function(e) {
                e.stopPropagation();
            }).datepicker("setDate", new Date(_start.format('YYYY-MM-DD')));
        }

        if (updateEvent) {
            // Datepiker for current date update event modal
            updateMultiDatepicker('.update-event');
        } else {
            // Datepiker for current date
            updateMultiDatepicker('#modalMultiAdd .modal-body');
        }

        // Datepiker for end date
        $('#form-repeat-event-radio-after-endday-select', _this).datepicker('destroy').datepicker({
            format: "dd MM yyyy",
            todayHighlight: true,
            container: '#modalMultiAdd .modal-body'
        }).datepicker("setDate", new Date(_end.add(1, 'M').format('YYYY-MM-DD'))).on("changeDate", function () {
            updateEvents();
        });

        // Click save button and show final result
        $('#form-calculate-dates-array', _this).on('click', function (e) {
            if ($(e.target).attr('disabled')) return;
            var dates = $('#preview-calendar', _this).datepicker('getDates');

            var data_dates = [];

            dates.forEach(function (item, i, arr) {
                var month = ("0" + (item.getMonth() + 1)).substr(-2, 2),
                    day = ("0" + item.getDate()).substr(-2, 2);
                data_dates[i] = item.getFullYear() + '-' + month + '-' + day;
            });

            var event_type = $('input[name="form_repeat_event_radio[]"]:checked', _this).val() || null;
            if (null !== event_type) {
                getFormValues(event_type, function (data) {
                    if (null !== data) {
                        calculateEvents(data, function (result) {
                            var answer = {
                                start: $('#time1', _this).val(),
                                end: $('#time2', _this).val(),
                                text: $('#jform_text', _this).val(),
                                tid: $('#jform_tid', _this).val(),
                                dates: data_dates
                            };

                            // _this.modal('hide');
                            eventSave(answer, _this);
                            if(typeof(fieldsets) != "undefined" && fieldsets !== null) {
                                setTimeout(function(){
                                    fieldsets.find('.next-activate').click();
                                }, 1000);
                            }
                        });
                    }
                });
            }
        });

        function showalert(message) {
            $('#alert_placeholder').append('<div id="alertdiv" class="alert alert-warning"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>');
            setTimeout(function () {
                $("#alertdiv").remove();
            }, 5000);
        }

        var opts = {
            title: '',
            placement: 'left',
            trigger: 'hover',
            html: 'true',
            content: ''
        };

        $('#form-calculate-dates-array', _this).popover(opts);

        function updateEvents() {

            dates = [];
            var event_type = $('input[name="form_repeat_event_radio[]"]:checked', _this).val() || null;

            $('#form-calculate-dates-array', _this).attr('data-content', '');

            if (null !== event_type && 'none' !== event_type) {
                getFormValues(event_type, function (data) {
                    if (null !== data) {
                        calculateEvents(data, function (result) {

                            if (result.length === 0) {
                                switch (event_type) {
                                    case 'weekly':
                                        $('#form-calculate-dates-array', _this).attr('data-content', Joomla.JText.strings.COM_TEACHING_SELECT_DAY_WEEK);
                                        break;
                                    case 'montly':
                                        $('#form-calculate-dates-array', _this).attr('data-content', Joomla.JText.strings.COM_TEACHING_SELECT_DATE_MONTH);
                                        break;
                                    default:
                                        break;
                                }
                            }

                            result.map(function (date) {
                                var day = moment(date).format('D/M/YYYY');
                                dates.push(day);
                            });
                            $('#preview-calendar', _this).datepicker('setDates', dates);
                        });
                    }
                });
            } else {

                var date1 = $('#date1', _this).parent('.input-group.date').datepicker("getDate");
                $('#preview-calendar', _this).datepicker('setDate', moment(date1).format('D/M/YYYY'));
            }
            if ($('#preview-calendar', _this).datepicker('getDates').length) {
                $('#form-calculate-dates-array', _this).removeAttr("disabled");
            } else {
                $('#form-calculate-dates-array', _this).attr("disabled", "disabled");
            }
        }

        function getFormValues(event_type, callback) {
            const afterDefaultDay = new Date(new Date().getTime() + (86400000 * 199));
            var result = {};
            result = addTimePeriod();
            result.after = $('input[name="form_repeat_event_radio_after[]"]:checked', _this).val();
            if ('after_occurencies' === result.after) {
                result.after_occurencies = $('#form-repeat-event-radio-after-occurencies-input', _this).val();
                result.after_endby = moment(afterDefaultDay).format("DD MMMM YYYY");
            } else if ('after_endday' === result.after) {
                result.after_occurencies = null;
                result.after_endby = $('#form-repeat-event-radio-after-endday-select', _this).val();
            }
            if ('daily' === event_type) {
                result.type = 'daily';
                result.every = $('input[name="form_repeat_event_radio_every[]"]:checked', _this).val();
                if ('every_day' === result.every) {
                    result.every_count = $('#form-repeat-event-radio-daily-every-day-input', _this).val();
                } else {
                    result.every_count = null;
                }
            } else if ('weekly' === event_type) {
                result.type = 'weekly';
                result.every_count = $('#form-repeat-event-weekly-every-week-input', _this).val();
                var mas = [];
                $('#form-repeat-event-weekly-every-week-weekdays', _this).find('li').each(function () {
                    var self = $(this);
                    var active = self.attr('data-day-active');
                    var nr = self.attr('data-day-nr');
                    if (1 === parseInt(active)) {
                        mas.push(parseInt(nr));
                    }
                });
                result.every_weekdays_list = mas;
            } else if ('montly' === event_type) {
                result.type = 'montly';
                var mas = [];
                $('#form-repeat-event-montly-every-day', _this).find('li').each(function () {
                    var self = $(this);
                    var active = self.attr('data-day-active');
                    var nr = self.attr('data-day-nr');
                    if (1 === parseInt(active)) {
                        mas.push(parseInt(nr));
                    }
                });
                result.every_day_list = mas;
            }
            result.every_count = result.every_count || 1;
            callback(result);
        }

        function addTimePeriod() {
            return {
                start_time: $('#time1', _this).val(),
                end_time: $('#time2', _this).val(),
                date: $('#date1', _this).val()
            };
        }

        function calculateEvents(params, callback) {

            var result = [];
            var init_start = moment(params.date + ' ' + params.start_time, 'D MMMM YYYY HH:mm');
            var init_end = moment(params.date + ' ' + params.end_time, 'D MMMM YYYY HH:mm');
            var now = moment();
            var item = {};
            var count_start = init_start.clone();
            var count_end = init_end.clone();
            var i = 0;
            var j = null;
            var k = null;
            var m = null;
            var work_day_count = null;
            var endday = null;
            var every_day_count = null;
            var every_month_count = null;

            var end = moment(params.after_endby);
            var duration = moment.duration(now.diff(end));
            var days = duration.asDays();
            if (days && days < 1) {
                if ('daily' === params.type) {
                    if ('every_day' === params.every) {
                        if (parseInt(now.format('X')) < parseInt(init_start.format('X'))) {
                            result.push(count_start.format());
                        }
                        every_day_count = parseInt(params.every_count);

                        if ('after_occurencies' === params.after) {
                            work_day_count = parseInt(params.after_occurencies);

                            // j = 0;
                            for (i = 0; i < work_day_count - 1; i++) {
                                count_start = count_start.clone().add(every_day_count, 'day');
                                count_end = count_end.clone().add(every_day_count, 'day');
                                if (parseInt(now.format('X')) < parseInt(count_start.format('X'))) {
                                    result.push(count_start.format());
                                    //j++;
                                }

                                // if (j === work_day_count - 1) {
                                //     break;
                                // }
                            }
                        } else if ('after_endday' === params.after) {
                            endday = moment(params.after_endby + ' ' + params.start_time, 'D MMMM YYYY HH:mm');
                            j = true;
                            while (true === j) {
                                count_start = count_start.clone().add(every_day_count, 'day');
                                count_end = count_end.clone().add(every_day_count, 'day');
                                if (parseInt(endday.format('X')) < parseInt(count_start.format('X'))) {
                                    j = false;
                                } else {
                                    if (parseInt(endday.format('X')) >= parseInt(count_start.format('X')) &&
                                        parseInt(endday.format('X')) >= parseInt(count_end.format('X')) &&
                                        parseInt(now.format('X')) < parseInt(count_start.format('X'))) {
                                        result.push(count_start.format());
                                    }

                                }
                            }
                        }
                    } else if ('every_workday' === params.every) {
                        if (0 !== init_start.day() && 6 !== init_start.day()) {
                            result.push(count_start.format());
                        }
                        if ('after_occurencies' === params.after) {
                            work_day_count = parseInt(params.after_occurencies);
                            for (i = 0; i < 1000; i++) {
                                if (result.length === work_day_count) {
                                    break;
                                }
                                count_start = count_start.clone().add(1, 'day');
                                count_end = count_end.clone().add(1, 'day');
                                if (0 !== count_start.day() && 6 !== count_start.day() && parseInt(now.format('X')) < parseInt(count_start.format('X'))) {
                                    result.push(count_start.format());
                                }
                            }
                        } else if ('after_endday' === params.after) {
                            endday = moment(params.after_endby + ' ' + params.start_time, 'D MMMM YYYY HH:mm');
                            j = true;
                            while (true === j) {
                                count_start = count_start.clone().add(1, 'day');
                                count_end = count_end.clone().add(1, 'day');
                                if (0 !== count_start.day() && 6 !== count_start.day() && parseInt(now.format('X')) < parseInt(count_start.format('X'))) {
                                    result.push(count_start.format());
                                }
                                if (endday.format() === count_start.format()) {
                                    j = false;
                                }
                            }
                        }
                    }
                } else if ('weekly' === params.type) {
                    var every_week_count = parseInt(params.every_count);
                    if (-1 < params.every_weekdays_list.indexOf(init_start.day()) &&
                        1 === every_week_count &&
                        parseInt(now.format('X')) < parseInt(count_start.format('X'))) {

                        result.push(count_start.format());
                    }
                    if ('after_occurencies' === params.after) {
                        work_day_count = parseInt(params.after_occurencies);
                        j = 0;
                        k = 1;
                        for (i = 0; i < 1000; i++) {
                            if (result.length === work_day_count) {
                                break;
                            }
                            count_start = count_start.clone().add(1, 'day');
                            count_end = count_end.clone().add(1, 'day');
                            if (-1 < params.every_weekdays_list.indexOf(count_start.day()) &&
                                (0 === k % every_week_count) &&
                                parseInt(now.format('X')) < parseInt(count_start.format('X'))) {

                                result.push(count_start.format());
                                j++;
                            }
                            if (0 === ((i + 1) % 7)) {
                                k++;
                            }
                            if (j === work_day_count) {
                                break;
                            }
                        }
                    } else if ('after_endday' === params.after) {
                        endday = moment(params.after_endby + ' ' + params.start_time, 'D MMMM YYYY HH:mm');
                        j = true;
                        k = 1;
                        i = 0;
                        while (true === j) {
                            count_start = count_start.clone().add(1, 'day');
                            count_end = count_end.clone().add(1, 'day');
                            if (-1 < params.every_weekdays_list.indexOf(count_start.day()) &&
                                (0 === k % every_week_count) &&
                                parseInt(now.format('X')) < parseInt(count_start.format('X'))) {

                                result.push(count_start.format());
                            }
                            if (0 === ((i + 1) % 7)) {
                                k++;
                            }
                            if (endday.format() <= count_start.format()) {
                                j = false;
                            }
                            i++;

                        }
                    }
                } else if ('montly' === params.type) {
                    if ('after_occurencies' === params.after) {
                        work_day_count = parseInt(params.after_occurencies);
                        j = 0;
                        for (i = 0; i < 1000; i++) {
                            count_start = count_start.clone().add(1, 'day');
                            count_end = count_end.clone().add(1, 'day');

                            if (-1 < params.every_day_list.indexOf(count_start.date()) &&
                                parseInt(now.format('X')) < parseInt(count_start.format('X'))) {
                                result.push(count_start.format());
                                j++;
                            }
                            if (j === work_day_count) {
                                break;
                            }
                        }
                    } else if ('after_endday' === params.after) {
                        endday = moment(params.after_endby + ' ' + params.start_time, 'D MMMM YYYY HH:mm');
                        j = true;
                        while (true === j) {
                            count_start = count_start.clone().add(1, 'day');
                            count_end = count_end.clone().add(1, 'day');
                            if (parseInt(endday.format('X')) <= parseInt(count_start.format('X'))) {
                                j = false;
                            } else {
                                if (parseInt(endday.format('X')) > parseInt(count_start.format('X')) &&
                                    parseInt(endday.format('X')) > parseInt(count_end.format('X')) &&
                                    -1 < params.every_day_list.indexOf(count_start.date()) &&
                                    parseInt(now.format('X')) < parseInt(count_start.format('X'))) {
                                    result.push(count_start.format());
                                }
                            }
                        }
                    }

                }

            }

            callback(result);
        }

        $('select', _this).selectize({
            create: true,
            sortField: {
                direction: "asc"
            }
        });

        var endDatetime=$("#time2")[0].selectize;
        endDatetime.setValue( $("#time2").val() );

        return _this;

    };

})(jQuery);
