function readURL(input) {
    return new Promise((resolve, reject) => {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            var max_file_size = jQuery('#max_file_size').val();
            var exactSize = input.files[0].size / 1024;
            var exactSize = exactSize / 1024;
            var avatar = jQuery("#jform_profile_default_avatar");
            if (exactSize > max_file_size) {
                avatar.replaceWith(avatar.val('').clone(true));
                let message = Joomla.JText._('COM_TEACHING_FILE_SIZE');
                message = message.replace('%s', max_file_size);
                Notifier.error(message);
            } else {
                reader.onload = function (e) {
                    var formData = new FormData();
                    var files = input.files[0];
                    formData.append('file', files);
                    jQuery.ajax({
                        url: '/index.php?option=com_teaching&task=child.avatarAjaxValidate&ajax=1',
                        type: 'post',
                        data: formData,
                        contentType: false,
                        processData: false,
                        accept: 'application/json',
                        dataType: 'json',
                        success: function (result) {
                            if (result.success) {
                                // jQuery('#preview').attr('src', e.target.result);
                                document.getElementById('jform_profile_default_avatar').classList.remove('error');
                                jQuery(document.getElementById('jform_profile_default_avatar').parentElement.parentElement.querySelector('span.error-field-text')).remove();
                                return resolve(true);
                            } else {
                                input.value = '';
                                // jQuery('#preview').attr('src', '/images/noavatar.png');
                                if (typeof result.message == "object") {
                                    result.message.forEach(function (message) {
                                        Notifier.warning(message['message']);
                                    });
                                } else {
                                    Notifier.warning(result.message);
                                }
                                return resolve(false);
                            }
                        }
                    });
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    })
}

function readURLCropped(base64, fileSize) {
    let storage_teaching = Joomla.getOptions('teaching');

    let processData = false,
        contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
    let id_field = document.getElementById('jform_id');

    jQuery.ajax({
        xhr: function () {
            let xhr = new window.XMLHttpRequest();
            let randomPercent = randomNumber(91,99);
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    let percentComplete = evt.loaded / evt.total;
                    let toProgress = parseInt(percentComplete) * 100;
                    toProgress = (toProgress > 90) ? randomPercent : toProgress;

                    jQuery('.completion-avatar .upload-progress-bg').css({
                        width: toProgress + '%'
                    });
                    jQuery('.completion-avatar .upload-progress-percent')
                        .text(toProgress + '%');
                }
            }, false);
            return xhr;
        },

        url: '/index.php?option=com_teaching&task=completion.update_avatar&ajax=1' + "&id=" + id_field.value,
        data: 'avatar=' + base64 + "&size=" + fileSize + "&" + storage_teaching.token + "=1",
        method: 'POST',
        dataType: 'json',
        accept: 'application/json',
        processData: processData,
        contentType: contentType,
        beforeSend: function () {
            jQuery('#cropper-modal').modal('hide');
            jQuery('.completion-avatar #preview, .cropperBox').hide();
            jQuery(`<div class="upload-content">
                                    <div class="upload-progress">
                                        <span class="upload-progress-bg"></span>
                                        <span class="upload-progress-percent">0%</span>
                                    </div>
                                    <span class="upload-txt">${Joomla.JText._('COM_TEACHING_IMAGE_WAITING_UPLOAD')}</span>
                                </div>`)
                .insertBefore('.completion-avatar .repeatable-control.current-file .small.text-primary');
            jQuery('body').append(`<div class='upload-overlay'></div>`);
        },
        success: function (result) {
            if (result.success) {
                jQuery('.upload-overlay').remove();
                jQuery('.completion-avatar .upload-content').remove();
                jQuery('#preview').show().attr('src', base64);
                document.getElementById('jform_profile_default_avatar').classList.remove('error');
                jQuery(document.getElementById('jform_profile_default_avatar').parentElement.parentElement.querySelector('span.error-field-text')).remove();
                jQuery('img.avatar').attr('src', base64);
                jQuery('.cropperBox').remove();
                if (!(jQuery('#jform_profile_default_avatar_file_name').length > 0)) {
                    jQuery('.upload-file.clearfix').append('<input type="hidden" value="' + result.data.avatar + '" name="jform[profile][default][avatar][file-name]" id="jform_profile_default_avatar_file_name">');
                }

                jQuery('form#member-profile').each(function () {
                    if (!jQuery(this).find(':input').hasClass('error')){
                        jQuery('button.validation-init').removeClass('disabled');
                    }
                });

                Notifier.success(Joomla.JText._('COM_TEACHING_IMAGE_UPLOADED_SUCCESSFULLY'));
            } else {
                jQuery('button.validation-init').addClass('disabled');
                // input.value = '';
                if (typeof result.message == "object") {
                    result.message.forEach(function (message) {
                        Notifier.warning(message['message']);
                    });
                } else {
                    Notifier.warning(result.message);
                }
            }
        }
    });
}

function randomNumber(min, max){
    const r = Math.random()*(max-min) + min
    return Math.floor(r)
}

jQuery(document).ready(function ($) {
    function cropperModal(self, cropBtn){
        let file_reader = new FileReader();

        // If isset cropperBox remove that
        let cropperBox = $('.cropperBox');
        if (cropperBox.length){
            $(cropperBox.remove())
        }
        // Hide Images
        // $('.teaching-file .imagefile').hide();

        // Append Cropper Box before text(Formats)
        // $(`<div class="cropperBox">
        //     <img src="#" alt="Cropped Image" />
        // </div>`)
        //     .insertAfter('.teaching-file .current-file > .note.small.text-primary');

        let modalHtml = `<div class="cropperBox">
                        <img src="#" alt="Cropped Image" />
                     </div>`;
        let cropperOptions =
            {
                modalSelector: 'cropper-modal',
                body: modalHtml,
                header_html: false,
                footer_close_button: false
            };
        $.teaching.runModal(cropperOptions);

        file_reader.onload = function (e) {
            $('.cropperBox img').attr('src', e.target.result);
        }
        file_reader.readAsDataURL(self.files[0]);

        setTimeout(function () {
            uploadCrop = $('.cropperBox img').croppie({
                enableExif: true,
                viewport: {
                    width: 300,
                    height: 300,
                    type: 'circle'
                },
                boundary: {
                    width: 300,
                    height: 300
                }
            });
            $('.cropperBox').append(`<button type="button" class="btn bg-grey cropImg ${cropBtn}">Crop Image</button>`);
        }, 150);
    }

    $(document).on('change', '#jform_profile_default_avatar', function (){
        const fileName = $(this).val().split('\\').pop();
        $(this).closest('.upload-file').find('.uploaded-file-name').html(`File to upload: ${fileName}`).css('display', 'block');

        let id_field = document.getElementById('jform_id');

        if (id_field) {
            if (parseInt(id_field.value) === 0) {
                let cropBtn = 'cropImgChild';
                readURL(this).then((imgValidate) => {
                    if (imgValidate){
                        cropperModal(this, cropBtn);
                    }
                });
            }else{
                if (this.files && this.files[0]) {
                    let max_file_size = jQuery('#max_file_size').val();
                    if (!max_file_size){
                        max_file_size = parseInt( $(this).data('size') );
                    }
                    window.fileSize = this.files[0].size;
                    var exactSize = fileSize / 1024;
                    var exactSize = exactSize / 1024;
                    var avatar = jQuery("#jform_profile_default_avatar");
                    if (exactSize > max_file_size) {
                        avatar.replaceWith(avatar.val('').clone(true));
                        jQuery('button.validation-init').addClass('disabled');
                        let message = Joomla.JText._('COM_TEACHING_FILE_SIZE');
                        message = message.replace('%s', max_file_size);
                        Notifier.error(message);
                    } else {
                        cropperModal(this, '');
                    }
                }
            }
        }

    });

    let fileBase64;
    $(document).on('click', '.cropImg', function (){
        let child = false;
        if ($(this).hasClass("cropImgChild")) {
            child = true;
        }
        uploadCrop.croppie('result', {
            type: 'base64',
        }).then(function (base64){
            fileBase64 = base64
            if (child){
                jQuery('#cropper-modal').modal('hide');
                jQuery('#preview').show().attr('src', base64);
                jQuery('img.avatar').attr('src', base64);
                jQuery('.cropperBox').remove();
            } else {
                readURLCropped(base64, fileSize)
            }
        });
    });

    $(document).on('submit', '#member-profile', function (event){
        event.preventDefault();
        let img = $('#jform_profile_default_avatar');
        img.closest('.custom-file-input').append(`
                <input 
                type="hidden" 
                name="jform[profile][default][avatar]"
                value="${fileBase64}" />
            `);
        img.remove();
        document.getElementById('member-profile').submit();
    });

});
