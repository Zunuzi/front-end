function readVideoURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        var max_file_size = jQuery('#max_file_size_video').val();

        var exactSize = input.files[0].size / 1024;
        var exactSize = exactSize / 1024;
        var video = jQuery("#jform_profile_tutor_video");
        if (exactSize > max_file_size) {
            video.replaceWith(video.val('').clone(true));
            let message = Joomla.JText._('COM_TEACHING_VIDEO_FILE_SIZE');
            message = message.replace('%s', max_file_size);
            Notifier.error(message);
        } else {
            var storage_teaching = Joomla.getOptions('teaching');
            var formdata = new FormData();
            formdata.append("video", input.files[0]);
            formdata.append(storage_teaching.token, 1);
            jQuery.ajax({
                xhr: function () {
                    let xhr = new window.XMLHttpRequest();
                    let randomPercent = randomNumber(91,99);
                    xhr.upload.addEventListener("progress", function (evt) {
                        if (evt.lengthComputable) {
                            let percentComplete = evt.loaded / evt.total;
                            let toProgress = parseInt(percentComplete) * 100;
                            toProgress = (toProgress > 90) ? randomPercent : toProgress;

                            jQuery('.profile-video .upload-progress-bg').css({
                                width: toProgress + '%'
                            });
                            jQuery('.profile-video .upload-progress-percent')
                                .text(toProgress + '%');
                        }
                    }, false);
                    return xhr;
                },

                url: '/index.php?option=com_teaching&task=completion.update_video&ajax=1',
                data: formdata,
                method: 'POST',
                dataType: 'json',
                accept: 'application/json',
                processData: false,
                contentType: false,
                beforeSend: function () {
                    jQuery('.profile-video #preview').hide();
                    jQuery('.profile-video .review-section-videoSection__iframe__profile').hide();
                    jQuery(`
                                    <div class="upload-content">
                                        <div class="upload-progress">
                                            <span class="upload-progress-bg"></span>
                                            <span class="upload-progress-percent">0%</span>
                                        </div>
                                        <span class="upload-txt">${Joomla.JText._('COM_TEACHING_VIDEO_WAITING_UPLOAD')}</span>
                                    </div>
                                `).insertBefore('.profile-video .repeatable-control.current-file .small.text-primary');
                    jQuery('body').append(`<div class='upload-overlay'></div>`);
                },
                success: function (result) {
                    jQuery('.profile-video .upload-content').remove();
                    jQuery('.upload-overlay').remove();
                    if (result.success) {
                        jQuery('.profile-video #preview').remove();
                        document.getElementById('jform_profile_tutor_video').classList.remove('error');
                        jQuery(document.getElementById('jform_profile_tutor_video').parentElement.parentElement.querySelector('span.error-field-text')).remove();
                        jQuery('.profile-video .review-section-videoSection__iframe__profile').remove();
                        jQuery(`
                            <div class="review-section-videoSection__iframe review-section-videoSection__iframe__profile">
                                    <video width="100%" height="315" controls>
                                        <source src="${result.data.video}" type="video/mp4">
                                    </video>
                            </div>
                        `).insertBefore('.profile-video .repeatable-control.current-file .small.text-primary');
                        Notifier.success(Joomla.JText._('COM_TEACHING_VIDEO_UPLOADED_SUCCESSFULLY'));
                    } else {
                        jQuery('.profile-video #preview').show();
                        jQuery('.profile-video .review-section-videoSection__iframe__profile').show();
                        input.value = '';
                        if (typeof result.message == "object") {
                            result.message.forEach(function (message) {
                                Notifier.warning(message['message']);
                            });
                        } else {
                            Notifier.warning(result.message);
                        }
                    }
                }
            });
        }
    }


    // if (input.files && input.files[0]) {
    //     var reader = new FileReader();
    //     var max_file_size = jQuery('#max_file_size_video').val();
    //
    //     var exactSize = input.files[0].size / 1024;
    //     var exactSize = exactSize / 1024;
    //     var video = jQuery("#jform_profile_tutor_video");
    //     if (exactSize > max_file_size) {
    //         video.replaceWith(video.val('').clone(true));
    //         let message = Joomla.JText._('COM_TEACHING_VIDEO_FILE_SIZE');
    //         message = message.replace('%s', max_file_size);
    //         Notifier.error(message);
    //     } else {
    //         reader.onload = function (e) {
    //             let processData = false,
    //                 contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
    //             let id_field = document.getElementById('jform_id');
    //             if (id_field) {
    //                 if (id_field.value > 0) {
    //                     jQuery.ajax({
    //                         xhr: function () {
    //                             let xhr = new window.XMLHttpRequest();
    //                             xhr.upload.addEventListener("progress", function (evt) {
    //                                 if (evt.lengthComputable) {
    //                                     let percentComplete = evt.loaded / evt.total;
    //                                     jQuery('.profile-video .upload-progress-bg').css({
    //                                         width: percentComplete * 100 + '%'
    //                                     });
    //                                     jQuery('.profile-video .upload-progress-percent')
    //                                         .text(percentComplete * 100 + '%');
    //                                 }
    //                             }, false);
    //                             xhr.addEventListener("progress", function (evt) {
    //                                 if (evt.lengthComputable) {
    //                                     let percentComplete = evt.loaded / evt.total;
    //                                     jQuery('.profile-video .upload-progress-bg').css({
    //                                         width: percentComplete * 100 + '%'
    //                                     });
    //                                 }
    //                             }, false);
    //                             return xhr;
    //                         },
    //
    //                         url: '/index.php?option=com_teaching&task=completion.update_video&ajax=1' + "&id=" + id_field.value,
    //                         data: 'video=' + e.target.result + "&size=" + input.files[0].size + "&" + storage_teaching.token + "=1",
    //                         method: 'POST',
    //                         dataType: 'json',
    //                         accept: 'application/json',
    //                         processData: processData,
    //                         contentType: contentType,
    //                         beforeSend: function (){
    //                             jQuery('.profile-video #preview').hide();
    //                             jQuery('.profile-video .review-section-videoSection__iframe__profile').hide();
    //                             jQuery(`
    //                                 <div class="upload-content">
    //                                     <div class="upload-progress">
    //                                         <span class="upload-progress-bg"></span>
    //                                         <span class="upload-progress-percent">0%</span>
    //                                     </div>
    //                                     <span class="upload-txt">${Joomla.JText._('COM_TEACHING_VIDEO_WAITING_UPLOAD')}</span>
    //                                 </div>
    //                             `).insertBefore('.profile-video .repeatable-control.current-file .small.text-primary');
    //                             jQuery('body').append(`<div class='upload-overlay'></div>`);
    //                         },
    //                         success: function (result) {
    //                             jQuery('.profile-video .upload-content').remove();
    //                             jQuery('.upload-overlay').remove();
    //                             if (result.success) {
    //                                 jQuery('.profile-video #preview').remove();
    //                                 document.getElementById('jform_profile_tutor_video').classList.remove('error');
    //                                 jQuery(document.getElementById('jform_profile_tutor_video').parentElement.parentElement.querySelector('span.error-field-text')).remove();
    //                                 jQuery('.profile-video .review-section-videoSection__iframe__profile').remove();
    //                                 jQuery(`
    //                                     <div class="review-section-videoSection__iframe review-section-videoSection__iframe__profile">
    //                                         <iframe width="100%" height="315px;" src="${result.data.video}?autoplay=0"  webkitallowfullscreen mozallowfullscreen allowfullscreen>
    //                                             </iframe>
    //                                     </div>
    //                                 `).insertBefore('.profile-video .repeatable-control.current-file .small.text-primary');
    //                                 Notifier.success(Joomla.JText._('COM_TEACHING_VIDEO_UPLOADED_SUCCESSFULLY'));
    //                             } else {
    //                                 jQuery('.profile-video #preview').show();
    //                                 jQuery('.profile-video .review-section-videoSection__iframe__profile').show();
    //                                 input.value = '';
    //                                 if (typeof result.message == "object") {
    //                                     result.message.forEach(function (message) {
    //                                         Notifier.warning(message['message']);
    //                                     });
    //                                 } else {
    //                                     Notifier.warning(result.message);
    //                                 }
    //                             }
    //                         }
    //                     });
    //                 }
    //             }
    //         };
    //
    //         reader.readAsDataURL(input.files[0]);
    //     }
    // }
}

function randomNumber(min, max){
    const r = Math.random()*(max-min) + min
    return Math.floor(r)
}

jQuery(document).ready(function ($) {

    $(document).on('change', '#jform_profile_tutor_video', function () {
        const fileName = $(this).val().split('\\').pop();
        $(this).closest('.upload-file').find('.uploaded-file-name').html(`File to upload: ${fileName}`).css('display', 'block');

        readVideoURL(this);
    });
});
