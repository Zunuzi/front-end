jQuery(document).ready(function ($) {

    /*active tab from url*/
    var url = document.location.toString();
    if (url.match('#')) {
        sessionStorage.setItem(window.location.href.toString().split(window.location.host)[1].replace(/&return=[a-zA-Z0-9%]+/, '').replace(/&[a-zA-Z-_]+=[0-9]+/, ''), JSON.stringify(['#' + url.split('#')[1]]));
    }
    var requestMessage = function (task, form) {
        if ($(form).find('.alert-no-items').length > 0)
        {
            return false;
        }
        if (task === 'requests.decline') { // || task === 'instanttutoring.decline'
            var _comment = $('#comment', form);
            var _footer = $('#comment_button',form);
            var modal = $.teaching.runModal({
                header: Joomla.JText._('COM_TEACHING_DECLINE_COMMENT'),
                body: _comment.html(),
                footer: _footer.html()
            });
            
             modal.on('click','#decline',function(){
                    var temp = modal.find('textarea[name="comment"]'),
                        comment = temp.val();    
                    if (comment.length === 0) {
                        temp.parent('.form-group').addClass('has-error').focus();
                        return false;
                    } else {
                        _comment.find('textarea').val(comment);
                        Joomla.submitform(task, form);
                    }
            });
            modal.on('hide.bs.modal', function () {
                return true;
            });

        } else {
            Joomla.submitform(task, form);
        }
    };

    $('[data-request]').on('click', function (e) {
        var type = $(this).data('request'),
            form = $('.tab-pane.active', '#requestsTabsContent').attr('id');
        if (!requestMessage(form + '.' + type, document.getElementById(form + 'Form')))
            $(this).attr('focus', false).blur();
    });

    window.teachingChecked = function (el) {
        Joomla.isChecked(el.checked, document.getElementById($(el).closest('form').attr('id')));
    };

    window.teachingTableOrdering = function (el, order, dir, task) {
        Joomla.tableOrdering(order, dir, task, document.getElementById($(el).closest('form').attr('id')));
    };
    window.requestTableOrdering = function (el, order, dir, task) {
        Joomla.tableOrdering(order, dir, task);
    };
    window.listItemTask = function (id, task) {
        var cb = $('#' + id),
            form = cb.closest('form');
        if (!cb) return false;
        $('[name="' + cb.attr('name') + '"]', form).prop('checked', false);
        cb.prop('checked', true);
        form[0].boxchecked.value = 1;
        requestMessage(task, form[0]);

        return false;
    }

    $(document).on('click', '.tutoring-requests-btn-accept', function (){
        $('body').append(`<div class="loaderGreyBg"></div>`);
        eventWorker.startWork();
    });

});