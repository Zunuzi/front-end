jQuery(document).ready(function ($) {
    var rateTypeChecked = 0,
        input_ratetypes = $('input.input_ratetypes');
    function rateTypeBuild(el) {
        var val = el.val();
        var tr =  $('#tr_rate_levels_'+val);
        if(el.is(':checked')) {
            tr.show();
            $('input', tr).each(function () {
                $(this).removeAttr('disabled');
            });
            return true;
        } else {
            tr.hide();
            $('input', tr).each(function () {
                $(this).attr('disabled', 'disabled');
            });
            return false;
        }
    }
    input_ratetypes.each(function () {
        if(rateTypeBuild($(this))) {
            rateTypeChecked++;
        }
        $(this).click( function(){
            if(rateTypeBuild($(this))) {
                rateTypeChecked++;
            } else {
                rateTypeChecked--;
            }
            if(rateTypeChecked > 0) {
                input_ratetypes.removeAttr('required');
            } else {
                input_ratetypes.attr('required', 'required');
            }
        });
    });
});