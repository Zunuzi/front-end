jQuery(document).ready(function($){

	// Prepare modal defaults
	$.teaching.modalDefaults = {
		// Here we extend core options, see http://getbootstrap.com/javascript/#modals-options

		modalSelector: 'bs3-modal',
		modalClassList: '',
		// If contains something, will add it to modal header <h4> tag.
		// We mostly use it to automatically make modal header from the click button text
		header: false,
		// If to show x close button in the header
		header_dismiss: false,
		// Any additional HTML to place in the modal header
		header_html: '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>',


		// Or body HTML
		body: false,

		// If using AJAX
		url: false,

		// Container selector to be loaded in the modal popup
		// This may be useful if we don't have a raw view but want to load a form from a full document
		// found in the server response. It's a bad practice in most cases, as a raw output demands
		// less recourses then a full page load, even with ?tmpl=raw addition.
		// So try to create raw views for modal when possible
		url_element_selector: 'form.form-validate',

		// Forced to load respone as raw HTML anyway
		url_use_as_raw: false,

		// Set loaded element class. E.g. forms often com with well class which is not needed in our popups
		// So we can set our own classes. See timer.js for example
		url_response_class: '',

		// Surround body contents, e.g. to wrap an ajax returned form
		body_prepend: '',
		body_append: '',

		// Footer HTML if needed
		footer: false,
		// If to adde footer close button
		footer_close_button: false,

		// Modal bootstrap size
		size: 'md', // sm, md, lg

	};



	/**
	 * Prepares options based on the passed DDOM element
	 *
	 * @param   element  $el  DOM element
	 *
	 * @return   object  Object of options
	 */
	$.teaching.prepareOptionsFromElement = function (el) {

		$el = $(el);

		// Get options from the current element
		var options = {};

		options = $el.data();

		// Use click element text at the modal header
		// TODO Think if it's celever. I'm lazy write everwhere twice link text and modal header text. But what if we want a different header?
		if (options.header === true)
		{
			options.header = $el.html();
		}

		// Prepare URL if remote loading is needed
		while (true)
		{
			if ($el.data('remote'))
			{
				options.url = $el.data('remote');
				break;
			}
			if ($el.attr('href') && $el.attr('href') != '#')
			{
				options.url = $el.attr('href');
				if (options.url.indexOf('format=raw') === -1)
				{
					options.url += (options.url.indexOf('?') === -1 ? '?' : '&') + 'format=raw';
				}
				break;
			}
			break;
		}
		return options;
	};

	/**
	 * Handles clicks on modal buttons
	 *
	 * If a modal buttos indends to load some remote data (ajax)
	 * then loads it.
	 * For example used for login and register forms
	 * <a href="<?php echo JRoute::_('?option=com_users&format=raw');?>" data-toggle="modall" data-body="" data-header="" data-footer=""><?php echo JText::_('COM_TEACHING_LOG_IN'); ?></a>
	 * We use data-toggle="modall" to avoid conflict with BS3 data-toggle="modal"
	 *
	 * @param   obeject  $options  Modal options
	 *
	 * @return   jQuery boostrap modal object
	 */
	$.teaching.runModal = function (options)
	{
		options = $.extend(true, {}, $.teaching.modalDefaults, options);

		options.size = 'modal-'+options.size;

		// Remove all possible previous modals
		$('#'+options.modalSelector).modal('hide');
		$('#'+options.modalSelector).remove();
		$('.modal-backdrop').remove();

        options.deferred = $.Deferred();

		// Inject modal HTML into DOM
		var modal = $.teaching.injectModal(options).modal(options);

		$('.custom-sys-message').hide();
        $.when(options.deferred).then(function () {
            modal.trigger('teaching-modal-run');
        });
        $(document).on('teaching-modal-run', function() {
        	$(document).on('hide.bs.modal', function (e) {
        		let form = $(e.target).find('form');
        		if (form.length > 0 && form.attr('action').indexOf('users') > 0) {
					$('.custom-sys-message #system-message-container').empty();
				}
				$('.custom-sys-message').show();
			});
		});
		return modal;
	};

	/**
	 * Prepares bootstrap 3 modal HTML based on passed options
	 * and injects it into DOM
	 *
	 * @param   object  $options  Options
	 *
	 * @return   jQueryObject  Returns injected modal jQuery object
	 */
	$.teaching.injectModal = function(options) {

		var html = ''+
		'<div class="modal fade ' + options.modalClassList + '" id="'+options.modalSelector+'" tabindex="-1" role="dialog" aria-labelledby="'+options.modalSelector+'Label"> ' +
			'<div class="modal-dialog '+ options.size+'" role="document">' +
				'<div class="modal-content">';
				if (options.header_dismiss || ( options.header !== false && options.header.length > 0 ) || options.header_html.length > 0) {
					html += '<div class="modal-header">';
						if (options.header_dismiss) {
							html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
						}
						if (options.header !== true && options.header !== false) {
							html += '<h4 class="modal-title" id="'+options.modalSelector+'Label">'+options.header+'</h4>';
						}
						if (options.header_html.length >0 ) {
							html += options.header_html;
						}
					html +=
					'</div>' +
					'<!-- modal-header !-->';
				}

				html +=
					'<div class="modal-body" data-loading-text="'+Joomla.optionsStorage.teaching.loading_snippet+'">';
						if (options.body ) {

							html += options.body_prepend + options.body + options.body_append;
						}
					html +=
					'</div>' +
					'<!-- modal-body !-->';

				  if ((options.footer && options.footer.length > 0) || options.footer_close_button) {
						html += '<div class="modal-footer">';
							if (options.footer && options.footer.length > 0) {
								html += options.footer;
							}
							if (options.footer_close_button) {
								html += '<button type="button" class="btn btn-default" data-dismiss="modal">'+Joomla.JText._('JLIB_HTML_BEHAVIOR_CLOSE')+'</button>';
							}
						html += '</div>' +
						'<!-- modal-footer !-->';

				  }
				html +=
				'</div>' +
			'</div>' +
		'</div>';

		$('body').append(html);

		if (options.url)
		{
			$.teaching.loadAjaxForm(options);
		}

		// fix issue in IE with disabled inputs in login modal
		setTimeout(function () {
			jQuery('#bs3-modal').find('.modal-body').removeClass('disabled').removeAttr('disabled');

        }, 100);

		return $('body').find('#'+options.modalSelector);
	};

	/**
	 * Loads a remote ajax form into the modal
	 * If the AJAX response is a full page, tries to find the from
	 * to inject only the from, not the whole page.
	 *
	 * @param   string  options  Options object must containe at least modalSelector and url
	 *
	 * @return   type  Description
	 */
	$.teaching.loadAjaxForm = function (options)
	{
		var modal = $('#'+options.modalSelector);
		var modalBody = $('#'+options.modalSelector+' .modal-body');

		// Enable loading text/spinner
		modalBody.button('loading');

		// Add token to GET
		if(options.url.indexOf('?') !== -1) {
			options.url += '&';
		}
		else
		{
			options.url += '/?';
		}

		options.url += Joomla.optionsStorage.teaching.token + '=1&tmpl=raw';


		$.get(options.url, function (data, success, dataType)
		{
			// This is an example line code for the future
			// if (dataType.getResponseHeader("content-type") == 'text/html')

			var $response = parseResponse(data, success, dataType, options);

			modalBody.html($response).prepend(options.body_prepend).append(options.body_append);

			var forms = modalBody.find('form.form-validate');

			for (var i = 0, l = forms.length; i < l; i++)
			{
				// Initialize joomla formvalidator for newly loaded form
				document.formvalidator.attachToForm(forms[i]);

				// Attach AJAX submit event
				$.teaching.formSubmitAttachEvent(forms[i]);

				// Add hidden ajax field to let Jooma know it's an ajax form'
				$('<input>').attr({
					 type: 'hidden',
					 name: 'ajax',
					 value: '1',
					 'data-form-uri': options.url,
				}).appendTo(forms[i]);

				// Init click for modal popups found in the newly injected forms
			}
			$.teaching.autoloader(modalBody);

            options.deferred.resolve();

		});
	};


	var parseResponse = function (data, success, dataType, options)
	{
		options = options || false;
		// Force raw output
		if (options.url_use_as_raw)
		{
			return data;
		}

		var $response;
		var content_type = dataType.getResponseHeader("content-type").split(';');



		switch (content_type[0])
		{
			case 'application/json':
				return data;

			case 'text/html':
			default:
				if (options.getMessagesOnJSONFail)
				{
					// Prepare standard JSON object we use
					response = {
						data: {
							status:false,
						},
						success: false,
					};
					response.message = $response = $('<span/>').html(data).find('#system-message').html() || '<div class="alert alert-danger" role="alert">'+Joomla.JText._('COM_TEACHING_FAILED_TO_LOAD_REMOTE_CONTENT')+'</div>';

					var $obj = $($('<span/>').html(response.message).find('div.alert')[0]);
					var statuses = ['success', 'info', 'danger', 'warning'];
					for (i = 0; i < statuses.length; i++)
					{
						if ($obj.hasClass('alert-' + statuses[i]))
						{
							response.data.status = statuses[i];
							break;
						}
					}

					return response;
				}

				$response = $('<span/>').html(data);

				if (options.url_element_selector && options.url_element_selector.length)
				{
					$response = $('<div class="container"/>').html(data);

					if ($response.find(options.url_element_selector).length)
					{
						$response = $response.find(options.url_element_selector);
					}

					if (options.url_response_class && options.url_response_class.length)
					{
						$response.attr('class', options.url_response_class);
					}
				}
				else if (options.url_response_class && options.url_response_class.length)
				{
					$response.attr('class', options.url_response_class);
				}
				break;
		}

		$($response[0][8]).val($('#hiddenCaptcha').val());
		return $response;
			/*
		if (!$response.length)
		{
			// I'm not sure how to better behave - either to load response as is, or to show an error message
			$response = responseText;
			// var $response = $('<div class="alert alert-danger" role="alert">'+Joomla.JText.strings.COM_TEACHING_FAILED_TO_LOAD_REMOTE_CONTENT+'</div>');
		}
		*/
	};

	/**
	 * Handles submits for ajax forms
	 *
	 * Posts the given form
	 * Places placeholders and disables forms during loading ajax data
	 * Outpuuts the response
	 *
	 * @param   DOM element  form  Form to be used for ajax posts
	 *
	 * @return   void
	 */
	$.teaching.formSubmitAttachEvent = function (form)
	{
		$.teaching.autoPopulateHiddenFieldsonSubmit($(form).parent());

		$(form).submit(function(e)
		{

			e.preventDefault();

			var $form = $(this);
			var url = $form.attr( "action" );
			var formData = $form.serialize();

			var $response = $form.find(".response");

			if(!$response.length)
			{
				$form.prepend('<div class="response"></div>');
				$response = $form.find(".response");
			}

			$response.data('loading-text', Joomla.optionsStorage.teaching.loading_snippet);
			$response.button('loading');
			$response.removeClass('error');

			var $form_fieldsets = $form.find('fieldset');
			$form_fieldsets.prop('disabled', true);

			var posting = $.post( url, formData );

			posting.done(function( data, success, dataType )
			{
				var response = parseResponse(data, success, dataType, {getMessagesOnJSONFail: true});

				$form_fieldsets.prop('disabled', false);

				if (!response.success)
				{
					$response.addClass('error');
				}

				switch (response.data.status)
				{
					case 'expired_session':
					case 'invalid_token':
						$form_fieldsets.prop('disabled', true);
						// Create a deferred object
						$.teaching.addProgressBar($form, 2).done(function() {
							var modalSelector = '#'+$form.closest('div.modal').attr('id');
							var url = $form.find('input[name="ajax"]').data('form-uri');
							$.teaching.loadAjaxForm(modalSelector, url);
						});
						break;
					case 'login_falure':
						if (response.data.wait)
						{
							$form_fieldsets.prop('disabled', true);
							setTimeout(function() {
								// return;
									$form_fieldsets.prop('disabled', false);
									$response.empty();
							}, response.data.wait*1000+1000);
							$.teaching.addProgressBar($form, response.data.wait);
						}
						break;
					case 'success':
						$form_fieldsets.prop('disabled', true);

						setTimeout(function() {
								$form.closest('.modal').modal('hide');
								$('#system-message-container').html(response.message);
						}, 3000);

						break;
					default:
				}

				$form.find(".response").empty().html(response.message);
				if (response.data.redirect) {
					window.location = response.data.redirect;
				}
				// $("#register").modal();
			});
		});
	};


	/**
	 * Adds a progress bar and runs it
	 *
	 * @param   $object  $form           jQuery form object where to add the progress bar
	 * @param   int      $max            Time in seconds to run the progress
	 * @param   bool     $emptyResponse  Wether to empty .response area
	 *
	 * @return   $.Deferred  Returns deferred object to be able to perform some action when the progress is completed
	 */
	$.teaching.addProgressBar = function($form, max, emptyResponse)
	{
		var d = new $.Deferred();
		emptyResponse = emptyResponse || true;

		// Get progress bar HTML and append it to the form
		var $bar = $(Joomla.optionsStorage.teaching.progress_bar);
		$form.prepend($bar);

		// Run progress bar
		var time = 0;
		var speed = 5; // 1 - one step per second, 2 - 2 steps per second and so on
		int = setInterval(function()
		{
			time = time+1/speed;
			var width = (100 - (100 * time / max));
			$form.find('.progress-bar').width(width + '%');
			$form.find('.progress-bar').attr('valuenow', width);

			if (time-1/speed >= max)
			{
				$form.find('fieldset').prop('disabled', false);

				if(emptyResponse)
				{
					$form.find('.response').empty();
				}

				$bar.hide('1000', function(){ $bar.remove(); });
				d.resolve();
				clearInterval(int);
			}
		}, 1000/speed);
		return d;
	};

});
