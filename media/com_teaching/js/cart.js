jQuery(document).ready(function ($) {
    var storage_teaching = Joomla.getOptions('teaching');
    window.listItemTask = function (id, task) {
        jQuery.ajax({
            url: '?option=com_teaching&task='+task + '&ajax=1',
            type: 'POST',
            data: 'cid[]='+id + '&' + storage_teaching.token + '=1',
            dataType: 'json',
            success: function (result) {
                let data = result.data;
                if (data.status === 1) {
                    // Notifier.success(data.message);
                    jQuery('tr#item'+id).remove();
                    location.reload();
                } else {
                    Notifier.warning(data.message, 'Warning');
                }
            },
            error: function (rq, err) {
                console.log(err);
            }
        });
    }

    $(document).on('click', '.clearCartBtn', function (){
        let clearCart = {
            header: `${Joomla.JText._('COM_TEACHING_CONFIRMATION')}`,
            body_append: `<div class="removePopupBody text-right">
                                <button id="mod_yes_btn" type="button" class="btn btn-primary emptyCartBtn">
                                    ${Joomla.JText._('JYES')}
                                </button>
                                <button id="mod_no_btn" type="button" class="btn" data-dismiss="modal" aria-label="Close">
                                    ${Joomla.JText._('JNO')}
                                </button>
                            </div>`,
            modalSelector: 'confirm',
            body: `<span>${Joomla.JText._('COM_TEACHING_CLEAR_CART_CONFIRMATION')}</span>`,
            footer_close_button: false
        };
        jQuery.teaching.runModal(clearCart);
    })

    $(document).on('click', '.emptyCartBtn', function (e) {
        e.preventDefault();
        $('#confirm').modal('hide');
        this.setAttribute('disabled', 'disabled');
        Joomla.submitbutton('cart.clear');
    });

    $(document).on('click', '.removeCartItem', function (){
        let ID = $(this).data('id');
        let clearCartItem = {
            header: `${Joomla.JText._('COM_TEACHING_CONFIRMATION')}`,
            body_append: `<div class="removePopupBody text-right">
                                <button id="mod_yes_btn" type="button" class="btn btn-primary removeCartItemBtn" data-id="${ID}">
                                    ${Joomla.JText._('JYES')}
                                </button>
                                <button id="mod_no_btn" type="button" class="btn" data-dismiss="modal" aria-label="Close">
                                    ${Joomla.JText._('JNO')}
                                </button>
                            </div>`,
            modalSelector: 'confirm',
            body: `<span>${Joomla.JText._('COM_TEACHING_CLEAR_CART_ITEM_CONFIRMATION')}</span>`,
            footer_close_button: false
        };
        jQuery.teaching.runModal(clearCartItem);
    });


    $(document).on('click', '.removeCartItemBtn', function () {
        let ID = $(this).data('id');
        console.log(ID)
        $('#confirm').modal('hide');
        listItemTask(ID,'cart.delete');
    });

    let minimumRefill = $('input[name="minimum-refill"]').val();
    $('input[name="use_gift"]').on('change', function() {
        let total_balance = $('.your-total-balance').data('value');
        let gift_balance = $('.your-gift-balance').data('value');
        let total_price = $('.total-price').data('value');
        let refill_balance_button = $('.refill-balance-button').attr('href');
        let needToRefill;
        let currentNeedToRefill = $('span.minimum-refill').html();
        if($(this).is(":checked")) {
            total_balance = total_balance + gift_balance;
            needToRefill = total_price - total_balance;
            $('.your-total-balance').data('value', total_balance);
            $('.your-total-balance span').html(total_balance);
            refill_balance_button = refill_balance_button.replace("use_gift=false", "use_gift=true");
        } else {
            total_balance = total_balance - gift_balance;
            needToRefill = total_price - total_balance;
            $('.your-total-balance').data('value', total_balance);
            $('.your-total-balance span').html(total_balance);
            refill_balance_button = refill_balance_button.replace("use_gift=true", "use_gift=false");
        }
        if (needToRefill < minimumRefill) {
            needToRefill = minimumRefill;
        }
        refill_balance_button = refill_balance_button.replace("sum="+currentNeedToRefill, "sum="+needToRefill);
        if (total_balance >= total_price) {
            $('.pay-button').prop('disabled', false);
            $('.refill-balance').css('display', 'none');
        } else {
            $('.pay-button').prop('disabled', true);
            $('.refill-balance').css('display', 'inline');
            if (needToRefill > minimumRefill) {
                $('.minimum-refill-information').css('display', 'none');
            } else {
                $('.minimum-refill-information').css('display', 'inline');
            }
            $('span.minimum-refill').html(needToRefill);
        }
        $('.refill-balance-button').prop('href', refill_balance_button);
    });

    let displaySignInModal = $.teaching.getUrlParameter('payed_successfully');
    if (displaySignInModal == 'true') {
        jQuery('.for-close-modal').fadeIn();
        jQuery('.cart-page.my-balance-payment-modal.my-balance-payment-modal-message.success-messages').fadeIn();
    }
    jQuery('.cart-page .payment-modal-close-btn, .cart-page.for-close-modal').on('click', function () {
        jQuery('.cart-page.my-balance-payment-modal, .cart-page.for-close-modal').fadeOut();
        window.location.href = '/index.php?option=com_teaching&view=calendar';
    });
});