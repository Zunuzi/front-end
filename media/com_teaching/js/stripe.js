Joomla.tableOrdering = function (el, order, dir, task) {
    var form = document.adminForm;

    form.filter_order.value = order;
    form.filter_order_Dir.value = dir;
    document.adminForm.submit(task);
}
var clientSecret = '';
var clientUrl = '';
let elements, cardNumber, cardCvc, cardDate;

setTimeout(generatePaymentForm, 300);

function generatePaymentForm() {
    document.getElementById('submit').disabled = true
    var publishkey = 'pk_test_51HedooAtKGhka9CkFc2SjzQtNje0CDpLb4uQj01F6lS87JgIZH0RD4thjhy3DzOoRV8AbhNhi4xfPyAWygQ4cZ6A0055Hhm8JG';
    // Create an instance of the Stripe object with your publishable API key
    var stripe = Stripe(publishkey);
    elements = stripe.elements();
    var style = {
        base: {
            color: "#32325d",
            lineHeight: "18px",
            ':focus': {
                lineHeight: "18px"
            },
        }
    };

    cardNumber = elements.create("cardNumber", {style: style});
    cardCvc = elements.create("cardCvc", {style: style});
    cardDate = elements.create("cardExpiry", {style: style});
    var errors = {date: 'date', cvc: 'cvc', card:'card'};

    cardNumber.mount("#card-element");
    cardCvc.mount("#card-cvc");
    cardDate.mount("#card-date");

    cardNumber.on('change',function (e) {
        const displayError = document.getElementById('card-errors');
        if (e.error) {
            displayError.textContent = e.error.message;
            if (typeof errors.card == "undefined"){
                errors.card = 'card'
            }
        } else if(e.complete) {

            displayError.textContent = '';
            delete errors.card
            payButton(errors)
        }else{
            // for undefined
            errors.card = 'card'
            payButton(errors)
        }
    })

    cardDate.on('change',function (e) {
        const displayError = document.getElementById('date-errors');
        if (e.error) {
            displayError.textContent = e.error.message;
            if (typeof errors.date == "undefined"){
                errors.date = 'date'
            }
        } else if(e.complete) {

            displayError.textContent = '';
            delete errors.date
            payButton(errors)
        }else{
            // for undefined
            errors.date = 'date'
            payButton(errors)
        }
    })

    cardCvc.on('change',function (e) {
        const displayError = document.getElementById('cvc-errors');
        if (e.error) {
            displayError.textContent = e.error.message;
            if (typeof errors.cvc == "undefined"){
                errors.cvc= 'cvc';
            }
        } else if(e.complete) {

            displayError.textContent = '';
            delete errors.cvc
            payButton(errors)
        }else{
            // for undefined
            errors.cvc= 'cvc';
            payButton(errors)
        }
    })


    var form = document.getElementById('payment-form');


    form.addEventListener('submit', function (ev) {
        document.getElementById('submit').disabled = true

        ev.preventDefault();
        jQuery('#loader').css('display', 'block');
        fetch('/index.php?option=com_teaching&task=payments.setkey').then(function (response) {
            return response.json();
        }).then(function (responseJson) {
            clientSecret = responseJson.client_secret;
            clientUrl = responseJson.client_url;


            stripe.confirmCardPayment(clientSecret, {
                payment_method: {
                    card: cardNumber,

                    billing_details: {
                        name: 'Stripe Payment'
                    }
                },
                return_url: clientUrl,
            }).then(function (result) {
                jQuery('#payment-form .error').html('');
                if (result.error) {
                    paymentFalse(result.error.message, true);
                } else {
                    if (result.paymentIntent.status === 'succeeded') {
                        jQuery.ajax({
                            url: clientUrl + '&' + Joomla.getOptions('teaching').token + '=1',
                            success: function (data) {
                                // console.log(data.data.return_to);
                                if (data.data.result) {
                                    if (data.data.return_to == '') {
                                        paymentSuccess(jQuery('input[name="jform[purchase_value]"]').val(), true);
                                        jQuery('#loader').css('display', 'none');
                                    } else {
                                        window.location.href = data.data.return_to;
                                    }
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                Notifier.success(Joomla.JText._('ERROR'));
                            }
                        });
                    }
                }
                // jQuery('#loader').css('display', 'none');
            });
        });

    });
}
jQuery(function (){
    jQuery('.payment-modal-close-btn, .for-close-modal').on('click', function () {
        jQuery('.my-balance-payment-modal, .for-close-modal').fadeOut();
        jQuery('#payment-form .error').html('');
        cardNumber.unmount();
        cardCvc.unmount();
        cardDate.unmount();
        setTimeout(function (){
            cardNumber.mount("#card-element");
            cardCvc.mount("#card-cvc");
            cardDate.mount("#card-date");
        }, 300);
    });
});

function payButton(errors) {
    if (Object.keys(errors).length === 0) {
        document.getElementById('submit').disabled = false
    } else {
        document.getElementById('submit').disabled = true
    }
}