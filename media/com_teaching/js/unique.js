jQuery(document).ready(function ($) {

    var unique = $('[data-unique]');

    $('form').on('change', '[data-unique]', function () {
        processUnique(this);
        $(this).attr('data-oldval', $(this).val());
    });

    unique.each(function () {
        processUnique(this);
    });

    $('.subform-repeatable .group-remove').on('click', function (e) {
        let $el = $(e.target).closest('.subform-repeatable-group');
        let sel = $el.find('select');
        if (sel && sel[0]) {
            if ($el.closest('.profile-edit').length === 0){
                sel[0].selectize.clear(true);
            }
        }
    });
    $('div.subform-repeatable').on('subform-row-add subform-row-remove', function (row) {
        $('.subform-repeatable .group-remove').on('click', function (e) {
            let $el = $(e.target).closest('.subform-repeatable-group');
            let sel = $el.find('select');
            if (sel && sel[0]) {
                if ($el.closest('.profile-edit').length === 0){
                    sel[0].selectize.clear(true);
                }
            }
        });
        $('[data-unique]', this).each(function () {
            processUnique(this);
        });
    });


});

function processUnique(el) {
    (function ($) {
        var old_val = $(el).attr('data-oldval'),
            new_val = $(el).val(),
            unique = $(el).data('unique');
        $('[data-unique="' + unique + '"]').each(function () {
            var select = $(this);
            if ($(el).attr('name') != select.attr('name')) {
                if (new_val || old_val) {
                    $('option:not(:selected)', select).each(function (i, option) {

                        if ($(option).prop('value') == new_val) {
                            $(option).prop('disabled', 'disabled');
                        }
                        if (new_val != old_val && $(option).attr('value') == old_val) {
                            $(option).prop('disabled', false);
                        }
                    });
                    if (select[0].hasOwnProperty('selectize')) {
                        select[0].selectize.refreshOptions(false);
                        if (select[0].selectize.options[new_val]) {
                            select[0].selectize.updateOption(new_val, {
                                'disabled': true,
                                'value': new_val,
                                'text': select[0].selectize.options[new_val].text
                            });
                        }
                        if (old_val !== '' && new_val !== old_val) {
                            let text = select[0].selectize.options[old_val] ?
                                select[0].selectize.options[old_val].text : ';'
                            select[0].selectize.updateOption(old_val, {
                                'disabled': false,
                                'value': old_val,
                                'text': text
                            });
                        }
                    }
                } else {
                    var val = select.val();
                    if (val) {
                        $('option[value="' + val + '"]', el).prop('disabled', 'disabled');
                    }
                }
                select.trigger('liszt:updated');
            }
        });
    })(jQuery);
}
