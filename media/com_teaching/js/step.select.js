(function($){
    $.stepSelect = function(el, data, params){
        var base = this;

        base.$el = $(el);
        base.el = el;

        base.$el.data("stepSelect", base);

        base.init = function() {
            if( typeof( data ) === "undefined" || data === null ) data = [];

            base.data = data;
            base.row = base.$el.parent();

            base.params = $.extend({},$.stepSelect.defaultParams, params);

            base.select = $('.skill-select', base.row);

            var val = base.el.value;

            $.each(data, function(k, v) {
                var css = 'list-group-item';
                if(val == v.id) css += ' active';
                if(v.children.length) css += ' parent';
                $('<a/>', {"class": css, text: v.name, rel: v.id, href: '#'}).data('idx', k).appendTo(base.select.get(0));
            });

            base.select.on('click', 'a', function(e) {
                e.preventDefault();
                var a = $(this), p = a.hasClass('parent');
                if(a.hasClass('active'))
                {
                    if(p)
                        base.select.get(1).innerHTML = '';
                    base.el.value = '';
                }
                else
                {
                    $('a', a.parent()).removeClass('active');
                    if(p && base.data[a.data('idx')].children.length)
                    {
                        base.select.get(1).innerHTML = '';
                        $.each(base.data[a.data('idx')].children, function(k, v) {
                            $('<a/>', {
                                "class": 'list-group-item',
                                text: v.name,
                                rel: v.id,
                                href: '#'
                            }).appendTo(base.select.get(1));
                        });
                    }
                    base.el.value = p ? 0 : this.rel;
                }
                base.$el.trigger('change');
                a.toggleClass('active');
                return false;
            });
        };

        base.init();
    };

    $.stepSelect.defaultParams = {
        data: []
    };

    $.fn.stepSelect = function(data, params){
        return this.each(function(){
            (new $.stepSelect(this, data, params));
        });
    };

})(jQuery);