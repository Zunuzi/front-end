var storage_teaching = Joomla.getOptions('teaching');

function proSkillSubject(el) {
    var _this = jQuery(el),
        val = _this.val(),
        id = _this.attr('id'),
        subject_id = id.replace('_area', '_subject'),
        form = _this.closest('form');

    if (val) {
        var subject = _this.closest('fieldset').find('#' + subject_id);
        changeSubject(subject);
    }
}

function changeSubject(el) {
    var _subject = jQuery(el);
    if (_subject.val().length) {
        _subject.removeClass('error');
    } else {
        _subject.addClass('error').attr('data-validation-error', Joomla.JText._('COM_TEACHING_ENTER_SUBJECT'));
    }
    ValidationHelper.check_validate(_subject.closest('form'));
}

function getAge(DOB) {
    var today = new Date();
    var birthDate = new Date(DOB.replace(/-/g, "/"));
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age = age - 1;
    }
    return age;
}

function initTutorSinceList(onload = false) {

    jQuery('div.tutor-since').find('select').each(function () {
        var $ = jQuery;
        var since_field = $(this);
        var dob_field = $('input.date-of-birth');
        var $since_field = null;
        var begin_year;
        $since_field = since_field.selectize({
            valueField: 'value',
            labelField: 'text',
            searchField: ['text'],
            sortField:  [
                {
                    field: 'text',
                    direction: 'desc'
                }
            ],
            create: false,
            render : {
                option: function(data, escape) {
                    return `<div class="option ${data.disabled ? 'hasPopover' : ' '}" 
                            data-content="${Joomla.JText._('COM_TEACHING_TUTORING_SINCE_HOVER_INFORMATION').replace("%c", getAge($('#jform_profile_default_dob').val())).replace("%m", $('#jform_profile_default_dob').data('minage'))}">
                            ${escape(data['text'])}</div>`;
                    }
            },
            onLoad: function() {
                setTimeout(function(){
                    $(document).find('.selectize-dropdown-content .option.hasPopover').popover({
                        placement: 'top',
                        container: 'body',
                        trigger: 'hover',
                        html: true,
                        content: function () {
                            return $(this).attr('data-validation-error');
                        }
                    });
                }, 1000);

            },
        });
        $since_field = $since_field[0].selectize;

        if (!onload) {
            if (!dob_field.val()) {
                $($('ul.nav.nav-tabs').children('li')[0]).click();
                $since_field.disable();
                $since_field.clearOptions();
                $since_field.clear();
                dob_field.focus();
                Notifier.warning(Joomla.JText._('COM_TEACHING_FIRST_INPUT_BIRTH'));
                return false;
            }
        } else {
                dob_field.on('change', function () {
                    initTutorSinceList(doDobValidation);
                    if(onload && doDobValidation) {
                        doDobValidation=false;
                    }
                });
        }
        begin_year = new Date(dob_field.val().replace(/-/g, "/")).getFullYear() + 16;
        if ($(this).find(':selected').length > 0 && $(this).find(':selected')[0].value.length === 4) {
            if ($(this).find(':selected')[0].value < begin_year) {
                Notifier.warning(Joomla.JText._('COM_TEACHING_FRONT_RESELECT_SINCE_YEAR'));
                $since_field.clear();
            }
        }


        $since_field.disable();
        $since_field.clearOptions();

        $since_field.load(function (callback) {
            let data = [];
            for (let i = 1950; i <= (new Date().getFullYear()); i++) {
                let disable = true;
                if(i >= begin_year) {
                    disable = false;
                }
                data.push(
                    {
                        'value': i,
                        'text': i,
                        'disabled': disable
                    }
                );
            }
            $since_field.enable();
            callback(data);
            $('.tutor-since .selectize-input.items').addClass('has-options')
        });
    });
}


function verifyAge(since_year) {
    var $ = jQuery;
    var dob_field = $('input.date-of-birth');
    var begin_year = new Date(dob_field.val()).getFullYear() + 16;
    var tutoring_since = parseInt(since_year); //jQuery(el).children(':selected').val());

    if (tutoring_since <= begin_year) {
        initTutorSinceList();
        return false;
    }
    return true;
}

function changeAboutMe(el) {
    var _this = jQuery(el),
        val = _this.val(),
        id = _this.attr('id'),
        language = jQuery('#' + id.replace('_text', '_language')),
        chzn = jQuery('#' + id.replace('_text', '_language_chzn')),
        form = _this.closest('form');

    var check_language = function () {
        if (val && !language.val()) {
            language.addClass('error').attr('data-validation-error', Joomla.JText._('COM_TEACHING_ENTER_LANGUAGE'));
            chzn.addClass('error');
        } else {
            language.removeClass('error').removeAttr('data-validation-error');
            chzn.removeClass('error');
        }
        language.trigger("chosen:updated");
    };

    check_language();

    language.on('change', function () {
        check_language();
        ValidationHelper.check_validate(_this.closest('form'));
    });

    ValidationHelper.check_validate(_this.closest('form'));
}

function initInstitutionsSiteField() {
    let el = jQuery('#jform_profile_institution_site, #jform_profile_educational_site');
    if (el.length > 0) {
        if (el.val().length == 0 || el.val().search('https') !== 0) {
            el.val('https://' + el.val());
        }
    }
}

jQuery(document).ready(function ($) {
    let activeTab = $.teaching.getUrlParameter('tab');
    if(activeTab) {
        $('a#tab-' + activeTab).click();
    }

    $.teaching.displayCertificateFile();

    // jQuery('.professional-trainings').selectize({
    //     valueField: 'value',
    //     labelField: 'name',
    //     sortField: [
    //         {
    //             field: 'name',
    //             direction: 'asc'
    //         }
    //     ],
    // });

    $('.sorted-alphabetically select').selectize({
        sortField: [
            {
                field: 'text',
                direction: 'asc'
            }
        ],
    });

    $('.uploadCertificateReplace input[type="file"]').addClass('supported');

    $(document).on('change', '.uploadCertificateReplace input', function() {
        let removeFile = $('input[name="' + $(this).attr('name') + '[delete-file]' +'"]');
        if(removeFile.length > 0) {
            removeFile.prop('checked', true);
        }
    });

    function setUploadedFileUrl(){
        let upload_img = $('.teaching-file .uploadCertificateImg');

        if (upload_img.length){

            $.each( upload_img, function(key) {
                let uploadUrl = $(this).closest('.uploadCertificateUploaded').find('.uploadCertificateReplace > input.bg-primary').data('value');
                let fileFormat = uploadUrl.split(/\.(?=[^\.]+$)/)[1];
                let url = document.location.origin;

                if (fileFormat === 'pdf'){
                    pdfjsLib.getDocument(`${url}/${uploadUrl}`).then(doc => {
                        doc.getPage(1).then(page => {
                            let myCanvas = document.getElementById('pdf_canvas_jform_profile__tutor__certificates__certificates' + key + '__file');
                            let context = myCanvas.getContext('2d');
                            let viewport = page.getViewport(0.3);
                            myCanvas.width = 70;
                            myCanvas.height = 55;

                            page.render({
                                canvasContext: context,
                                viewport: viewport
                            })
                        })
                    });
                } else{
                    $(this).html(`
                        <img src="${url}/${uploadUrl}" alt="certificate" />
                    `);
                }
            });

        }
    }

    var transferRadio = function (field) {

        let paypalDiv = $('#payment-account-elements-container');
        let stripe_countries = $('.payment_country');
        let stripe_payment_account = $('#payment-account-elements-container');

        if (!field || field.length === 0) {
            paypalDiv.hide();
            stripe_countries.hide();
            stripe_payment_account.hide();
            jQuery('#jform_profile_tutor_payment_account-lbl').hide();
            return;
        }

        jQuery('#jform_profile_tutor_payment_account-lbl').show();

        switch (field.val().toLocaleLowerCase()) {
            case 'stripe':
                paypalDiv.hide();
                stripe_countries.show();
                stripe_payment_account.html('');
                stripe_payment_account.show();
                $.teaching.prepareStripePaymentAccount($(Joomla.getOptions('teaching').profile.stripe_fields_container_selector)[0]);
                jQuery(Joomla.getOptions('teaching').profile.stripe_bank_country_selector).on('change', (e) => {
                    jQuery.teaching.prepareStripePaymentAccount(jQuery(Joomla.getOptions('teaching').profile.stripe_fields_container_selector)[0]);
                });
                break;
            case 'paypal':
                stripe_payment_account.hide();
                stripe_countries.hide();
                paypalDiv.html('');
                paypalDiv.show();
                $.teaching.preparePayPalPaymentAccount($(Joomla.getOptions('teaching').profile.paypal_fields_container_selector)[0]);
                break;
            case 'payoneer':
                paypalDiv.hide();
                stripe_payment_account.hide();
                stripe_countries.hide();
                paypalDiv.html('');
                paypalDiv.show();
                $.teaching.preparePayoneerPaymentAccount($(Joomla.getOptions('teaching').profile.payoneer_fields_container_selector)[0]);
                break;
            case 'debit_card':
                stripe_payment_account.hide();
                stripe_countries.hide();
                paypalDiv.html('');
                paypalDiv.show();
                $.teaching.prepareDebitCardPaymentAccount(paypalDiv[0]);
                break;
            default:
                paypalDiv.hide();
                stripe_countries.hide();
        }
    };

    $('#jform_profile_tutor_transfer').on('change', 'input[type="radio"]', function (e) {
        transferRadio($(this));
    });

    $('#jform_profile_tutor_video').on('change', function () {
        const max_file_size_text = $(this).data('size');
        const max_file_size = parseInt(max_file_size_text.split('M')[0]);
        const fileSize = this.files[0].size / 1024 / 1024;
        const video = $("#jform_profile_tutor_video");
        const defaultMessage = Joomla.JText._('COM_TEACHING_FILE_SIZE');
        const errMessage = defaultMessage.slice(0, defaultMessage.lastIndexOf(' ') + 1) + max_file_size_text;
        const fileName = $(this).val().split('\\').pop();

        if (fileSize > max_file_size) {
            video.val('');
            video.blur();
            setTimeout(function () {
                video.attr('aria-invalid', true);
            }, 1000);
            Notifier.error(errMessage);
        }

        $(this).closest('.upload-file').find('.uploaded-file-name').html(`File to upload: ${fileName}`).css('display', 'block');

    });

    transferRadio($('#jform_profile_tutor_transfer input:checked'));

    initTutorSinceList(true);
    initInstitutionsSiteField();
    jQuery('#jform_profile_institution_site, #jform_profile_educational_site').on('change', initInstitutionsSiteField);
    document.formvalidator.setHandler('tutoring_since', verifyAge);

    /// country phone code
    $('#jform_profile_default_country').on('change', function (){
        const val = $(this).val();
        const token = Joomla.getOptions('csrf.token');
        const url = "/index.php?option=com_teaching&task=profile.getCountryCode";
        var data = {
            id: val,
            [storage_teaching.token]: 1
        }
        if(val != '') {
            $.ajax({
                url: url,
                dataType: "json",
                accept: {
                    json: 'application/json'
                },
                type: 'POST',
                data: data,
                success: function (data) {
                    let code = data.data.phone_code.phone_code;
                    code = code[0] === '+' ? code : '+' + code;
                    const phone = $('#jform_profile_default_phone');

                    if (phone.val().length <= 7) {
                        phone.val(code)
                    }
                }
            });
        }

    });

    $('#LeaveReview .star-cb-group input[type="radio"]').on('change', function () {
        $('#LeaveReview > button[type="submit"]').removeClass('disabled');
        $('#LeaveReview > button[type="submit"]').prop("disabled", false);
    });

    setUploadedFileUrl();

    let noError = [];
    let hasError = [];
    let hasErrorMessages = [];
    let errorFieldTitle;
    $(document).on('click', '.tutor-profile.profile-edit .controls button[type="submit"]', function (e){
        if ($('.tutor-profile.profile-edit #what-i-can-teach-free-short-video-lessons').hasClass('active')){
            e.preventDefault();
            setTimeout(function(){
                $('.tutor-profile.profile-edit #member-profile #what-i-can-teach-free-short-video-lessons').find(':input').parent('.selectize-input').each(function(){
                    if ($(this).hasClass('error')){
                        if($(this).parents().eq(9).find('h2.rl_tabs-title').find('a.anchor').length > 0) {
                            errorFieldTitle = $.trim($(this).parents().eq(9).find('h2.rl_tabs-title').first().text());
                        } else {
                            errorFieldTitle = $.trim($(this).parents().eq(8).find('label.control-label').first().text());
                        }
                        if (hasErrorMessages.indexOf(errorFieldTitle) == -1) {
                            hasErrorMessages.push(errorFieldTitle);
                        }
                        hasError.push($(this));
                    } else{
                        noError.push($(this));
                    }
                });
                if (noError.length && hasError.length){
                    let message = Joomla.JText._('COM_CONTENT_REMOVE_EMPTY_FIELDS_MODAL_CONTENT');
                    message = message.replace('%s', hasErrorMessages.join(', '));
                    let removeItemsProfile = {
                        header: `${Joomla.JText._('COM_TEACHING_CONFIRMATION')}`,
                        body_append: `<div class="removeItemsProfile text-right">
                                <button id="mod_yes_btn" type="button" class="btn btn-primary">
                                    ${Joomla.JText._('COM_CONTENT_RATING_DELETE_MODAL_CONFIRM')}
                                </button>
                                <button id="mod_no_btn" type="button" class="btn" data-dismiss="modal" aria-label="Close">
                                    ${Joomla.JText._('COM_CONTENT_RATING_DELETE_MODAL_CANCEL')}
                                </button>
                            </div>`,
                        modalSelector: 'confirm',
                        body: `<span>` + message + `</span>`,
                        footer_close_button: false
                    };
                    jQuery.teaching.runModal(removeItemsProfile);
                } else {
                    let checkError = true;
                    $('.tutor-profile.profile-edit #member-profile').find(':input').each(function(){
                        if ( $(this).hasClass('error') || $(this).parent('.selectize-input').hasClass('error') ){
                            checkError = false;
                        }
                    });
                    if (checkError){
                        $('.tutor-profile.profile-edit #member-profile').submit();
                    }
                }
            }, 1000);
        }
    });

    $(document).on('click', '.removeItemsProfile .btn-primary', function (e) {
        e.preventDefault();
        $('#confirm').modal('hide');
        $(hasError).each(function(index, value){
            value.closest('.subform-repeatable-group').remove();
        });
        hasError = [];
        noError = [];
        $(document).find('.tutor-profile.profile-edit .controls button[type="submit"]')
            .removeClass('disabled').click()
            .find('i').removeClass('fa-close').addClass('fa-check-circle-o');
    });

    $(document).on('click', '.group-remove', function() {
        if($(window).width() <= 768) {
            $(document).find('.popover.fade').remove();
        }
    });

    $(document).on('change', '#what-i-can-teach-free-short-video-lessons select', function(){
        let freeVideoSubjectText;
        if ($(this).hasClass('country')) {
            freeVideoSubjectText = $(this).text() + ' ' + $(this).closest('fieldset').find('select.province').text();
        } else if ($(this).hasClass('province')) {
            freeVideoSubjectText = $(this).closest('fieldset').find('select.country').text() + ' ' + $(this).text();
        } else {
            freeVideoSubjectText = $(this).text();
        }
        $(this).closest('fieldset').find('div.free_videos_row input.free_video_subject').val(freeVideoSubjectText);
    });

    let aboutMeTextarea = $('.about-me textarea');
    let aboutMeTextareaLeftCount;
    if (aboutMeTextarea.length > 0) {
        var aboutMeTextareaCurrentCount = aboutMeTextarea.val().length;
        var aboutMeTextareaMaxLength = aboutMeTextarea.attr('maxlength');
        aboutMeTextareaLeftCount = changeAboutMeLeftCount(aboutMeTextareaCurrentCount)
        $('<span style="float: right;"><span class="left-count">'+ aboutMeTextareaLeftCount +'</span> characters left</span>').insertAfter(aboutMeTextarea);
    }

    $('.about-me textarea').on('input', function() {
        aboutMeTextareaLeftCount = changeAboutMeLeftCount($(this).val().length)
        $(document).find('.about-me .left-count').html(aboutMeTextareaLeftCount);
    });

    function changeAboutMeLeftCount(currentLength){
        return aboutMeTextareaMaxLength - currentLength
    }

});
