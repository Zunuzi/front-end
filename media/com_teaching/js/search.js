jQuery(function ($) {

    $('.op-search-skills input').on('change', function(){
        if($(this).val() == '') {
            $('#op-search-skill-child-label').css('display', 'none');
        } else {
            $('#op-search-skill-child-label').css('display', 'block');
        }
    });

    var list = $('#filter-list');
    var loader = $('#loader');
    var form = $('#filter-form').data('rel', '').on('submit', function (e) {
        e.preventDefault();
        if (loader.is(':visible')) return;

        var is_parent = $('#is_parent').val();
        var children = $('#children').val();
        if (is_parent === 'yes' && children < 1) {
            Notifier.error(Joomla.JText._('COM_TEACHING_ADD_CHILD_BEFORE_SEARCH'), Joomla.JText._('COM_TEACHING_NO_CHILDREN'));
        } else {
            loader.show();
            let state = jQuery(this).serializeArray();
            let formData = jQuery(this).serialize();
            /*let url = jQuery(this).attr('action');
            if (url === '' || url === null) {
                url = window.location;
            }
            if (url.indexOf('?') === -1) {
                url += '?' + formData;
            } else {
                url += '&' + formData;
            }
            history.pushState(state, Joomla.JText._('COM_TEACHING_SCHEDULE'), url);*/
            $.ajax({
                'url': form.data('rel') ? form.data('rel') : form.attr('action'),
                'data': formData + '&ajax=1',
                'success': function (html) {
                    loader.hide();
                    list.html(html);
                    initZunuzi();
                    scrollDown("#filter-list", 1000);
                }
            });
        }
        return false;
    });

    function scrollDown(section, time){
        $('html, body').animate({
            scrollTop: $(section).offset().top - 130
        }, time);
    }


    var disabledBtn = function () {

        var btn = $(':submit', form),
            countError = 0,
            msg = '';

        btn.prop('disabled', false);
        btn.closest('span').popover({
            trigger: 'hover',
            container: 'body',
            html: true,
            placement: 'auto'
        });

        $('.search-required', form).each(function () {
            var field = $('input, select, textarea', this),
                field_val = field.val();
            if (field.length > 1 && (field.attr('type') === 'checkbox' || field.attr('type') === 'radio')) {
                field_val = [];
                $(field).each(function (i) {
                    if ($(this).is(':checked'))
                        field_val.push($(this).val());
                });
                if (field_val.length === 0) {
                    field_val = false;
                }
            }
            if (!field_val || field_val === '' || field_val === '0') {
                countError++;
                var label_content = $('label[data-content]', this).data('content');
                if (label_content) {
                    msg += label_content + '<br>';
                }
            }
        });

        if (countError > 0) {
            btn.prop('disabled', true);
            btn.css('pointer-events', 'none');
        } else {
            btn.css('pointer-events', 'unset');
        }

        btn.closest( "span" ).attr('data-content', msg);
        // btn.data('bs.popover').setContent();

    };

    if ($(':submit', form).length) disabledBtn();

    $('.search-required', form).on('change', 'input, select:first', function () {
        disabledBtn();
    });

    list.on('click', '.btn-link', function () {
        var el = $(this), rel = el.val(), slide = el.parent().next();
        var data_bind_id = el.attr("data-bind-id");
        if (!slide.data('loaded')) {
            if (el.is(':disabled')) return;
            el.prop('disabled', true);
            $.post('index.php?option=com_teaching&view=search&task=search.profile', {
                id: data_bind_id
            }, function (html) {
                $('#profile-' + data_bind_id).remove();
                $('#price-' + data_bind_id).remove();
                slide.data('loaded', 1);
                slide.append(html).children('.' + rel).trigger('slide');

                el.prop('disabled', false);
            });
        } else
            slide.children('.' + rel).trigger('slide');
    });
    list.on('click', '.btn-login, .button-login', function () {
        var tutor_id = $(this).data('bind-id'); // Hiring tutor id
        var t_type = $(this).data('tutoring-type'); // Selected Tutoring type

        $.ajax({
            url: '?option=com_users&view=login&format=raw',
            type: 'POST',
            cache: false,
            success: function (data) {
                var modal = $.teaching.runModal({
                    body: data,
                    header: Joomla.JText._('TPL_ZUNUZI_SIGN_IN'),
                });

                $($(modal).find('fieldset')[0]).append(`
                    <input type="hidden" name="tutoring.tutor" value="${tutor_id}"/>
                    <input type="hidden" name="tutoring.type" value="${t_type}" />
                `);
            }
        });
    });
    list.on('click', '.panel-heading', function () {
        $(this).parent().find('.btn-details').trigger('click');
    });
    list.on('click', '.btn-fav', function () {
        var el = $(this), em = $('em', el);
        $.post('index.php?option=com_teaching&task=favourite.action', {
            uid: el.closest('.teach-item').data('id'),
            act: em.hasClass('glyphicon-star') ? 0 : 1
        }, function (r) {
            if (!r.ok) alert(r.msg ? r.msg : 'Error! Please try again later.');
            em.prop('class', r.class);
            $('span', el).html(r.text);
        }, 'json');
    });
    list.on('slide', '.profile-slide', function () {
        var data_attr = $(this).attr('data-profile-slide');
        $('#table-price-html-' + data_attr).slideUp();
        var slide = $('#profile-details-html-' + data_attr);
        slide.html($('#profile-' + data_attr).html() + '<div style="clear: both"></div>');
        if (!slide.is(':visible')) {
            slide.slideDown();
        } else {
            slide.slideUp();
        }
    });
    list.on('slide', '.table-prices', function () {
        var data_attr = $(this).attr('data-prices-slide');
        $('#profile-details-html-' + data_attr).slideUp();
        var slide = $('#table-price-html-' + data_attr);
        slide.html($('#price-' + data_attr).html() + '<div style="clear: both"></div>');
        if (!slide.is(':visible')) {
            slide.slideDown();
        } else {
            slide.slideUp();
        }
    });

    list.on('slide', '.profile-video', function () {

        var data_attr = $(this).attr('data-video-slide');
        $('#profile-video-html-' + data_attr).slideUp();
        var slide = $('#profile-video-html-' + data_attr);
        slide.html($('#video-' + data_attr).html() + '<div style="clear: both"></div>');
        if (!slide.is(':visible')) {
            slide.slideDown();
            $(slide).children('video').attr({'id': 'video-' + data_attr, 'class': 'afterglow'});
            afterglow.init();
        } else {
            afterglow.getPlayer('video-' + data_attr).pause();
            slide.slideUp();
        }
    });

    list.on('click', '.pagination a', function (e) {
        e.preventDefault();
        if (this.href) {
            if (form.length)
                form.data('rel', this.href).trigger('submit').data('rel', '');
            else {
                if (loader.is(':visible')) return;
                loader.show();
                $.get(this.href, {ajax: 1}, function (html) {
                    loader.hide();
                    list.html(html);
                });
            }
        }
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });


    $('#filter_language_learning').on('change', function (){
        showHideEnglishField (this.value)
    });
    function showHideEnglishField (val) {
        let value = val || $('#filter_language_learning').val();

        if(value && value.match('English')) {
            $('.english-accent-row').show()
        } else {
            $('.english-accent-row').hide();
        }
    }
    showHideEnglishField($('#filter_language_learning').val())


    let oldDisabled = [];
    jQuery('.from_to_time_content').find('select').each(function () {
        let since_field = $(this);
        let selectID = since_field.attr('id');
        let $since_field = null;
        let todayMili = todayMiliseconds();

        $since_field = since_field.selectize({
            valueField: 'value',
            labelField: 'text',
            searchField: ['text'],
            sortField:  [
                {
                    field: 'text',
                    direction: 'asc'
                }
            ],
            create: false,
            render : {
                option: function(data, escape) {
                    return `<div class="option">
                                ${escape(data['text'])}
                            </div>`;
                }
            },
            onChange: function (value){

                if (selectID === 'filter_from_time'){
                    let selectize = $('#filter_to_time')[0].selectize;
                    let newHour = Number(value.split(':')[0]) + 1;

                    if (newHour === 24 ){
                        newHour = '00';
                    } else if(newHour < 10){
                        newHour = '0' + newHour;
                    }

                    let newVal = newHour + ':' + value.split(':')[1];

                    selectize.setValue(newVal, false);

                    let selectedToMili = (newHour * 3600 * 1000) + (value.split(':')[1] * 60 * 1000);
                    fifteenMili = 15 * 60 * 1000;
                    if (oldDisabled.length){
                        $.each(oldDisabled, function(i, v) {
                            selectize.updateOption(v, {
                                value: v,
                                text: v,
                                disabled: false
                            });
                        });
                    }
                    oldDisabled = [];
                    for (let i = 0; i < 3; i++){
                        selectedToMili -= fifteenMili
                        selectize.updateOption(msToTime(selectedToMili), {
                            value: msToTime(selectedToMili),
                            text: msToTime(selectedToMili),
                            disabled: true
                        });
                        oldDisabled.push(msToTime(selectedToMili));
                    }
                }
            }
        });
        $since_field = $since_field[0].selectize;

        $since_field.disable();
        $since_field.clearOptions();

        $since_field.load(function (callback) {
            let data = [];

            $.each(getTimes(), function( i, val ) {
                let disable = false;

                if(since_field.closest('.from_time_box').length
                    && val < ( todayMili + (since_field.closest('.to_time_box').length * 3600 * 1000) )
                    && $('.layout-instanttutoring').length ) {
                    disable = true;
                }

                data.push(
                    {
                        'value': msToTime(val),
                        'text': msToTime(val),
                        'disabled': disable
                    }
                );
            });
            $since_field.enable();
            callback(data);
        });


    });

    function todayMiliseconds() {
        var start = new Date();
        start.setHours(0,0,0,0);

        return new Date().getTime() - start.getTime();
    }

    function addMinutes(date, minutes) {
        return new Date(date.getTime() + minutes*60000);
    }

    function getTimes() {
        let timesResult = [];
        let d = new Date();
        for (var idx = 0; idx < 96; idx++)
        {
            let m = (((d.getMinutes() + 7.5)/15 | 0) * 15) % 60;
            let h = ((((d.getMinutes()/105) + .5) | 0) + d.getHours()) % 24;
            d = new Date(d.getYear(), d.getMonth(), d.getDay(), h, m, 0, 0);

            timesResult.push( (h * 3600 * 1000) + (m * 60 * 1000) );

            d = addMinutes(d, 15);
        }
        return timesResult;
    }

    function msToTime(duration) {
        var milliseconds = parseInt((duration % 1000) / 100),
            seconds = Math.floor((duration / 1000) % 60),
            minutes = Math.floor((duration / (1000 * 60)) % 60),
            hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

        hours = (hours < 10) ? "0" + hours : hours;
        minutes = (minutes < 10) ? "0" + minutes : minutes;
        seconds = (seconds < 10) ? "0" + seconds : seconds;

        return hours + ":" + minutes;
    }

    $.teaching.displayCertificateFile();

});
