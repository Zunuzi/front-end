var players = [];
var player;
var currentPlayerId;
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

function onYouTubeIframeAPIReady() {
    jQuery('div.youtube-video').each(function () {
        let did = jQuery(this).attr('id');
        let yid = jQuery(this).attr('data-youtube-id');
        player = new YT.Player(did, {
            height: '100%',
            width: '100%',
            videoId: yid,
            playerVars: {
                'playsinline': 1
            },
            events: {
                'onStateChange': onPlayerStateChange
            }
        })
        players.push(player);

    });
}

function onPlayerReady(event) {
    event.target.playVideo();
}

function onPlayerStateChange(event) {
    if (event.data === YT.PlayerState.PLAYING) {
        if (currentPlayerId && currentPlayerId != event.target.id) {
            players.find(pl => pl.id === currentPlayerId).pauseVideo();
            currentPlayerId = null;
        }
        currentPlayerId = event.target.id
    }
}

var modal;
jQuery(document).on('click', '.lessons-item-img-box, .lessons-item-title', function () {
    const type = jQuery(this).closest('.lessons-item').data('type');
    const url = jQuery(this).closest('.lessons-item').data('url');
    const title = jQuery(this).closest('.lessons-item').data('title');
    if (url) {
        let options;
            if (type == 'youtube') {
                options =
                    {
                        modalSelector: 'lessonsModal',
                        body: `<iframe width="100%" height="350"
                            src="${url}?autoplay=1&mute=1">
                            </iframe>`,
                        header: title
                    }
            } else {
                options =
                    {
                        modalSelector: 'lessonsModal',
                        body: `<iframe width="100%" height="350"
                            src="${url}?" title="YouTube video player"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope"
                            allowfullscreen></iframe>`,
                        header: title
                    }
            }
            modal = jQuery.teaching.runModal(options);

            modal.on('hidden.bs.modal', function (e) {
                modal.remove();
            });
    }

});
// https://www.youtube.com/embed/HMWoNjQbf-E
// body: `<iframe width="100%" height="350"
//                         src="${url}?" title="YouTube video player"
//                         frameborder="0"
//                         allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope"
//                         allowfullscreen></iframe>`,
jQuery(document).on('click', '.lessons-load-btn', function () {
    var form = jQuery('#filter-free-videos');
    let currentSearch = '';
    let action = '';
    let start = jQuery(document).find('#lessons-load-btn').data('start');
    let limit = jQuery(document).find('#lessons-load-btn').data('limit');
    let userId = '';
    if (form.length > 0) {
        action = form.data('rel') ? form.data('rel') : form.attr('action')
        currentSearch = jQuery('#filter_free_video_lessons_search').val();
    } else {
        action = jQuery(document).find('input[name="load_more_video_action"]').val();
        userId = jQuery(document).find('input[name="user_id"]').val();
    }

    var items = jQuery('.lessons-row');
    var loader = jQuery('#loader');
    let currentSubject = jQuery('input[name="current_search"]').val();
        if (loader.is(':visible')) return;
        loader.show();
        let data = 'search=' + currentSearch + '&subject=' + currentSubject +'&ajax=1' + '&start=' + start + '&limit=' + limit + '&user_id=' + userId;
            jQuery.ajax({
            'url': action,
            'data': data,
            'success': function (html) {
                data = jQuery.parseJSON(html);
                items.append(data['items']);
                if (data['page']) {
                    jQuery(document).find('#lessons-load-btn').data('start', data['page']);
                } else {
                    jQuery(document).find('#lessons-load-btn').remove();
                }
                loader.hide();
            },
            'error': function (error) {
                loader.hide();
            }
        });
        return false;
});
jQuery(function ($) {

    var items = $('.lessons-row');
    var loader = $('#loader');
    var form = $('#filter-free-videos').on('submit', function (e) {
        e.preventDefault();
        if (loader.is(':visible')) return;

        loader.show();
        let formData = jQuery(this).serialize();
        $.ajax({
            'url': form.data('rel') ? form.data('rel') : form.attr('action'),
            'data': formData + '&ajax=1',
            'success': function (data) {
                data = jQuery.parseJSON(data);
                items.html(data['items']);
                if (data['page']) {
                    if (jQuery(document).find('#lessons-load-btn').length == 0) {
                        jQuery('.lessons-load').append(`<button id="lessons-load-btn" class="lessons-load-btn" data-start="1" data-limit="4">${Joomla.JText._("COM_TEACHING_LOAD_MORE")}</button>`);
                    }
                } else {
                    jQuery(document).find('#lessons-load-btn').remove();
                }
                loader.hide();
            },
            'error': function (error) {
                loader.hide();
            }
        });
        return false;
    });

    $(document).on('change', '.free-video-lessons input[type="file"]', function() {
        const fileName = $(this).val().split('\\').pop();
        if (fileName != '') {
            $(this).closest('.upload-file').find('.uploaded-file-name').html(`${Joomla.JText._('COM_TEACHING_SELECTED_FILE_NAME').replace('%s', fileName)}`).css('display', 'block');
        } else {
            $(this).closest('.upload-file').find('.uploaded-file-name').html(``).css('display', 'none');
        }
    });

});
