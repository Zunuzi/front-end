jQuery(document).ready(function ($) {

    /*active tab from url*/
    var url = document.location.toString();
    if (url.match('#')) {
        sessionStorage.setItem(window.location.href.toString().split(window.location.host)[1].replace(/&return=[a-zA-Z0-9%]+/, '').replace(/&[a-zA-Z-_]+=[0-9]+/, ''), JSON.stringify(['#' + url.split('#')[1]]));
    }
    window.childLoggedIn = function (child_id,heading) {
        
        $('#child_id').val(child_id);
        $('#requestsForm').submit();
        return false;
    }
});