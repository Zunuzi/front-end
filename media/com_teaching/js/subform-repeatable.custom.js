;(function ($) {

    var parentRemoveRow = $.subformRepeatable.prototype.removeRow;
    $.subformRepeatable.prototype.removeRow = function ($row) {
        parentRemoveRow.call(this, $row);
        this.$container.trigger('subform-after-remove');
    };

    // var parentFixScripts = $.subformRepeatable.prototype.fixScripts;
    // $.subformRepeatable.prototype.fixScripts = function($row){
    //     parentFixScripts.call(this, $row);
    //     // bootstrap tooltips
    //     if($.fn.tooltip){
    //         // Fix popover
    //         $row.find('.hasPopover').popover({
    //             trigger: 'hover focus'
    //         });
    //     }
    // }

})(jQuery);


jQuery(document).ready(function ($) {

    $('div.subform-repeatable').on('subform-row-add', function (e, row) {
        let addButtonTooltip = $(this, row).find('.group-add.btn span:last-child').html();
        let removeButtonTooltip = addButtonTooltip.replace(`${Joomla.JText._('JGLOBAL_FIELD_ADD')}`, `${Joomla.JText._('JGLOBAL_FIELD_REMOVE')}`);
        $('a.group-remove', row).removeClass('hide');
        $('a.group-remove', row).attr('data-content', removeButtonTooltip);
    })

    $('.free_videos_row div.subform-repeatable').on('subform-row-add', function (e, row) {
        let currentFreeVideoSubject = $(this).closest('div.free_videos_row').find('input.free_video_subject');
        if (currentFreeVideoSubject.val() == '') {
            currentFreeVideoSubject.val($(this).closest('fieldset').find('div select').text());
        }
    })

});