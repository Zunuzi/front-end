var storage_teaching = Joomla.getOptions('teaching'),
    storage_calendar = Joomla.getOptions('calendar'),
    autoReloadTimer;

function refetchCalendar() {
    let subject = jQuery('#subject').val();
    jQuery('#system-message-container').find('.alert-message').map(function (key, el) {
        if (el.innerText === Joomla.JText._('COM_TEACHING_SELECT_SUBJECT') && subject.length > 0) {
            if (jQuery('#system-message-container').find('.alert-message').length > 1) {
                jQuery(el).remove();
            } else {
                jQuery(el).closest('.alert.alert-warning').find('a.close').click();
            }
        }
    });
    jQuery('#calendar').fullCalendar('refetchEvents');
}

function confirmCancelBooked() {
    var msg = Joomla.JText._('COM_TEACHING_CONFIRM_CANCELED_ALL_BOOKED'),
        filter_child_id = jQuery('#filter_child_id');
    if (filter_child_id.length) {

        if (filter_child_id.val()) {
            msg = Joomla.JText._('COM_TEACHING_CONFIRM_CANCELED_ALL_BOOKED_FOR') + ' ' + jQuery(':selected', filter_child_id).text();
        } else {
            msg = Joomla.JText._('COM_TEACHING_CONFIRM_CANCELED_ALL_BOOKED_ALL_CHILD');
        }
    }
    return confirm(msg);
}

function easyAddCart(event) {

    if (eventWorker.checkWork()) {
        return false;
    }

    eventWorker.startWork();

    var subject = jQuery('#subject').val().split('='),
    data = {
        jform: {
            'event_id': event.id,
            'tid': event.tid,
            'child_id': jQuery('#filter_child_id').val(),
            'subject_type': subject[0],
            'subject_value': subject[1],
            'uid': storage_calendar.state.uid,
            'start': event.start.format('YYYY-MM-DD HH:mm:SS'),
            'end': event.end.format('YYYY-MM-DD HH:mm:SS')

        }
    };

    data[storage_teaching.token] = 1;

    jQuery.ajax({
        url: '?option=com_teaching&task=cart.add&ajax=1',
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function (data) {

            if (data.status === 1 || data.status === 2) {
                Notifier.success(data.message);
                if (data.status === 2) {
                    Notifier.warning(data.warning, 'Warning');
                }
                /*cart count*/
                var cart_count = 1;
                if (storage_teaching.cart_count) {
                    cart_count = (parseInt(storage_teaching.cart_count)) + 1;
                }
                storage_teaching.cart_count = cart_count;
                jQuery('.cart_count').text(cart_count);
                jQuery('#calendar').fullCalendar('refetchEvents');

            } else {
                Notifier.warning(data.message, 'Warning');
                console.log(data);
                // TODO check if event type Any, if not, not refetch
                jQuery('#calendar').fullCalendar('refetchEvents');
            }
            eventWorker.stopWork();
        },
        error: function (rq, err) {
            console.log(err);
            eventWorker.stopWork();
        }
    });
}

function eventChange(event, delta, revertFunc) {
    if (eventWorker.checkWork()) {
        revertFunc();
        return;
    }
    eventWorker.startWork();
    data = {
        'id': event.id,
        'start': event.start.format('YYYY-MM-DD HH:mm:SS'),
        'end': event.end.format('YYYY-MM-DD HH:mm:SS')
    };
    data[storage_teaching.token] = 1;
    jQuery.ajax({
        url: '?option=com_teaching&task=calendar.update',
        data: data,
        type: 'POST',
        accept: 'application/json',
        dataType: 'json',
        success: function (data) {
            if (data.success) {
                Notifier.success(data.message);
            } else {
                revertFunc();
                Notifier.error(data.message);
            }
        },
        error: function (rq, err) {
            console.log(err);
        }

    }).done(function () {
        eventWorker.stopWork();
    });
}

function eventSave(data, modal) {
    eventWorker.startWork();
    var calendar = jQuery('#calendar');
    if (typeof data == "object") {
        data[ storage_teaching.token] = 1;
    } else if (typeof data == "string") {
        data += "&" + storage_teaching.token + '=1';
    }
    let eventCounts = data.dates.length;
    jQuery.ajax({
        url: '?option=com_teaching&task=calendar.add',
        data: data,
        type: "POST",
        accept: 'application/json',
        dataType: 'json',
        success: function (result) {
            var eventsData = [];
            if (!result.data.completed) {
                Notifier.error(result.data.error);
                return false;
            }
            if (result.message.length > 0) {
                if (result.success) {
                    if(eventCounts != result.data.errorEventCount) {
                        Notifier.success(result.message);
                    }
                } else {
                    Notifier.warning(result.message);
                }
            }
            if (typeof result.data.completed != 'undefined') {
                if (result.data.completed == '1' || result.data.completed) {
                    jQuery('[data-profile-search="on"]').addClass('active disabled').attr('checked', true).siblings('[type="radio"]').removeClass('active disabled');
                    jQuery('[data-profile-search="on"]').trigger('bind');
                    jQuery('#profile-show-result').val(Joomla.JText._('JYES'));
                }
            }
            for (key in result.data.events) {
                if (/^\d+$/.test(result.data.events[key])) {
                    eventsData.push({
                        id: result.data.events[key],
                        backgroundColor: storage_teaching.type[data.tid].color,
                        title: storage_teaching.type[data.tid].title,
                        start: data.start,
                        end: (data.end === '00:00' ? '24:00' : data.end)
                    });
                } else {
                    Notifier.warning(result.data.events[key], Joomla.JText._('COM_TEACHING_FAILED_TO_ADD'));
                }
            }
            if (eventsData.length) {
                calendar.fullCalendar('renderEvents', eventsData, true); // stick? = true
            }
            modal.modal('hide');

        },
        error: function (rq, err) {
            Notifier.error(err, 'Request Error');
            console.log(err);
            eventWorker.stopWork();
            calendar.fullCalendar('unselect');
            calendar.fullCalendar('refetchEvents');
        }
    }).done(function () {
        eventWorker.stopWork();
        calendar.fullCalendar('unselect');
        calendar.fullCalendar('refetchEvents');
    });
}

function order() {
    eventWorker.startWork();
    jQuery('#orderModal').modal('hide');
    var calendar = jQuery('#calendar');
    var formData = jQuery('#orderForm').serializeArray();
    jQuery.ajax({
        url: '?option=com_teaching&task=order.add',
        data: formData,
        type: "POST",
        success: function (data) {
            if (/^\d+$/.test(data)) {
                eventWorker.reservation[0].reserved = true;
                calendar.fullCalendar('updateEvent', eventWorker.reservation[0]);
            } else {
                Notifier.warning(data, 'Warning');
                console.log(data);
            }
        },
        error: function (rq, err) {
            Notifier.error(err, 'Request Error');
            console.log(err);
        }
    }).done(function () {
        eventWorker.stopWork();
        eventWorker.reservation = null;
    });
}

jQuery(document).ready(function ($) {
    var updateSessionTime1;
    $(document).on('click', 'button.edit-on-popup', function(){
        let dataTab = $(this).attr('data-tab');
        updateSessionTime1 = $('#'+dataTab + ' #time1').val();
        $(this).css('display', 'none');
        $('#eventModal button.discard-change-on-popup').css('display', 'initial');
        $('#'+dataTab).addClass('active');
        let start = moment.utc($('input[name="event_start"]').val());
        moment.tz(start, moment.tz.guess()).format('YYYY-MM-DD HH:mm:ss');
        let end = moment.utc($('input[name="event_end"]').val());
        moment.tz(end, moment.tz.guess()).format('YYYY-MM-DD HH:mm:ss');

        $('#eventModal').updateEvent(start, end, true);
    });

    $(document).on('click', 'button.discard-change-on-popup', function(){
        $(this).css('display', 'none');
        $('#eventModal button.edit-on-popup').css('display', 'initial');
        let dataTab = $(this).attr('data-tab');
        $('#'+dataTab).removeClass('active');
        $('#'+dataTab + ' #time1').parent('.time-calander-add').find('.selectize-dropdown-content > div[data-value="' + updateSessionTime1 + '"]').click();
    });

    $(document).on('click', '#update-event', function () {
        let eventId = $('input[name="event_id"]').val();
        eventWorker.startWork();
        let date = $('#edit_event_' + eventId).find("#date1").val();
        let dateStartTime = date + ' ' + $('#edit_event_' + eventId +' #time1').val();
        let dateEndTime = date + ' ' + $('#edit_event_' + eventId +' #time2').val();

        data = {
            'id': eventId,
            'start': moment(new Date(dateStartTime)).format('YYYY-MM-DD HH:mm:SS'),
            'end': moment(new Date(dateEndTime)).format('YYYY-MM-DD HH:mm:SS')
        };
        data[storage_teaching.token] = 1;
        jQuery.ajax({
            url: '?option=com_teaching&task=calendar.update',
            data: data,
            type: 'POST',
            accept: 'application/json',
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    Notifier.success(data.message);
                    $('#eventModal').modal('toggle');
                } else {
                    $('#eventModal').find('.validation-error-message').remove();
                    $(`<span class="validation-error-message error-field-text" 
                            style="color: red;font-size: 14px;line-height:1.1;display:block; padding: 5px;">
                            ${data.message}</span>`).insertAfter('#eventModal .modalMultiAdd-first-block');
                }
            },
            error: function (rq, err) {
                console.log(err);
            }
        }).done(function () {
            eventWorker.stopWork();
            calendar.fullCalendar('refetchEvents');
        });
    });

        // return true;
    var calendar = $('#calendar');

    /*auto reload begin*/
    var autoreload = storage_calendar ? parseInt(storage_calendar.autoreload) : 0;
    if (autoreload > 0) {
        var autoReloadCalendar = function () {

            if (eventWorker.checkWork() || !eventWorker.canReload) {
                setTimeout(autoReloadCalendar, 1000);
            } else {
                calendar.fullCalendar('refetchEvents');
                autoReloadTimer = setTimeout(autoReloadCalendar, storage_calendar.autoreload * 1000);
            }
        };
        autoReloadCalendar();
    }

    $('[data-toggle="popover"]').popover();

    $(document).on('click', 'tr', function (e) {

        var row_id = $(this).index() + 1;
        /*
         * Condition check if we are at day type calander and doing instant tutoring
         * Exact 93 condition is on day type calander we have 96 cells. That condition only work for last Quarter of day.
         */
        if (row_id > 93 && storage_calendar.state['instant_tutoring']) {
            var calendar = $('#calendar');
            var start_time = '';
            var end_time = '';
            switch (row_id) {
                case 94:
                    start_time = '23:15:00';
                    end_time = '00:15:00';
                    break;
                case 95:
                    start_time = '23:30:00';
                    end_time = '00:30:00';
                    break;
                default:
                    start_time = '23:45:00';
                    end_time = '00:45:00';
            }
            var start = calendar.fullCalendar('getView').start.format('YYYY-MM-DD') + ' ' + start_time;
            var end = calendar.fullCalendar('getView').end.format('YYYY-MM-DD') + ' ' + end_time;
            confirm_modal('Request for instant tutoring session today from ' + start_time + ' till ' + end_time).done(function () {
                instant_tutoring(start, end);
            }).fail(function () {
                calendar.fullCalendar('unselect');
            });
        }
    });
    $(document).on('click', '[data-request]', function (e) {
        e.preventDefault();
        var task = $(this).data('request'),
            id = $(this).data('id'),
            _this = $(this);

        //eventWorker.startWork();

        var sendTaskRequest = function (id, comment) {
            let req_data = {
                comment: comment
            };
            req_data[storage_teaching.token] = 1;
            $.ajax({
                url: '?option=com_teaching&task=request.' + task + '&ajax=1&id=' + id,
                data: req_data,
                dataType: 'json',
                method: 'POST',
                success: function (data) {
                    if (data.success) {
                        if (data.message) {
                            Notifier.success(data.message);
                        } else {
                            Notifier.success(task);
                        }
                        if (task == 'decline') {
                            _this.closest('li').remove();
                        } else {
                            _this.siblings('[data-request]').remove();
                            _this.remove();
                        }
                        calendar.fullCalendar('refetchEvents');
                    } else {
                        if (data.message) {
                            Notifier.warning(data.message, 'Warning');
                        }
                        console.log(data);
                    }
                },
                error: function (rq, err) {
                    Notifier.error(err, 'Request Error');
                    console.log(err);
                }
            }).done(function () {
                eventWorker.stopWork();
            });
        };

        if (task === 'decline') {

            jQuery('body > .modal').modal('hide');
            var modal = $.teaching.runModal({
                    modalSelector: 'decline-comment',
                    header: 'Decline comment',
                    body: '<form><div class="form-group"><textarea class="form-control" name="comment" required="required">Sorry! I\'m busy</textarea></div><button type="button" class="btn btn-default" >Submit</button></form>',
                    footer_close_button_text: 'Decline'
                }),
                temp = modal.find('textarea[name="comment"]'),
                commentVal = function () {
                    return temp.val();
                };
                // hidding_modal = false;

            temp.on('blur', function () {
                $(this).parent().removeClass('has-error')
                    .find('div.alert').remove();
            });

            jQuery('button', modal).on('click', function (e) {
                if (commentVal().length === 0) {
                    temp.parent('.form-group').addClass('has-error').focus()
                        .prepend('<div class="alert alert-danger">Please, enter the reason of declination</div>');
                    return false;
                }
                sendTaskRequest(id, commentVal());
                // hidding_modal = true;
                $('.modal').modal('hide');
            });
            // modal.on('hide.bs.modal', function () {
            //     if (!hidding_modal) jQuery('<div class="modal-backdrop fade in"></div>').appendTo('body');
            // });

        } else {
            sendTaskRequest(id, null);
        }
    });

    $('#tutor_rates').on('click', function () {
        var link = $(this);
        var rates = $(this).next().next('div.rates');
        if ($(this).hasClass('show')) {
            rates.slideUp(function () {
                link.text(Joomla.JText._('COM_TEACHING_SHOW_TUTOR_RATES')).removeClass('show');
            });
        } else {
            rates.slideDown(function () {
                link.text(Joomla.JText._('COM_TEACHING_HIDE_TUTOR_RATES')).addClass('show');
            });
        }
    });

    $('.modal').on('hide.bs.modal', function () {
        $('.modal-backdrop').remove();
    });

    $.fn.iniDatePicker = function (dt) {
        if ($(this).data("DateTimePicker") != undefined) {
            $(this).datetimepicker('destroy');
        }
        $(this).datetimepicker({
            toolbarPlacement: 'top',
            date: dt,
            format: 'YYYY-MM-DD HH:mm',
            sideBySide: true,
            debug: true,
            datepickerInput: '.datepickerinput'
        });
        return $(this);
    };

    $('.fc-event', '#external-events').each(function () {

        // store data so the calendar knows to render an event upon drop
        $(this).data('event', {
            title: $.trim($(this).text()), // use the element's text as the event title
            backgroundColor: $(this).css('background-color'),
            stick: true // maintain when user navigates (see docs on the renderEvent method)
        });

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true, // will cause the event to go back to its
            revertDuration: 0  //  original position after the drag
        });

    });

    $('input[name="filter[anotherrole]"]').on('change', function () {
        refetchCalendar();
    });

    var snapDuration = '01:00:00',
        slotDuration = '00:30:00',
        customButtons = {},
        header = {
            left: 'prev, title, next',
            center: '',
            right: 'month, agendaWeek, agendaDay'
        };

    if (storage_calendar.state.editable) {

        //header.right += ' acceptAll, declineAll, removeTimetable, removePast';

        snapDuration = '01:00:00';
        slotDuration = '00:15:00';
/*        customButtons.removeAll = {
            text: Joomla.JText._('COM_TEACHING_REMOVE_ALL'),
            click: function () {
                if (eventWorker.checkWork()) {
                    return false;
                }
                if (confirm(Joomla.JText._('COM_TEACHING_REMOVE_ALL_CONFIRM'))) {
                    eventWorker.startWork();
                    $.ajax({
                        url: '?option=com_teaching&task=calendar.removeAll&' + storage_teaching.token + '=1',
                        type: 'post',
                        data: {
                            start: calendar.fullCalendar('getView').start.format('YYYY-MM-DD'),
                            end: calendar.fullCalendar('getView').end.format('YYYY-MM-DD')
                        },
                        cache: false,
                        dataType: 'json',
                        success: function (result) {
                            if (result.status == '1') {
                                jQuery.each(result.data, function (key, val) {
                                    Notifier.info(val, key);
                                    calendar.fullCalendar('refetchEvents');
                                });
                            } else {
                                Notifier.warning(result.data, 'Warning');
                                console.log(result.data);
                            }
                            console.log(result);
                            eventWorker.stopWork();

                        },
                        error: function (r, e) {
                            console.log(e);
                            eventWorker.stopWork();
                        }
                    })
                }
            }
        };*/
    }


    if (!storage_calendar.state.editable && storage_calendar.personal) {
        //header.right += ' cancelBooked';
    }
/*

    customButtons.acceptAll = {

        text: Joomla.JText._('COM_TEACHING_ACCEPT_ALL'),
        click: function () {
            if (eventWorker.checkWork()) {
                return false;
            }
            location.href = '?option=com_teaching&task=calendar.acceptAll&' + storage_teaching.token + '=1';
        }
    };

    customButtons.declineAll = {

        text: Joomla.JText._('COM_TEACHING_DECLINE_ALL'),
        click: function () {
            if (eventWorker.checkWork()) {
                return false;
            }
            location.href = '?option=com_teaching&task=calendar.declineAll&' + storage_teaching.token + '=1';
        }
    };

    customButtons.removeTimetable = {

        text: Joomla.JText._('COM_TEACHING_REMOVE_TIMETABLE'),
        click: function () {
            if (eventWorker.checkWork()) {
                return false;
            }
            if (confirm(Joomla.JText._('COM_TEACHING_REMOVE_TIMETABLE_CONFIRM'))) {
                location.href = '?option=com_teaching&task=calendar.removeTimetable&' + storage_teaching.token + '=1';
            }
        }
    };

    customButtons.removePast = {

        text: Joomla.JText._('COM_TEACHING_REMOVE_PAST'),
        click: function () {
            if (eventWorker.checkWork()) {
                return false;
            }
            if (confirm(Joomla.JText._('COM_TEACHING_REMOVE_PAST_CONFIRM'))) {
                location.href = '?option=com_teaching&task=calendar.removePast&' + storage_teaching.token + '=1';
            }
        }
    };

    customButtons.cancelBooked = {

        text: Joomla.JText._('COM_TEACHING_CANCEL_BOOKED'),
        click: function () {
            if (eventWorker.checkWork()) {
                return false;
            }
            if (confirm(Joomla.JText._('COM_TEACHING_CANCEL_BOOKED_CONFIRM'))) {
                location.href = '?option=com_teaching&task=calendar.cancelBooked&' + storage_teaching.token + '=1';
            }
        }
    };
*/

    var now = moment.tz(storage_calendar.state['filter.timezone']);

    var defaultView = storage_calendar.state['defaultView'];

    if (storage_calendar.state['instant_tutoring']) {
        slotDuration = '00:15:00';
        snapDuration = '01:00:00';
        maxTime = '01:00:00';
        defaultView = 'agendaDay';
        header = {
            left: 'prev, next',
            center: 'title',
            right: ''
        };
    }

    var isValidEvent = function (start, end) {
        var tz = $('#filter_timezone').val(),
            dt_now = moment().tz(tz);
        start = moment.tz(start.format('YYYY-MM-DD HH:mm'), tz);
        end = moment.tz(end.format('YYYY-MM-DD HH:mm'), tz);
        dt_now = moment.tz(dt_now.format('YYYY-MM-DD HH:mm:ss'), tz);
        return calendar.fullCalendar('clientEvents', function (event) {
            return (event.rendering === "inverse-background" && start.isSameOrAfter(dt_now, 'second') && start.isSameOrAfter(event.start, 'minute') && end.isSameOrBefore(event.end, 'minute'));
        }).length > 0;
    };

    var eventsLoaded = $.Deferred();

    calendar.fullCalendar({
        customButtons: customButtons,
        locale: Joomla.getOptions('language')['tag'],
        buttonText: {
            prev: Joomla.JText._('COM_TEACHING_PREV'),
            next: Joomla.JText._('COM_TEACHING_NEXT'),
            prevYear: 'prev year',
            nextYear: 'next year',
            year: Joomla.JText._('COM_TEACHING_YEAR'),
            today: Joomla.JText._('COM_TEACHING_TODAY'),
            month: Joomla.JText._('COM_TEACHING_MONTH'),
            week: Joomla.JText._('COM_TEACHING_WEEK'),
            day: Joomla.JText._('COM_TEACHING_DAY'),
            list: Joomla.JText._('COM_TEACHING_LIST'),
        },
        header: header,
        nowIndicator: true,
        now: storage_calendar.state.today,
        allDaySlot: false,
        editable: storage_calendar.state.editable,
        droppable: storage_calendar.state.editable, // this allows things to be dropped onto the calendar
        eventDurationEditable: false,
        drop: function () {
            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
                // if so, remove the element from the "Draggable Events" list
                $(this).remove();
            }
        },
        views: {
            week: { // name of view
                titleRangeSeparator: ' \u2013 ',
                titleFormat: 'D MMM  YYYY'
            }
        },
        defaultView: defaultView,
        selectable: true,
        selectHelper: storage_calendar.state.editable || storage_calendar.state['instant_tutoring'],

        defaultDate: storage_calendar.state['filter.startDate'],
        firstDay: storage_calendar.state['filter.firstDay'],

        timeFormat: 'H(:mm)',
        slotLabelFormat: 'HH:mm',
        snapDuration: snapDuration,
        slotDuration: slotDuration,
        height: 'auto',

        selectOverlap: function (event) {
            if (storage_calendar.state['instant_tutoring'] && event.rendering === 'inverse-background') {
                return true;
            }
        },

        // dayClick: function (date, jsEvent, view) {
        //
        //     if (jsEvent.target.classList.contains('fc-bgevent')) {
        //         // clicking to background event
        //     } else {
        //         if (view.type === "month") {
        //             addEventModal(date, moment(date).add(1, 'hour'));
        //         }
        //     }
        // },

        select: addEventModal,

        dayRender: function (date, cell) {
            // todo
        },

        events: function (start, end, timezone, callback) {

            calendar.trigger('beforeRenderEvents');

            if (timezone === false || timezone == null) {
                timezone = $('#filter_timezone').val();
            }
            if (timezone == null || timezone.length < 2) {
                timezone = moment.tz.guess();
            }
            calendar.fullCalendar('removeEvents');
            eventWorker.startWork();

            var data = {
                uid: storage_calendar.state.uid,
                start: start.format('YYYY-MM-DD'),
                end: end.format('YYYY-MM-DD'),
                timezone: timezone,
                // teachingtype: $('#filter_teachingtype').val(),
                child_id: $('#filter_child_id').val(),
                anotherrole: $('input[name="filter[anotherrole]"]:checked').val(),
                current_group_id: $('#current_group_id').val()
            };

            if (storage_calendar.state['instant_tutoring']) {
                data.instant_tutoring = true;
            }

            var subject = $('select#subject');
            if (subject.length) {
                var subject_value = subject.val();
                if (subject_value.length) {
                    var subject_data = subject_value.split('=');
                    data[subject_data[0]] = subject_data[1];
                }
            }

            data[storage_teaching.token] = 1;
            $.ajax({
                url: '?option=com_teaching&task=calendar.getEvents',
                dataType: 'json',
                type: 'POST',
                data: data,
                success: function (result) {
                    data = result.data;
                    if (data.result === 1) {
                        if (data.hasOwnProperty('minDate')) {
                            storage_calendar.state.minDate = data.minDate;
                        }
                        callback(data.events);
                        eventsLoaded.resolve(data.events);
                        calendar.trigger('afterRenderEvents', data);
                    } else {
                        if (!result.data.redirect) {
                            Notifier.error('', Joomla.JText._('COM_TEACHING_ERROR_LOAD_EVENTS'));
                        }
                    }
                }
            }).done(function () {
                eventWorker.stopWork();
            });
        },

        eventDrop: function (event, delta, revertFunc) {
            eventChange(event, delta, revertFunc);
        },

        eventResize: function (event, delta, revertFunc) {
            eventChange(event, delta, revertFunc);
        },

        eventRender: function (event, element, view) {
            if (event.hasOwnProperty('id') && event.id > 0) {
                jQuery(element).attr('id', 'Event-' + event.id);
            }
            if (event.another_role === 'true') {

                element.css({'opacity': '0.5'});

            } else {

//                if (calendar.fullCalendar('getView').name !== 'month') {

                /*if (storage_calendar.state.reservation) {
                 element.css({'cursor': 'pointer'});
                 }*/
                if (storage_calendar.state['instant_tutoring']) {
                    event.start = now;


                    setTimeout(function(){
                        let availableHeight = parseInt($(document).find('.fc-body').height()) + parseInt($(document).find(".fc-bgevent").css("bottom"));
                        let availableTop = -(parseInt( $(document).find('.fc-bgevent').css('bottom')));

                        let fcBgeventTop = parseInt($(document).find(".fc-bgevent").css("top"));

                        if (fcBgeventTop === 0){
                            $(document).find('.availableEvents').css({
                                'height': availableHeight - 2,
                                'top': availableTop,
                            });
                        } else {
                            $(document).find('.availableEvents').css({
                                'height': fcBgeventTop,
                                'top': 0,
                            });
                        }

                    }, 0);

                }
                if (event.start.unix() >= now.unix()) {
                    if (event.note !== undefined) {

                        var addNote = function (el, ico) {

                            $('<a title="' + Joomla.JText._('COM_TEACHING_EDIT_NOTE') + '" class="addnote" data-toggle="modall" href="/index.php?option=com_teaching&view=note&layout=edit&event_id=' + event.id + '&id=' + event.note_id + '&format=raw" data-header="' + Joomla.JText._('COM_TEACHING_NOTE') + '"><span class="glyphicon glyphicon-' + ico + ' small"></span></a>').appendTo(el).tooltip({
                                container: 'body',
                                trigger: 'hover',
                                title: Joomla.JText._('COM_TEACHING_EDIT_NOTE')
                            }).on('click', function (e) {

                                e.preventDefault();
                                e.stopPropagation();
                                $(this).tooltip('destroy').popover('toggle');

                                if (eventWorker.checkWork())
                                    return;

                                eventWorker.startWork();

                                $.teaching.runModal($.teaching.prepareOptionsFromElement(this));
                                $('.modal').on('shown.bs.modal', function () {
                                    eventWorker.stopWork();
                                });
                            });
                        };

                        if (event.note === null) {
                            addNote(element, 'pencil');
                        } else if (event.note !== undefined && event.note.length) {

                            var noteHtml = $('<div class="note"></div>');
                            noteHtml.text(event.note + " ");
                            addNote(noteHtml, 'pencil');

                            var ico_note = $('<span class="glyphicon glyphicon-info-sign small note"></span>').appendTo(element);

                            ico_note.popover({
                                trigger: 'manual',
                                container: 'body',
                                html: true,
                                title: Joomla.JText._('COM_TEACHING_NOTE') + '<span style="cursor: pointer;" class="pull-right glyphicon glyphicon-remove" onclick="jQuery(\'span.glyphicon-info-sign.note\').popover(\'hide\');"></span>',
                                content: noteHtml,
                                placement: 'auto'
                            }).tooltip({
                                container: 'body',
                                trigger: 'hover',
                                title: Joomla.JText._('COM_TEACHING_VIEW_NOTE')
                            }).on('click', function (e) {
                                e.preventDefault();
                                e.stopPropagation();
                                $(this).tooltip('destroy').popover('toggle');
                            });

                            $('body').on('show.bs.modal', '*', function () {
                                ico_note.popover('hide');
                            });
                        }

                    }

                    var addEditBtn = function (el, ico) {

                        $('<a title="' + Joomla.JText._('COM_TEACHING_EDIT_NOTE') + '" class="addnote" data-toggle="modall" ' +
                            'href="/index.php?option=com_teaching&view=note&layout=edit&event_id=' + event.id + '&id=' + event.note_id + '&format=raw" ' +
                            'data-header="' + Joomla.JText._('COM_TEACHING_NOTE') + '"><span class="glyphicon glyphicon-' + ico + ' small"></span></a>').appendTo(el).tooltip({
                            container: 'body',
                            trigger: 'hover',
                            title: Joomla.JText._('COM_TEACHING_EDIT_NOTE')
                        }).on('click', function (e) {
                            alert('vbn');
                            e.preventDefault();
                            e.stopPropagation();
                            // $(this).tooltip('destroy').popover('toggle');
                            //
                            // if (eventWorker.checkWork())
                            //     return;
                            //
                            // eventWorker.startWork();
                            //
                            // $.teaching.runModal($.teaching.prepareOptionsFromElement(this));
                            // $('.modal').on('shown.bs.modal', function () {
                            //     eventWorker.stopWork();
                            // });
                        });
                    };

                    if (event.note === null) {
                        addNote(element, 'pencil');
                    } else if (event.note !== undefined && event.note.length) {

                        var noteHtml = $('<div class="note"></div>');
                        noteHtml.text(event.note + " ");
                        addNote(noteHtml, 'pencil');

                        var ico_note = $('<span class="glyphicon glyphicon-info-sign small note"></span>').appendTo(element);

                        ico_note.popover({
                            trigger: 'manual',
                            container: 'body',
                            html: true,
                            title: Joomla.JText._('COM_TEACHING_NOTE') + '<span style="cursor: pointer;" class="pull-right glyphicon glyphicon-remove" onclick="jQuery(\'span.glyphicon-info-sign.note\').popover(\'hide\');"></span>',
                            content: noteHtml,
                            placement: 'auto'
                        }).tooltip({
                            container: 'body',
                            trigger: 'hover',
                            title: Joomla.JText._('COM_TEACHING_VIEW_NOTE')
                        }).on('click', function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            $(this).tooltip('destroy').popover('toggle');
                        });

                        $('body').on('show.bs.modal', '*', function () {
                            ico_note.popover('hide');
                        });
                    }

                    var addRefuseBtn = function () {
                        if ($('#current_group_id').val() === event.selected_group) {
                            $('<a data-toggle="modall" class="refuseEvent" href="?option=com_teaching&view=refuse&layout=edit&id=' + event.id + '" data-header="' + Joomla.JText._('COM_TEACHING_REFUSE') + '"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>').tooltip({
                                container: 'body',
                                trigger: 'hover',
                                title: Joomla.JText._('COM_TEACHING_REFUSE')
                            }).appendTo(element).on('click', function (e) {

                                e.preventDefault();
                                e.stopPropagation();

                                if (eventWorker.checkWork())
                                    return;

                                var filter_child_id = $('#filter_child_id');
                                if (filter_child_id.length) {
                                    var child_id = filter_child_id.val(),
                                        href = $(this).attr('href');
                                    if (!child_id) {
                                        Notifier.info(Joomla.JText._('COM_TEACHING_NO_SELECT_CHILDREN'));
                                        return false;
                                    }
                                    $(this).attr('href', href + '&child_id=' + child_id);
                                }

                                eventWorker.startWork();

                                if (confirm(Joomla.JText._('COM_TEACHING_CONFIRM_REFUSE_EVENT'))) {

                                    $.teaching.runModal($.teaching.prepareOptionsFromElement(this));
                                    $('.modal').on('shown.bs.modal', function () {
                                        eventWorker.stopWork();
                                    });

                                } else {
                                    eventWorker.stopWork();
                                }
                            });
                        }
                    };


                    var color = storage_teaching.config.colors['color_ti_' + event.status];

                    if (typeof event.oe_status !== 'undefined') {
                        var oe_status = event.oe_status;
                        if (event.oe_status === '-1') {
                            oe_status = 'neg';
                        }
                        color = storage_teaching.config.colors['color_oe_' + oe_status];
                    }

                    var orders = [];

                    if (event.orders && event.orders.length > 0) {

                        color = storage_teaching.config.colors['color_oe_1'];

                        $.each(event.orders, function (index, value) {
                            orders.push('<span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>');
                        });
                    }

                    if (event.no_confirm && event.no_confirm.length > 0) {

                        color = storage_teaching.config.colors['color_oe_2'];

                        $.each(event.no_confirm, function (index, value) {
                            orders.push('<span class="glyphicon glyphicon-question-sign" aria-hidden="true"></span>');
                        });
                    }


                    if (storage_calendar.personal) {

                        if (storage_calendar.state.editable) {

                            if (event.all_orders > 0
                                && (
                                    (
                                        parseInt(event.status) == 2 &&
                                        event.start.unix() - (storage_calendar.minimum_refusing_time_instant * 60 * 60) > (new Date()).getTime() / 1000
                                    )
                                    ||
                                    (
                                        parseInt(event.status) == 1 &&
                                        event.start.unix() - (storage_calendar.minimum_refusing_time_event * 60 * 60) > (new Date()).getTime() / 1000
                                    )
                                )
                            ) {


                                if (event.orders.length === 0 && event.no_confirm.length === 0) {
                                    $('<span class="glyphicon glyphicon-remove refuseEvent" aria-hidden="true"></span>').appendTo(element).tooltip({
                                        container: 'body',
                                        trigger: 'hover',
                                        title: Joomla.JText._('COM_TEACHING_REMOVE_SESSION')
                                    }).click(function (e) {

                                        e.preventDefault();
                                        e.stopPropagation();

                                        if (eventWorker.checkWork())
                                            return;
                                        if (confirm(Joomla.JText._('COM_TEACHING_CONFIRM_REMOVE_SESSION'))) {
                                            eventWorker.startWork();
                                            let req_data = {
                                                'jform': {id: event.id, comment: '-'},
                                            };
                                            req_data[storage_teaching.token] = 1;
                                            $.ajax({
                                                url: '?option=com_teaching&task=event.refuse',
                                                data: req_data,
                                                type: 'POST',
                                                dataType: 'json',
                                                success: function (data) {
                                                    if (data.success) {
                                                        calendar.fullCalendar('removeEvents', event.id);
                                                        Notifier.success('', Joomla.JText._('COM_TEACHING_REFUSE_SUCCESS'));
                                                        calendar.trigger('removeEvent', event.id);
                                                    } else {
                                                        console.log(data);
                                                    }
                                                    if (data.message.length > 0) {
                                                        Notifier.warning(data.message, 'info');
                                                    }
                                                },
                                                error: function (rq, err) {
                                                    console.log(err);
                                                }
                                            }).done(function () {
                                                eventWorker.stopWork();
                                            });
                                        }
                                    });
                                } else {
                                    addRefuseBtn();
                                }

                            } else {
                                if (event.selected_group === undefined) {
                                    $('<span class="glyphicon glyphicon-remove refuseEvent" aria-hidden="true"></span>').appendTo(element).tooltip({
                                        container: 'body',
                                        trigger: 'hover',
                                        title: Joomla.JText._('COM_TEACHING_REMOVE_SESSION')
                                    }).click(function (e) {

                                        e.preventDefault();
                                        e.stopPropagation();

                                        if (eventWorker.checkWork())
                                            return;
                                        if (confirm(Joomla.JText._('COM_TEACHING_CONFIRM_REMOVE_SESSION'))) {

                                            $('.tooltip').fadeOut().remove();

                                            eventWorker.startWork();
                                            let req_data = {
                                                'id': event.id
                                            };
                                            req_data[storage_teaching.token] = '1';
                                            $.ajax({
                                                url: '?option=com_teaching&task=event.remove',
                                                data: req_data,
                                                type: 'POST',
                                                dataType: 'json',
                                                success: function (data) {
                                                    if (data.success) {
                                                        calendar.fullCalendar('removeEvents', event.id);
                                                        Notifier.success(Joomla.JText._('COM_TEACHING_EVENT_REMOVE_SUCCESS'));
                                                        calendar.trigger('removeEvent', event.id);
                                                    } else {
                                                        if (data.message.length > 0) {
                                                            Notifier.warning(data.message, 'Error');
                                                        }
                                                    }
                                                },
                                                error: function (rq, err) {
                                                    console.log(err);
                                                }
                                            }).done(function () {
                                                eventWorker.stopWork();
                                            });
                                        }
                                    });
                                }
                            }
                        } else {
                            if (event.refuse) {
                                addRefuseBtn();
                            }
                        }
                    }

                    if (event.seats > 1) {

                        if (orders.length > 0) {
                            element.find('.fc-content').append(
                                $('<div class="orders"/>').html(orders.join(''))
                            );


                            if (orders.length < event.seats) {
                                for (i = 0; i < (event.seats - orders.length); i++) {
                                    $('.orders', element).append('<span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span>');
                                }

                            }

                        } else {
                            element.find('.fc-content').append('<div class="orders"></div>');
                            for (i = 0; i < event.seats; i++) {
                                $('.orders', element).append('<span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span>');
                            }
                        }

                    }


                    if (event.now == true) {
                        color = storage_teaching.config.colors['color_current_lesson'];
                    }

                    if (color) {
                        element.css({'border-left-color': color, 'border-left-width': '5px'});
                    }
                }

                // TODO Check about this block of code. It was done by previous devs
/*                if (event.hasOwnProperty('end') && event.end.unix() < now.unix()) {

                    if (event.meeting_status == '2' && event.reserved == true) {
                        $('<span class="glyphicon glyphicon-ok confirmMeeting" title="confirm meeting" aria-hidden="true"></span>').appendTo(element).click(function (e) {
                            e.preventDefault();
                            e.stopPropagation();

                            var _this = $(this);

                            if (eventWorker.checkWork())
                                return;
                            if (confirm(Joomla.JText._('COM_TEACHING_CONFIRM_MEETING'))) {
                                eventWorker.startWork();
                                $.ajax({
                                    url: '?option=com_teaching&task=event.confirm&' + storage_teaching.token + '=1',
                                    data: {
                                        'id': event.id,
                                        'ajax': true
                                    },
                                    type: 'POST',
                                    dataType: "html",
                                    success: function (data) {
                                        if (data == 1) {
                                            Notifier.success('', Joomla.JText._('COM_TEACHING_MEETING_CONFIRM'));
                                            _this.remove();
                                        } else {
                                            Notifier.warning(data, 'Error');
                                            console.log(data);
                                        }
                                    },
                                    error: function (rq, err) {
                                        console.log(err);
                                    }
                                }).done(function () {
                                    eventWorker.stopWork();
                                });
                            }
                        });
                    }

                }*/

            }
        },

        eventClick: function (event, element, view) {

            if (eventWorker.checkWork()) {
                return false;
            }

            if (event.another_role !== 'true') {
                // If Meetings is now join to it by getting link
                if (event.now && event.selected_group != 11) {
                    eventWorker.startWork();
                    $.teaching.meeting.join(event);
                    if ((event.meeting_id && event.meeting_status == 0) || storage_calendar.state.myid === event.uid) {
                        return false;
                    }
                }

                if (storage_calendar.state.reservation) {

                    if (event.reserved) {
                        Notifier.info('', Joomla.JText._('COM_TEACHING_EVENT_RESERVED_ALREADY'));
                        return false;
                    }

                    var filter_child_id = $('#filter_child_id');
                    if (filter_child_id.length && !filter_child_id.val()) {
                        Notifier.info(Joomla.JText._('COM_TEACHING_NO_SELECT_CHILDREN'));
                        return false;
                    }

                    var subject = $('#subject');
                    if (!subject.val()) {
                        subject.focus();
                        Notifier.info(Joomla.JText._('COM_TEACHING_SELECT_SUBJECT'));
                        return false;
                    }

                    //console.log(event.cart_id);

                    if (!event.hasOwnProperty('cart_id') || event.cart_id === null) {

                        if (event.seats <= 0) {

                            var _this = $(this);
                            // $('#filter_teachingtype')
                            //     .find('option')
                            //     .remove()
                            //     .end()
                            //     .append('<option value="0">Select a teaching type</option>')
                            //     .val('0')
                            // ;
                            var items;
                            $.ajax({
                                url: '?option=com_teaching&task=calendar.getTeacherType',
                                type: 'POST',
                                cache: false,
                                dataType: 'json',
                                success: function (data) {

                                    var obj = data.data;
                                    if (!data.success) {
                                        if (data.data.redirect) {
                                            setTimeout(function () {
                                                location.href = data.data.redirect;
                                            }, 2000);
                                        }
                                        return false;
                                    }

                                    items = Object.keys(obj).map(function(i, e) {
                                        return $('<div class="alt-option" data-value="' + i + '">' + Object.values(obj)[e] + '</div>');
                                    });

                                    var title = '<div class="alt-option">Select a teaching type</div>';
                                    $('body').on('click', _hidePopover);
                                    $(document).on( 'scroll', _hidePopover);
                                    _this.popover({
                                        html: true,
                                        title: title,
                                        trigger: 'click',
                                        container: 'body',
                                        content: $('<div class="alt-select"/>').append(items).on('click', function (e) {

                                            tid = $(e.target).data('value');

                                            if (tid > 0) {

                                                event.tid = tid;

                                                easyAddCart(event);

                                                /*event.title = storage_teaching.type[tid].title;
                                                 event.color = storage_teaching.type[tid].color;
                                                 event.seats = storage_teaching.type[tid].seats;
                                                 _this.css('background-color', '' + storage_teaching.type[tid].color + '').find('.fc-title').text(storage_teaching.type[tid].title);*/

                                                /*$.ajax({
                                                 url: '?option=com_teaching&task=event.changeType&' + storage_teaching.token + '=1',
                                                 type: 'POST',
                                                 data: {'tid': tid, 'id': event.id},
                                                 dataType: 'html',
                                                 success: function (data) {
                                                 eventWorker.stopWork();
                                                 if (data == '1') {
                                                 event.title = storage_teaching.type[tid].title;
                                                 event.color = storage_teaching.type[tid].color;
                                                 event.seats = storage_teaching.type[tid].seats;
                                                 event.tid = tid;

                                                 _this.css('background-color', '' + storage_teaching.type[tid].color + '').find('.fc-title').text(storage_teaching.type[tid].title);
                                                 easyAddCart(event.id);

                                                 } else {
                                                 Notifier.warning(data, 'Warning');
                                                 console.log(data);
                                                 }
                                                 },
                                                 error: function (r, e) {
                                                 eventWorker.stopWork();
                                                 console.log(e);
                                                 }
                                                 });*/

                                            }
                                        }),
                                        placement: 'auto'
                                    }).popover('show');
                                }
                            });

                            function _hidePopover(e) {
                                var elem = _this.find('.fc-content').get(0);

                                if (!(e.target == elem || $(e.target).parent('.fc-content').get(0) == elem)) {
                                    $('.popover').popover('destroy');
                                    $('body').off('click', _hidePopover);
                                    $(document).off('scroll', _hidePopover);
                                }
                            }
                        } else {
                            easyAddCart(event);
                        }

                        eventWorker.reservation = [event, $(this)];
                    }
                }

                if (storage_calendar.personal || storage_calendar.state.editable) {

                    eventWorker.startWork();
                    $.ajax({
                        url: '?option=com_teaching&view=event&format=raw&layout=default.modal&id=' + event.id,
                        data: {timezone: storage_calendar.state['filter.timezone']},
                        cache: false,
                        type: 'POST',
                        success: function (data) {
                            $(data).appendTo('body').modal('show').on('hide.bs.modal', function (e) {
                                if (!($("#bs3-modal").data('bs.modal') || {}).isShown)
                                    $('.modal-backdrop').remove();
                            }).on('hidden.bs.modal', function () {
                                this.remove();
                            });

                            $('#bs3-modal').on('hide.bs.modal', function () {
                                $('.modal-backdrop').remove();
                            });
                        }
                    }).done(function () {
                        eventWorker.stopWork();
                    });

                }

            }
            return false;
        },

        selectAllow: function (selectInfo) {
           try {
               let hoursToAdd = Joomla.getOptions('calendar').allowed_booking_start;
               let diffHours, clickedDate, diffTime;
               let startArr = selectInfo.start._a;
               let myDate = new Date();

               if (startArr) {
                   clickedDate = new Date(`${startArr[1] + 1}/${startArr[2]}/${startArr[0]} ${startArr[3]}:${startArr[4]}`);
                   diffTime = clickedDate - myDate;
                   diffHours = Math.ceil(diffTime / (1000 * 60 * 60));
               } else {
                   diffHours = selectInfo.start.diff(myDate, 'hours');
               }


               if (storage_calendar.state['instant_tutoring']) {
                   if (selectInfo.end.diff(selectInfo.start, 'minutes') > 60) {
                       return false;
                   }
               } else {
                   if (diffHours <= hoursToAdd) {
                       return false;
                   }
               }
           } catch (e) {
               console.log(e.message);
           }

        },

        viewRender: function (currentView, element) {

            if ($('.teachingTypesLegend').length == 1) {
                $('.teachingTypesLegend').parent().clone()
                    .insertAfter($('#calendar').find('.fc-toolbar.fc-header-toolbar'));
            }

            if (!storage_calendar.personal) {
                var minDate = moment(storage_calendar.state.minDate);
                var btn_prev = $(".fc-prev-button");
                if (minDate.format('YYYY-MM-DD') >= currentView.start.format('YYYY-MM-DD') && minDate.format('YYYY-MM-DD') <= currentView.end.format('YYYY-MM-DD')) {
                    btn_prev.prop('disabled', true);
                    btn_prev.addClass('fc-state-disabled');
                } else {
                    btn_prev.removeClass('fc-state-disabled');
                    btn_prev.prop('disabled', false);
                }

                if (storage_calendar.state['instant_tutoring']) {
                    if(!$('.fc-left').find('.calendarLegend').length){
                        $('.fc-left').prepend(`
                        <div class="calendarLegend">
                            <div class="legendBox legendUnavailable">
                                <span class="legendColor"></span>
                                <span class="legendTxt">Unavailable</span>
                            </div>
                            <div class="legendBox legendAvailable">
                                <span class="legendColor"></span>
                                <span class="legendTxt">Available</span>
                            </div>
                        </div>
                    `);
                    }

                    var btn_next = $(".fc-next-button"),
                        maxDate,
                        hideBtn = function () {
                            btn_prev.hide();
                            btn_next.hide();
                        };

                    $.when(eventsLoaded).then(
                        function (events) {
                            events.forEach(function (event) {
                                if (typeof minDate === 'undefined') {
                                    minDate = event.start;
                                }
                                maxDate = event.end;
                            });
                            if (typeof minDate === 'undefined' || typeof maxDate === 'undefined') {
                                hideBtn();
                            } else {
                                minDate = moment(minDate);
                                maxDate = moment(maxDate);

                                if (minDate.format('YYYY-MM-DD') === maxDate.format('YYYY-MM-DD')) {
                                    hideBtn();
                                } else {
                                    if (maxDate > currentView.end) {
                                        btn_next.removeClass('fc-state-disabled');
                                        btn_next.prop('disabled', false);
                                    } else {

                                        if (minDate.format('YYYY-MM-DD') === currentView.start.format('YYYY-MM-DD')) {
                                            hideBtn();
                                        } else {

                                            btn_next.prop('disabled', true);
                                            btn_next.addClass('fc-state-disabled');
                                        }

                                    }
                                }
                            }
                            $('.fc-content-col').append(`<div class="availableEvents"></div>`);
                        }, function () {
                            hideBtn();
                        });
                }
            }
        },

        eventAfterAllRender: (view) => {
            // const scrollNumberFirst = $('.fc-time-grid-event .addnote')[0];
            // if (scrollNumberFirst && Joomla.getOptions('calendar').hasOwnProperty('scroll_to') && Joomla.getOptions('calendar').scroll_to) {
            //     window.scrolled_to = window.scrolled_to || false;
            //     let id = Joomla.getOptions('calendar').scroll_to.id;
            //     if (id != null && !window.scrolled_to) {
            //         const scrollNumber = $('#Event-'+id).offset().top - 180;
            //         Notifier.info(
            //             Joomla.JText._('COM_TEACHING_PAY_SUCCESS_FIRST').replace('{0}',
            //                 moment().tz(document.getElementById('filter_timezone').value
            //                     || Joomla.getOptions('calendar').state['filter.timezone']
            //                 ).format('YYYY-MM-DD HH:mm')).replace('{1}',
            //                 Joomla.getOptions('calendar').scroll_to.tutor_name
            //             )
            //         );
            //         $('body, html').animate({
            //             scrollTop: scrollNumber
            //         }, 800);
            //         window.scrolled_to = true;
            //     }
            // }
        }

    });

    function instant_tutoring(start, end) {
        eventWorker.startWork();
        /*
         * Build JSON to send data to model to book instant tutoring session.
         */
        var data = {
            tutor_id: storage_calendar.state.uid,
            start: start,
            end: end
        };

        var filter_child_id = $('#filter_child_id');
        if (filter_child_id.length) {
            if (!filter_child_id.val()) {
                Notifier.info(Joomla.JText._('COM_TEACHING_NO_SELECT_CHILDREN'));
                calendar.fullCalendar('unselect');
                return false;
            } else {
                data.child_id = filter_child_id.val();
            }
        }
        var subject = $('select#subject');
        if (subject.length) {
            var subject_value = subject.val();
            if (subject_value.length) {
                var subject_data = subject_value.split('=');
                data.subject_type = subject_data[0];
                data.subject_value = subject_data[1];
            }
        }
        data = {
            jform: data,
            'jform[timezone]': $('#filter_timezone').find('option:selected')[0].value
        };

        data[storage_teaching.token] = 1;
        /*
         * Ajax Call Instant Tutoring
         */
        $.ajax({
            url: '/index.php?option=com_teaching&task=instantTutoring.offer',
            data: data,
            type: 'POST',
            cache: false,
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    Notifier.success(Joomla.JText._('COM_TEACHING_INSTANT_TUTORING_SUCCESS'));
                    calendar.fullCalendar('refetchEvents');
                } else if (typeof data === "string") {
                    Notifier.error(data);
                    // console.log(data);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus);
                ajaxResultDone();
            }
        }).then(ajaxResultDone);
    }

    function ajaxResultDone() {
        eventWorker.stopWork();
        if (storage_calendar.state['instant_tutoring']) {
            calendar.fullCalendar('unselect');
        }
    }

    function confirm_modal(text) {

        var deferred = $.Deferred(),
            options =
                {
                    header: Joomla.JText._('COM_TEACHING_INSTANT_TUTORING_REQUEST'),
                    body_append: '<div class="text-right"><button type="button" class="btn btn-primary">' + Joomla.JText._('COM_TEACHING_CONFIRM') + '</button><button type="button" class="btn btn-error">' + Joomla.JText._('JCANCEL') + '</button></div>',
                    modalSelector: 'confirm',
                    body: text,
                    footer_close_button: false
                };

        var modal = $.teaching.runModal(options);

        modal.on('hidden.bs.modal', function (e) {
            deferred.reject();
        });

        $('[type="button"]', modal).on('click', function (e) {

            if ($(this).hasClass('btn-primary')) {
                deferred.resolve();

            } else {
                deferred.reject();
            }

            modal.modal('hide');

        });

        return deferred;

    }

    function addEventModal(start, end) {
        var $ = jQuery;
        if (storage_calendar.state.editable || storage_calendar.state['instant_tutoring']) {
            var today = moment().tz($('#filter_timezone').val());


            if (eventWorker.checkWork()) {
                calendar.fullCalendar('unselect');
                return false;
            }

            if (storage_calendar.state.editable) {
                eventWorker.startWork();

                $.ajax({
                    url: '?option=com_teaching&view=event&layout=multi.add&format=raw',
                    data: ({
                        start: start.format('YYYY-MM-DD HH:mm:ss'),
                        end: end.format('YYYY-MM-DD HH:mm:ss')
                    }),
                    type: 'POST',
                    cache: false,
                    success: function (data) {

                        // console.log(data)
                        jQuery('#modalMultiAdd').remove();
                        $(data).appendTo('body').addEvents(start, end).on('hide.bs.modal', function (e) {
                            calendar.fullCalendar('unselect');
                        });
                    }
                }).then(ajaxResultDone);

            } else {
                if (isValidEvent(start, end)) {
                    confirm_modal('Request for instant tutoring session today from ' + start.format('HH:mm') + ' till ' + end.format('HH:mm')).done(function () {
                        instant_tutoring(start.format('YYYY-MM-DD HH:mm:ss'), end.format('YYYY-MM-DD HH:mm:ss'));

                    }).fail(function () {
                        calendar.fullCalendar('unselect');
                    });

                } else {
                    // Notifier.warning('Session length is 1 hour. Not enough time available.');
                    calendar.fullCalendar('unselect');
                }

            }

        }

    }

});
