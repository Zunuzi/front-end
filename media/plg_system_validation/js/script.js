jQuery(document).ready(function ($) {
    const subject_levels_list = [
        'jform_profile_tutor_subject_levels0',
        'jform_profile_tutor_subject_levels1',
        'jform_profile_tutor_subject_levels2',
        'jform_profile_tutor_subject_levels3',
    ]
    setTimeout(function() {
        if ( $('#jform_profile_default_dob').length != 0 ) {
            // $('#dob_years').selectize()[0].selectize.open();
            // $('#dob_months').selectize()[0].selectize.open();
            // $('#dob_days').selectize()[0].selectize.open();
            $('#jform_profile_default_dob').on('change', function () {
                let userYear = $('#dob_years').val();
                let maxYear = $('#jform_profile_default_dob').attr("data-startyear");
                let selectedMonth = $('#dob_months').val();
                let selectedDay = $('#dob_days').val();
                let date = new Date();
                let month = date.getMonth() + 1;
                let day = date.getDate();
                let newSelectedDay = '';
                let newSelectedMonth = '';
                let newSelectedDayChoosed = false;
                let newSelectedMonthChoosed = false;
                let nextMonth = false;
                let nextDay = false;
                if (userYear == maxYear) {
                    nextDay = day + 1;
                    let nextDayItem = $(document).find('.div-dob-days .selectize-dropdown-content > div[data-value="' + nextDay + '"]');
                    if (selectedMonth < month) {
                        if (nextDayItem.length != 0) {
                            $(document).find(".div-dob-days .selectize-dropdown-content > div").each(function () {
                                let currentElement = $(this);
                                currentElement.data('value');
                                if (currentElement.data('value') <= day) {
                                    currentElement.removeAttr('data-selectable');
                                } else if (currentElement.data('value') == nextDay) {
                                    newSelectedDay = nextDay;
                                    newSelectedDayChoosed = true;
                                }
                            });
                        } else {
                            nextMonth = true;
                            newSelectedDayChoosed = true;
                            newSelectedDay = 1;
                        }
                        $(document).find(".div-dob-months .selectize-dropdown-content > div").each(function () {
                            let currentElement = $(this);
                            currentElement.data('value');
                            if (currentElement.data('value') <= month) {
                                currentElement.removeAttr('data-selectable');
                            } else if (currentElement.data('value') == month + 1) {
                                newSelectedMonth = month + 1;
                                newSelectedMonthChoosed = true;
                            }
                        });
                    } else if (month == selectedMonth) {
                        if (nextDayItem.length != 0) {
                            $(document).find(".div-dob-days .selectize-dropdown-content > div").each(function () {
                                let currentElement = $(this);
                                currentElement.data('value');
                                if (currentElement.data('value') <= day) {
                                    currentElement.removeAttr('data-selectable');
                                } else if (currentElement.data('value') == nextDay && selectedDay < nextDay) {
                                    newSelectedDay = nextDay;
                                    newSelectedDayChoosed = true;
                                }
                            });
                        } else {
                            newSelectedDay = 1;
                            newSelectedDayChoosed = true;
                            newSelectedMonth = month + 1;
                            newSelectedMonthChoosed = true;
                        }
                    } else {
                        $(document).find(".div-dob-days .selectize-dropdown-content > div").each(function () {
                            let currentElement = $(this);
                            currentElement.attr('data-selectable', '');
                        });
                        $(document).find(".div-dob-months .selectize-dropdown-content > div").each(function () {
                            let currentElement = $(this);
                            currentElement.attr('data-selectable', '');
                        });
                    }
                    if (newSelectedMonthChoosed) {
                        setTimeout(function () {
                            let element = $(document).find('.div-dob-months .selectize-dropdown-content > div[data-value="' + newSelectedMonth + '"]');
                            element.click();
                        }, 150);
                    }
                    if (newSelectedDayChoosed) {
                        setTimeout(function () {
                            let element = $(document).find('.div-dob-days .selectize-dropdown-content > div[data-value="' + newSelectedDay + '"]');
                            element.click();
                        }, 150);
                    }
                } else {
                    $(document).find(".div-dob-days .selectize-dropdown-content > div").each(function () {
                        let currentElement = $(this);
                        currentElement.attr('data-selectable', '');
                    });
                    $(document).find(".div-dob-months .selectize-dropdown-content > div").each(function () {
                        let currentElement = $(this);
                        currentElement.attr('data-selectable', '');
                    });
                }
            });
        }
    }, 500);

    $('.certificate[type=file]').each(function () {
        $(this).removeAttr('required');
    });

    var validate_selector = 'input:required, select:required, textarea:required',
        validate_forms = '[data-validation-model]';

    var currentErrors = [];

    var validate_deferreds = [];

    $.teaching = $.teaching || {};

    // $(document).on('change', '.op-user-date > div', function() {
    //     if ($(this).find('select').val() != '') {
            // console.log($(this).parent().removeClass('has-error'));
            // console.log($(this).find('> div').removeClass('selectize-error'));
            // console.log($(this).find('> div span').remove());
        // }
    // });
    window.ValidationHelper = {
        get_validate_error_msg: function (form) {

            var btn = $('[type="submit"]', form),
                msg = [];
            let rateTypesCount = 0;
            let whatICanTeachValidate = false;
            $('.error[name], div.selectize-input.error', form).each(function () {
                let error_msg = $(this).attr('data-validation-error');
                let el = this;
                if($(el).closest('#what-i-can-teach').length != 0 || $(el).closest('#what-i-can-teach-free-short-video-lessons').length != 0) {
                    whatICanTeachValidate = true;
                }
                if (this.hasOwnProperty('selectize')) {
                    el = $(this).next('.selectize-control')[0];
                }
                if (error_msg) {
                    if (el.tagName.toLowerCase() === 'input' && el.attributes.type.value.toLowerCase() === 'file') {
                        $.teaching.addErrorToInput(el.parentElement, error_msg);
                        msg.push('<a href="#' + $(this).attr('id') + '">' + error_msg + '</a>');
                    } else {
                        if ( typeof $(el).attr('name') !== "undefined" && $(el).attr('name').includes('rate_types')) {
                            rateTypesCount++;
                            if(rateTypesCount == $('input[name="jform[profile][tutor][rate_types][]"]').length) {
                                msg.push('<a href="#' + $(this).attr('id') + '">' + error_msg + '</a>');
                                $.teaching.addErrorToInput(el.closest('.checkbox'), Joomla.JText._('COM_TEACHING_PLEASE_SET_UP_YOUR_RATES'));
                            }
                        } else {
                            msg.push('<a href="#' + $(this).attr('id') + '">' + error_msg + '</a>');
                            $.teaching.addErrorToInput(el, error_msg);
                        }
                    }
                } else {
                    msg.push('<a href="#' + $(this).attr('id') + '">' + error_msg + '</a>');
                }

                setTimeout(() => {
                    $(el).popover('hide');
                }, 1500);

            });

            let whatICanTeachCompletionValidate = false;
            $('.error[name], .newRow.container-fluid.row div.selectize-input.error', form).each(function () {

                let error_msg = $(this).attr('data-validation-error');
                let el = this;
                if($(el).closest('.step3').length != 0) {
                    whatICanTeachCompletionValidate = true;
                }

            });

            let whatICanTeachCompletion = $(form).find('fieldset.step3');
            if(!whatICanTeachCompletionValidate) {
                if($(form).attr('id') == 'completion-steps' && whatICanTeachCompletion.length) {
                    let validate = false;
                    if ( (whatICanTeachCompletion).find('.container-fluid.row').find('select').length > 0){
                        $(whatICanTeachCompletion).find('.container-fluid.row').find('select').each(function(){
                            if($(this).val() != '')
                            validate = true;
                        });
                    }

                    let firstSkill = $(whatICanTeachCompletion).find('.container-fluid.row')[0];
                    console.log(firstSkill);
                    if(!validate) {
                        if($(whatICanTeachCompletion).find('span.validation-error-message.your-skills').length == 0) {
                            $(whatICanTeachCompletion).find('.container-fluid.row').css('border', '1px solid #f16731');
                            $('<span class="validation-error-message error-field-text your-skills" style="color: red;font-size: 16px;display:block;' +
                                ' text-align:center;">'+Joomla.JText._("COM_TEACHING_NOT_PROVIDED_TUTOR_SKILLS")+'</span>').insertBefore(firstSkill);
                        }
                    } else {
                        $(whatICanTeachCompletion).find('span.validation-error-message.your-skills').remove();
                        $(whatICanTeachCompletion).find('.container-fluid.row').css('border', '1px solid #ddd');
                    }
                }
            } else {
                $(whatICanTeachCompletion).find('span.validation-error-message.your-skills').remove();
                $(whatICanTeachCompletion).find('.container-fluid.row').css('border', '1px solid #ddd');
            }

            let whatICanTeach = $(form).find('#what-i-can-teach-free-short-video-lessons');
            if(!whatICanTeachValidate) {
                if($(form).attr('id') == 'member-profile' && whatICanTeach.length) {
                    let validate = false;
                    if ( (whatICanTeach).find('select').length > 0 ){
                        $(whatICanTeach).find('select').each(function(){
                            if($(this).val() != '')
                                validate = true;
                        });
                    }

                    if(!validate) {
                        msg.push('<a href="#' + $(this).attr('id') + '">'+Joomla.JText._("COM_TEACHING_NOT_PROVIDED_TUTOR_SKILLS")+'</a>');
                        if($(whatICanTeach).find('span.validation-error-message.your-skills').length == 0) {
                            $(whatICanTeach).find('.container-fluid.row').css('border', '1px solid #f16731');
                            $(whatICanTeach).prepend ('<span class="validation-error-message error-field-text your-skills" style="color: red;font-size: 16px;display:block; text-align:center;">'+Joomla.JText._("COM_TEACHING_NOT_PROVIDED_TUTOR_SKILLS")+'</span>');
                        }
                    } else {
                        $(whatICanTeach).find('span.validation-error-message.your-skills').remove();
                        $(whatICanTeach).find('.container-fluid.row').css('border', '1px solid #ddd');
                    }
                }
            } else {
                $(whatICanTeach).find('span.validation-error-message.your-skills').remove();
                $(whatICanTeach).find('.container-fluid.row').css('border', '1px solid #ddd');
            }

            if (msg.length) {
                btn.trigger('failure');
            } else {
                btn.trigger('success');

            }
            return msg;
        },

        check_validate: function (form) {
            let msg = this.get_validate_error_msg(form);
            validate_tabs(form);
            return msg.length;
        },

        ajax_validation: function (el) {

            let makeDisabled = false;

            if ($.inArray($(el).attr('id'), subject_levels_list) != '-1') {
                makeDisabled = true;
                $('#jform_profile_tutor_subject_levels input').prop('disabled', true);
            }
            setTimeout(function(){
            $(el).addClass('do-validate');
            var form = $(el).closest('form'),
                btn = $('[type="submit"]', form);
            var formId = form.attr('id');

            form.find('[name=option]').remove();

            var data = form.serializeArray();

            /*data build*/
            $('[type=file]:required', form).each(function () {
                data.push({
                    name: $(this).attr('name'),
                    value: $(this).val()
                });
            });
            $('.certificate[type=file]', form).each(function () {
                data.push({
                    name: $(this).attr('name'),
                    value: this.files[0]? [this.files[0].size, this.files[0].type]: $(this).data('value')
                });
            });
            data.push({
                name: 'validation-option',
                value: form.data('validation-option')
            });
            data.push({
                name: 'validation-model',
                value: form.data('validation-model')
            });
            /*data build*/

            btn.trigger('loading');

            $(el).removeClass('error').addClass('loading');

            var storage_teaching = Joomla.getOptions('teaching');

            let name = $(el).attr('name');
            let attr_id = $(el).attr('id');
            let dob_field = $(el).closest('div.op-user-date').length > 0;
            if(['dob_days', 'dob_months', 'dob_years'].includes(attr_id)){
                name = 'jform[profile][default][dob]';
            }
            if(name === 'jform[profile][default][dob]'){
                if( $('#dob_months').val() === '' || $('#dob_days').val() === '' || $('#dob_years').val() === '' ){
                    return false;
                }
            }
            return $.ajax({
                url: '/index.php?option=com_ajax&plugin=validation&group=system&format=json&field_name=' + name + '&' + storage_teaching.token + '=1',
                cache: false,
                data: data,
                dataType: 'json',
                method: 'post',
                success: function (success) {
                    let btnSuccess = true;
                    let field_name = $(el).attr('name');
                    if(formId == 'member-registration-welcome' || formId == 'member-registration' || formId == 'change-password'
                        || formId == 'member-profile' || formId == 'completion-steps' || formId == 'contact-form' || formId == 'refer-invite'
                        || 'user-registration') {
                        $('#' + formId + ' span.validation-error-message').not('.your-skills').remove();
                    }
                    $(el).removeClass('loading');
                    $(el).parent().find('.error-field-email-text').remove();
                    $(el).closest('div.checkboxes').find('.error-field-email-text').remove();
                    if ('string' === typeof success.msg && success.msg.toLowerCase() === 'error token') {
                        Notifier.warning(Joomla.JText._('PLG_SYSTEM_VALIDATION_CSRF_TOKEN_EXPIRED'));
                        btn.trigger('failure');
                        return false;
                    }
                    if (success.status === 1 || (success.msg == false && success.errors && !success.errors.hasOwnProperty(field_name))) {
                        $(el).parent().find('span.error-field-text').remove();
                        $(el).closest('.form-group').removeClass('has-error');
                        $(el).removeClass('error').removeClass('invalid').removeAttr('data-validation-error');
                        if (el.hasOwnProperty('selectize')) {
                            $(el).siblings('.selectize-control').children('.selectize-input').removeAttr('data-validation-error').removeClass('error invalid');
                        }
                        /** For DOB field validation is little bit complicated so we need to remove all errors if server returns all good for DOB */
                        if (dob_field) {
                            $(el).closest('div.op-user-date').find('span.error-field-text').remove();
                            $(el).closest('div.op-user-date').find('.error').removeClass('error');
                            $(el).closest('div.op-user-date').find('.selectize-error').removeClass('selectize-error');
                            $(el).closest('div.op-user-date').find('.invalid').removeClass('invalid');
                        }
                    } else {
                        if (field_name.match('rate_types')) {
                            $(el).parent().parent().parent().append($.teaching.errorSpan(Joomla.JText._('COM_TEACHING_HOURLY_RATE_SELECT_CHECKBOX')));
                        } else {
                            // $(el).closest('.form-group').addClass('has-error');
                            if(formId == 'member-registration-welcome' || formId == 'member-registration' || formId == 'change-password'
                                || formId == 'member-profile' || formId == 'completion-steps' || formId == 'contact-form' || formId == 'refer-invite'
                                || 'user-registration') {
                                if(success.current_status === 0)
                                {
                                    $(el).removeClass('error').removeAttr('data-validation-error');
                                } else {
                                    $(el).addClass('error');
                                }
                            } else {
                                if (el.hasOwnProperty('selectize')) {
                                    $(el).siblings('.selectize-control').children('.selectize-input').addClass('error').attr('data-validation-error', success.msg);
                                } else {
                                    $(el).addClass('error').attr('data-validation-error', success.msg);
                                }
                                if (dob_field) {
                                    // $.teaching.addErrorToInput($(el).closest('div.op-user-date').children('input[name="jform[profile][default][dob]"]'), success.msg);
                                } else {
                                    // $.teaching.addErrorToInput($(el), success.msg);
                                }
                            }
                        }
                        btn.trigger('failure');
                    }
                    if ($(el).is('select')) {
                        if ($(el).hasClass('error')) {
                            // $(el).siblings('.selectize-control').addClass('selectize-error');
                        } else {
                            $(el).siblings('.selectize-control').removeClass('selectize-error');
                        }
                    }

                    if(formId == 'member-registration-welcome' || formId == 'member-registration' || formId == 'change-password'
                        || formId == 'member-profile' || formId == 'completion-steps' || formId == 'contact-form' || formId == 'refer-invite'
                        || 'user-registration') {
                        let cerFileVal = true;
                        $.each(success.errors, function (key, message) {
                            btnSuccess = false;
                            let field = form.find('input[name="'+key+'"], textarea[name="'+key+'"], select[name="'+key+'"]');
                            if (field.length > 0) {
                                if(field.hasClass('do-validate')) {
                                    field.addClass('error').data('validation-error', message);
                                    field.parent().find('.chzn-container').addClass('error');
                                    if (key.indexOf('rate_types') > 0) {
                                        $.teaching.addErrorToInput(field.closest('.checkbox'), message)
                                    } else {
                                        if ( field.is( 'input[type="file"]' ) ) {
                                            field = field.parent();
                                            if($(el).hasClass('certificate')){
                                                cerFileVal = false;
                                            }
                                        }
                                        $.teaching.addErrorToInput(field, message);
                                    }

                                    if (field[0].id === 'jform_profile_default_avatar') {
                                        if ($('.upload-file .validation-error-message').length === 0) {
                                            $($.teaching.errorSpan(message)).insertBefore(
                                                $(field).closest('.custom-file-input').next()
                                            );
                                        }
                                    }
                                }
                            } else {
                                Notifier.warning(message, 'Warning');
                            }
                        });
                        if($(el).hasClass('certificate') && cerFileVal) {
                            let input = el,
                                inputID = $(el).attr('id'),
                                dataSize = $(el).data('size'),
                                acceptArray = $(el).attr('accept').split(","),
                                accept = acceptArray.map(x => {
                                    return x.replace('.','');
                                }).join(','),
                                extension = el.files[0].name.split(/[.;+_]/).pop();

                            if (extension !== 'pdf'){
                                $(el).closest('.uploadCertificateBox').append(el);
                                $(el).closest('.uploadCertificateBox').find('.uploadCertificateBtn, .uploadCertificateInfo, .uploadCertificateImg').remove();
                                $(el).closest('.uploadCertificateBox').append(`
                                <div class="uploadCertificateUploaded">
                                    <div class="uploadCertificateImg">
                                        <img src="#" alt="certificate" />
                                    </div>
                                    <div class="uploadCertificateInfo">
                                        <div class="uploadCertificateReplace">
                                            <label for="${inputID}" class="uploadCertificateReplaceLabel">${Joomla.JText._("COM_TEACHING_REPLACE_THE_FILE")}</label>
                                        </div>
                                        <div>${Joomla.JText._('COM_TEACHING_FILE_EXTENSIONS').replace("%s", accept)}</div>
                                        <div>${Joomla.JText._('JGLOBAL_MAXIMUM_UPLOAD_SIZE_LIMIT').replace("%s", dataSize)}</div>
                                        </div>
                                    </div>
                                </div>
                            `);
                                readUploadedURL(el);
                                $(el).closest('.uploadCertificateBox').find('.uploadCertificateReplace').prepend(input);
                            }

                            Notifier.info(Joomla.JText._('COM_TEACHING_SAVE_CERTIFICATE_CHANGE'));
                        }
                        btnSuccess ? btn.trigger('success'): btn.trigger('failure');
                    }

                    if (makeDisabled) {
                        $('#jform_profile_tutor_subject_levels input').removeAttr('disabled');
                    }
                },
                error: function (jqXHR, statusText, error) {
                    if (makeDisabled) {
                        $('#jform_profile_tutor_subject_levels input').removeAttr('disabled');
                    }
                    $(el).removeClass('loading').addClass('error');
                }
            });
            },200);
        }
    };

    function readUploadedURL(input) {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function(e) {
                $(input).closest('.uploadCertificateBox').find('.uploadCertificateImg').html(`
                    <img src="${e.target.result}" alt="certificate" />
                `);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function clearErrArr(el) {
        setTimeout(function () {
            currentErrors.splice(currentErrors.indexOf(el), 1);
        }, 6000);
    }


    function validate_tabs(form) {
        let tabs = [];
        $('li.rl_tabs-tab', form).each(function () {

            let tab = $(this),
                link_tab = $('a', tab),
                tab_content = $('#' + link_tab.attr('aria-controls')),
                tab_errors = 0;

            $('.error[name]', tab_content).each(function (i, el) {
                tab_errors++;
            });
            if (tab_errors > 0) {
                tab.addClass('red');
                tabs.push(link_tab);
            } else {
                tab.removeClass('red');
            }
        });
        if (tabs.length > 0) {
            tabs[0].click()
        }
    }

    $.teaching.validateTabs = validate_tabs;

    let init_validate_btn = function () {

        $('[type="submit"]:not(.validation-init)', validate_forms).each(function () {
            var btn = $(this),
                form = btn.closest('form');

            btn.off('click').on('click', function (e) {
                if (btn.hasClass('disabled')) {
                    e.preventDefault();
                    e.stopPropagation();
                    validate_tabs();
                    return false;
                }
            });

            btn.prepend('<i aria-hidden="true" class="fa fa-spinner fa-spin"></i>');

            btn.on('loading success failure', function (e) {

                switch (e.type) {
                    case 'loading':
                        $('i', this).removeClass('fa-check-circle-o fa-close').addClass('fa-spinner fa-spin');
                        break;
                    case 'success':
                        $(this).removeClass('disabled');
                        $('i', this).removeClass('fa-spin fa-spinner fa-close').addClass('fa-check');
                        break;
                    case 'failure':
                        $(this).addClass('disabled');
                        $('i', this).removeClass('fa-check-circle-o fa-spin fa-spinner').addClass('fa-close');

                }
            });
            $(document).on('change', 'input[type="file"].certificate', function(){
                $(this).addClass('supported');
            });
            btn.on('click', function (e) {
                if(btn.closest('#member-registration').length || btn.closest('#member-registration-welcome').length
                   || btn.closest('#refer-invite').length || btn.closest('#contact-form').length || btn.closest('#user-registration').length){
                    e.preventDefault();
                }
                setTimeout(() => {
                    let wrong_captcha = false;
                    let captcha_input = jQuery(this).closest('form').find('input[id^="jform_captcha"]');
                    if (
                        $(this).closest('form').find('input[type="file"].certificate ,input:invalid, input select:invalid, textarea:invalid, .selectize-input.required.invalid, input.required.invalid').not('.doNotListenValidate, .date-of-birth.hide, input[type=select-one], .supported')
                            .addClass('error')
                            .attr('data-validation-error', function (el) {
                                // init selects and add error
                                let selects = $(this).closest('.selectize-control ');
                                selects.addClass('selectize-error');
                                let element = this;
                                let prefix = '';
                                if ($(element).hasClass('selectize-input')) {
                                    if (element.classList.contains('required') && !element.value) {
                                        prefix = Joomla.JText._('PLG_SYSTEM_VALIDATION_FIELD_REQUIRED');
                                    }
                                    element = $('#' + jQuery(this).find('input').attr('id').replace('-selectized', ''))[0];
                                }
                                $(element).addClass('do-validate');
                                if ((element.hasAttribute('required') && !element.value) || $(element).is('input[type="file"].certificate')) {
                                    prefix = Joomla.JText._('PLG_SYSTEM_VALIDATION_FIELD_REQUIRED');
                                }
                                if (element.hasAttribute('placeholder')) {
                                    if (prefix.length > 0) {
                                        return prefix + $(element).attr('placeholder');
                                    }
                                    return $(element).attr('placeholder') + ' is Invalid';
                                }
                                if (prefix.length > 0) {
                                    return prefix + $('#' + $(element).attr('id') + '-lbl').text().replace('*', '');
                                }
                                return $('#' + $(element).attr('id') + '-lbl').text().replace('*', '') + ' is Invalid';
                            }).length > 0
                    ) {
                        e.preventDefault();
                        btn.trigger('failure');
                        var formId = $(this).closest('form').attr('id');
                        let _has_error_ = $('#' + formId +' .error[name], .selectize-input.error');
                        if (_has_error_.length > 0) {
                            _has_error_ = _has_error_[0];
                            if (_has_error_.classList.contains('selectized')) {
                                _has_error_ = _has_error_.previousElementSibling;
                            }
                            $([document.documentElement, document.body]).animate({
                                scrollTop: $(_has_error_).offset().top - window.innerHeight/2
                            }, 300);
                        }
                    } else {
                        if (captcha_input.length > 0) {
                            if (($('#captcha-modal-' + captcha_input.data('selector')).data('bs.modal') || {}).isShown) {
                                if (captcha_input.val().length < 10) {
                                    jQuery('.slidercaptcha.card').popover({
                                        placement: 'top',
                                        container: '.slidercaptcha.card',
                                        trigger: 'manual',
                                        html: true,
                                        content: function () {
                                            return Joomla.JText._('PLG_ZCAPTCHA_REQUIRED');
                                        }
                                    });
                                    wrong_captcha = true;
                                    captcha_input.attr('invalid', true).addClass('invalid').attr('data-validation-error', Joomla.JText._('PLG_ZCAPTCHA_REQUIRED'));
                                    btn.trigger('failure');
                                } else {
                                    captcha_input.attr('invalid', false).removeClass('invalid');
                                }
                            } else {
                                if ($('#captcha-modal-' + captcha_input.data().selector).length === 0) {
                                    $($('#captcha_block-' + captcha_input.data().selector).html()).appendTo('body');
                                }
                                $.teaching.captcha['captcha-' + captcha_input.data().selector]($(this).closest('form'));
                                return false;
                            }
                        }
                    }
                    let msg = ValidationHelper.get_validate_error_msg(form);
                    if (msg.length < 1) {
                        $(this).removeClass('disabled');
                    } else {
                        $(this).addClass('disabled');
                    }
                    return !wrong_captcha;
                }, 800);
            });



            btn.addClass('validation-init');

            $('i', btn).removeClass('fa-check-circle-o fa-spin fa-spinner');
        });

    };

    init_validate_btn();

    window.init_validation = function (el_val) {
        if (el_val == 'submit') {
            $(validate_selector, validate_forms).each(function (i, el) {
                validate_form(el);
            });
        } else {
            validate_form(el_val);
        }
        $.when.apply($, validate_deferreds).then(function () {
            ValidationHelper.check_validate(validate_forms);
        });

    };

    function validate_form(el_val) {
        let form = $(this).closest('form:not(.do-not-validate)');
        $.when(validate_deferreds.push(ValidationHelper.ajax_validation(el_val))).then(function () {
            let count_erors = ValidationHelper.check_validate(form);
            if (form.hasClass('change-msg')) {
                let btn = $('[type="submit"]', form),
                    alert = btn.siblings('#alert-warning');

                if (count_erors === 0) {
                    if (alert.length === 0) {
                        $('[type="submit"]', form).before('<div id="alert-warning" class="alert alert-warning"><a class="close" data-dismiss="alert">×</a><span>' + Joomla.JText._('PLG_SYSTEM_VALIDATION_SAVE_CHANGES_MSG') + '</span></div>');
                    } else {
                        alert.show();
                    }
                } else {
                    alert.hide();
                }
            }
        });
    }

    init_validate_btn();

    function init_fields_validation($) {
        $.find("div:not(.selectize-input) > input").not('.doNotListenValidate').on('focusout keyup', function () {
            init_validation(this);
        });

        $.find("input[type='checkbox'], input[type='radio']").not('.doNotListenValidate').on('change', function () {
            init_validation(this);
        });

        $.find("textarea").not('.doNotListenValidate').on('focusout', function () {
            init_validation(this);
        });

        $.find("select").not('.doNotListenValidate').change(function () {
            init_validation(this);
        });

        $.find(".certificate-file input[type='file']").not('.doNotListenValidate').change(function () {
            init_validation(this);
        });
    }

    init_fields_validation(jQuery('body'));

    $('#username').on('focusout', function () {
        let text = $(this).val();
        text = $.trim(text);
        $(this).val(text);
    });
    $('body').on('teaching-modal-run', '.modal', function () {
        if ($('form', this).length) {
            $('#username').on('focusout', function () {
                let text = $(this).val();
                text = $.trim(text);
                $(this).val(text);
            });
            init_validate_btn();

            $("input").not('.doNotListenValidate').on('focusout', function () {
                init_validation(this);
            });

            $("select").change(function () {
                init_validation(this);
            });
        }
    });
    /* fix  for repeatable */
    $('div.subform-repeatable').on('subform-row-add', function () {
        init_fields_validation($(this));
    });

    $('div.subform-repeatable').on('subform-after-remove', function () {
        ValidationHelper.check_validate($(this).closest('form'));
        init_fields_validation($(this));
    });

    if ($.isFunction('pwstrength')) {
        jQuery(".reset-complete").find('.validate-password').first().pwstrength({
            ui: {showVerdictsInsideProgressBar: true}
        });
    } else {
        console.log('bootstrap.pwstrength.js is not loaded');
    }
    /** Check tabs for errors */
    $('form').each((i, el) => {
        $.teaching.validateTabs(el);
    });


    $('.mobile-switch-role .avatar').on('click', function (){
        $(this).next('.new-dropdown-menu').toggleClass('opened');
    });

    $(document).on('click', function (event) {
        if ($('.new-dropdown-menu.opened').length){
            if (!$(event.target).closest('.new-dropdown-menu.opened').length
                && !$(event.target).closest('.mobile-switch-role .avatar').length) {
                $('.new-dropdown-menu.opened').removeClass('opened');
            }
        }
    });

    $(document).on('click', '.group-remove', function() {
        let rowRemoveFile = $(this).closest('.subform-repeatable-group').find('.checkbox.delete');
        let input = $(this).closest('.subform-repeatable-group').find('.teaching-file .uploadCertificateBox input[type="file"]');
        $(rowRemoveFile).find('input[type="checkbox"]').prop('checked', true);
        $(rowRemoveFile).append(input);
        if ($('#tutor-s-profile').length > 0) {
            $('#tutor-s-profile').append(rowRemoveFile);
        } else {
            $('fieldset.step2').append(rowRemoveFile);
        }
    });

    $('#registration-modal').on('hidden.bs.modal', function () {
        let regForm = $(this).find('form');
        regForm.find('input:not(input[type=submit])').val('').removeClass('error invalid');
        regForm.find('span.validation-error-message').remove();
    });

});

function check_rates(rate, old_value) {
    let new_value = rate.value;
    let name = rate.name;
    if (9 >= parseFloat(new_value)) {
        rate.value = old_value;
        jQuery(rate.id).addClass('error');
        jQuery(this).val(old_value);
        Notifier.warning(Joomla.JText._('COM_TEACHING_VALIDATION_PRICE_RATES'), 'Warning');
        return false;
    }
    let data = 'old_value=' + old_value + '&new_val=' + new_value + "&" + Joomla.getOptions('csrf.token') + "=1";
    jQuery.ajax({
        url: '/index.php?option=com_teaching&task=tutor.change_tutor_rates',
        accept: {
            json: 'application/json'
        },
        cache: false,
        data: data,
        dataType: 'json',
        method: 'post',
        success: function (result) {
            if (result.data[0] != 3) {
                rate.value = old_value;
                jQuery(rate.id).addClass('error');
                jQuery(this).val(old_value);
                if (result.data[1]) {
                    Notifier.warning(result.data[1], 'Warning');
                }
                if (result.message) {
                    Notifier.warning(result.message);
                }
            } else {
                jQuery('#rates_changed').val(1);
            }
        }
    });

}

let helpForTimezone = true;
function change_timezone(timezone) {
    if (helpForTimezone) {
        jQuery('.timezone .selectize-input').click();
        helpForTimezone = false;
    }
    let zone = timezone.split('/');
    jQuery('.timezone select').val(timezone);
    let filterCalendar = jQuery('.timezone #filter_timezone');
    if(filterCalendar.length != 0) {
        refetchCalendar();
    }
    jQuery('.timezone .selectize-dropdown-content .option').each(function (){
        if (jQuery(this).attr('data-value') === timezone) {
            jQuery(this).click();
        }
    });
    jQuery('.timezone .selectize-input .item').attr('data-value', timezone);
    jQuery('.timezone .selectize-input .item').html(zone[1]);
}
