jQuery(document).ready(function ($) {
    $('[name="instant_tutoring_state"]').on('change', function(){
         let btn = $(this);
         $.teaching.instant_tutoring(btn);
    });
    $(".instantOn").on('click', function (){
        let btn = $(this);
        $.teaching.instant_tutoring(btn);
    });

    $(document).on('click', '.uploadCertificateImg', function (){
        let file,
            uploadUrl = $(this).closest('.uploadCertificateUploaded').find('.uploadCertificateReplace > input.bg-primary').data('value');

        if (uploadUrl){
            let fileFormat = uploadUrl.split(/\.(?=[^\.]+$)/)[1];
            if (fileFormat === 'pdf'){
                file = `<iframe src="${uploadUrl}" frameBorder="0"></iframe>`;
            } else {
                file = `<img src="${uploadUrl}" />`;
            }
        } else {
            let uploadBase64 = $(this).find('img').attr('src');
            file = `<img src="${uploadBase64}" />`;
        }

        $.teaching.pdf_modal(file);
    });
});
