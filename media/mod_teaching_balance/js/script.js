jQuery(document).ready(function ($) {
    $('#calendar').on('beforeRenderEvents',function () {
        $.ajax({
            url: '/index.php?option=com_ajax&module=teaching_balance&format=raw&' + Joomla.optionsStorage.teaching.token + '=1',
            method: 'POST',
            success: function (data) {
                $('span','div.balance').text(data);
            }
        });
    });
});